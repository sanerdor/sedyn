#include "transition_function.h"


//* Constructor

TransitionFunction::TransitionFunction (std::string interaction_state, std::unique_ptr<BaseElectronicFunction>& electronic_function)
: interaction_state_(interaction_state), electronic_function_(std::move(electronic_function))
{
}

//* Getters

std::string TransitionFunction::GetInteractionStateLabel () const { return interaction_state_; }

//* Public Methods

double TransitionFunction::GetEnergy (const std::vector<std::weak_ptr<Molecule>>& molecule_list) const
{
    return electronic_function_->GetEnergy(molecule_list);
}