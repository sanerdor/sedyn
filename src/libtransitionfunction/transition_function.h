#pragma once

#include "../libelectronicfunction/base_electronic_function.h"

#include <memory>
#include <string>
#include <vector>


class Molecule;

class TransitionFunction
{
public:
    //* Constructor
    TransitionFunction () = default;
    TransitionFunction (std::string, std::unique_ptr<BaseElectronicFunction>&);

    //* Destructor
    ~TransitionFunction () = default;

    //* Getters
    std::string GetInteractionStateLabel () const;

    //* Public Methods
    double GetEnergy (const std::vector<std::weak_ptr<Molecule>>&) const;

private:
    //* Variables
    std::string interaction_state_;                                            // 32
    std::shared_ptr<BaseElectronicFunction> electronic_function_;              // 8
};