#pragma once

#include "../libalgorithm/kinetic_monte_carlo.h"
#include "../libmolecule/molecule.h"
#include "../libstate/state.h"
#include "../libanalysis/analysis.h"

#include <armadillo>
#include <map>
#include <memory>
#include <vector>


class System
{
public:
    //* Constructor
    System () = default;
    System (arma::uvec, arma::dvec, double, int, int, bool, ErrorManager* error_manager,
            FileManager* file_manager);

    //* Destructor
    ~System () = default;

    System& operator= (const System& other);

    //* Getters
    arma::uvec GetBoundaries () const;
    arma::dvec GetCellSize () const;
    int GetNStep () const;
    int GetNTraj () const;
    double GetTemperature () const;

    //* Setters
    void SetCellSize (arma::dvec);
#if DO_TEST
    void SetTemperature (double val) { temperature_ = val; }
#endif

    //* Public Methods
    void EndTrajectory ();
    int GetCurrentStateListCount (std::string) const;
    State GetCopyReferenceState (const std::string);
    void InitializeForTrajectory ();
    void ProcessNeighbours ();
    void RunTrajectory (Analysis& analysis);
    void SaveMolecules (std::vector<std::unique_ptr<Molecule>>&);
    void SaveReferenceStates (std::vector<std::unique_ptr<State>>&, std::unique_ptr<State>&);

private:
    //* Variables
    arma::uvec boundaries_;                                                     // 192
    arma::dvec cell_size_;                                                      // 192
    arma::dvec cut_off_;                                                        // 192
    std::map<std::string, std::unique_ptr<State>> reference_states_;            // 48
    std::vector<std::shared_ptr<Molecule>> molecule_list_;                      // 24
    std::vector<std::shared_ptr<State>> state_list_;                            // 24
    KineticMonteCarlo algorithm_;                                               // ???
    std::shared_ptr<State> ground_state_;                                       // 8
    double temperature_;                                                        // 8
    int n_step_;                                                                // 4
    int n_traj_;                                                                // 4
    bool read_sdn_;                                                             // 4
    ErrorManager* error_manager_;
    FileManager* file_manager_;

    //* Private Methods
    void LinkNeighbours (const arma::umat&) const;
    void UpdateStates ();
    void UpdateSystem (const RateStateProcess&);
};