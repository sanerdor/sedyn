#include "system.h"

#include "../global_functions.h"
#include "../global_objects.h"
#include "../libmanager/error_manager.h"
#include "../libmanager/file_manager.h"
#include "../libstate/rate_state_process.h"

#ifdef USE_OpenMP
    #include <omp.h>
#endif

#include <chrono>
#include <fstream>

#include <iomanip>

//* Constructor

System::System (arma::uvec boundaries, arma::dvec cut_off, double temperature, int n_step, int n_traj,
                bool read_sdn, ErrorManager* error_manager, FileManager* file_manager)
: boundaries_(boundaries), cell_size_(arma::dvec(3, arma::fill::zeros)), cut_off_(cut_off), temperature_(temperature),
    n_step_(n_step), n_traj_(n_traj), read_sdn_(read_sdn), error_manager_(error_manager), file_manager_(file_manager)
{
    algorithm_ = KineticMonteCarlo(error_manager);
}

System& System::operator= (const System& other)
{
    if (this == &other) { return *this; }

    boundaries_ = other.boundaries_;
    cell_size_ = other.cell_size_;
    cut_off_ = other.cut_off_;
    molecule_list_ = other.molecule_list_;
    state_list_ = other.state_list_;
    algorithm_ = other.algorithm_;
    temperature_ = other.temperature_;
    n_step_ = other.n_step_;
    n_traj_ = other.n_traj_;
    read_sdn_ = other.read_sdn_;
    error_manager_ = other.error_manager_;
    file_manager_ = other.file_manager_;

    for (auto& it : other.reference_states_)
    {
        std::unique_ptr<State> state = std::make_unique<State>(*it.second);
        reference_states_.insert({it.first, std::move(state)});
    }
    ground_state_ = other.ground_state_;

    return *this;
}

//* Getters

arma::uvec System::GetBoundaries () const { return boundaries_; }
arma::dvec System::GetCellSize () const { return cell_size_; }
int System::GetNStep () const { return n_step_; }
int System::GetNTraj () const { return n_traj_; }
double System::GetTemperature () const { return temperature_; }

//* Setters
void System::SetCellSize (arma::dvec cell_size) { cell_size_ = cell_size; }

//* Public Methods

void System::EndTrajectory ()
{

    state_list_.clear();
}

int System::GetCurrentStateListCount (std::string state_label) const
{
    int count = std::count_if(state_list_.begin(), state_list_.end(),
        [&] (const std::shared_ptr<State>& state)
        {
            return state->GetLabel() == state_label;
        });

    return count;
}

void System::InitializeForTrajectory ()
{
    for (std::shared_ptr<Molecule>& molecule : molecule_list_) { molecule->SetState(ground_state_); }
    
    std::uniform_int_distribution<int> molecule_distribution(0, molecule_list_.size()-1);
    for (auto& state : reference_states_)
    {
        int initial_number = state.second->GetInitialNumber();
        for (size_t i = 0; i < initial_number; i++)
        {
            //* New State
            int target_molecule = 0;
            int count = 0;
            std::string molecule_label;
            do
            {
                target_molecule = molecule_distribution(GlobalObjects::randomizer);
                const std::shared_ptr<Molecule>& molecule = molecule_list_[target_molecule];
                const std::shared_ptr<State>& molecule_state = molecule->GetState().lock();
                molecule_label = molecule_state->GetLabel();
                
                if (count > 100)
                {
                    std::string error = "Could not find an empty Molecule to add a new State\n";
                    error += "Check the Molecule/State ratio and decrease the number of initial states if required\n";
                    error_manager_->SSystemError(error);
                }
                count++;
            } while (molecule_label != "gs");
            
            std::shared_ptr<State> new_state = std::make_shared<State>(*state.second);
            new_state->SetMolecule(molecule_list_[target_molecule]);
            molecule_list_[target_molecule]->SetState(new_state);
            state_list_.push_back(std::move(new_state));
        }
    }

    this->UpdateStates();
}

void System::ProcessNeighbours ()
{
    if (read_sdn_)
    {
        arma::umat neighbours;
        file_manager_->ReadNeighbours(neighbours);
        this->LinkNeighbours(neighbours);

        return;
    }
    
    size_t size = molecule_list_.size();
    arma::sp_umat sp_neighbours(size, size);

#if USE_OpenMP
    //* OpenMP : Process Neighbours
    #pragma omp parallel default(shared)
    {
        #pragma omp for collapse(2)
        for (size_t i = 0; i < size-1; i++)
        {
            for (size_t j = i+1; j < size; j++)
            {
                Molecule& mol1 = *molecule_list_[i];
                Molecule& mol2 = *molecule_list_[j];

                arma::dvec com1 = mol1.GetCenterOfMass();
                arma::dvec com2 = mol2.GetCenterOfMass();
                arma::dvec trans = NearImageTransform(com1, com2, boundaries_, cell_size_);
                arma::dvec r = (com2 - trans - com1)/cut_off_;

                if (std::pow(arma::norm(r), 2) <= 1) { sp_neighbours(i, j) = 1; }
            }
            
        }
    }
#else
    //* SERIAL : Process Neighbours
    for (size_t i = 0; i < size-1; i++)
    {
        for (size_t j = i+1; j < size; j++)
        {
            Molecule& mol1 = *molecule_list_[i];
            Molecule& mol2 = *molecule_list_[j];

            arma::dvec com1 = mol1.GetCenterOfMass();
            arma::dvec com2 = mol2.GetCenterOfMass();
            arma::dvec trans = NearImageTransform(com1, com2, boundaries_, cell_size_);
            arma::dvec r = (com2 - trans - com1)/cut_off_;

            if (std::pow(arma::norm(r), 2) <= 1) { sp_neighbours(i, j) = 1; }
        }
    }
#endif

    size_t index = 0;
    arma::umat neighbours(2, sp_neighbours.n_nonzero, arma::fill::zeros);
    for (arma::sp_umat::iterator it = sp_neighbours.begin(); it != sp_neighbours.end(); ++it)
    {
        neighbours(0, index) = it.col();
        neighbours(1, index) = it.row();
        index++;
    }

    this->LinkNeighbours(neighbours);
    file_manager_->WriteNeighbours(neighbours);
}

void System::RunTrajectory (Analysis& analysis)
{
    for (size_t step = 0; step < n_step_; step++)
    {
        //* Perform step
        std::vector<RateStateProcess> chosen_processes;
        std::vector<RateStateProcess> non_isolated_processes;

#if USE_OpenMP
        //* OpenMP : Chosen process selection and non isolated storage
        #pragma omp parallel default(shared)
        {
            std::vector<RateStateProcess> thread_non_isolated_processes;
            std::vector<RateStateProcess> thread_chosen_processes;

            #pragma omp for
            for (const std::shared_ptr<State>& state : state_list_)
            {
                std::vector<RateStateProcess> allowed_processes = state->GetAllowedProcesses(*this);
                if (allowed_processes.size() == 0) { continue; }
                for (RateStateProcess& rsp : allowed_processes) { rsp.SetState(state); }

                if (state->IsIsolated())
                {
                    #pragma omp critical
                    {
                    RateStateProcess chosen = algorithm_.EvaluateProcesses(allowed_processes);
                    thread_chosen_processes.push_back(std::move(chosen));
                    }
                } else
                {
                    thread_non_isolated_processes.insert(thread_non_isolated_processes.end(),
                                                            allowed_processes.begin(), allowed_processes.end());
                }
            }

            #pragma omp critical
            {
                if (thread_chosen_processes.size() > 0)
                {
                    chosen_processes.insert(chosen_processes.end(), thread_chosen_processes.begin(), thread_chosen_processes.end());
                }

                if (thread_non_isolated_processes.size() > 0)
                {
                    non_isolated_processes.insert(non_isolated_processes.end(), thread_non_isolated_processes.begin(), thread_non_isolated_processes.end());
                }
            }
        }
#else
        //* SERIAL : Chosen process selection and non isolated storage
        for (const std::shared_ptr<State>& state : state_list_)
        {
            std::vector<RateStateProcess> allowed_processes = state->GetAllowedProcesses(*this);
            if (allowed_processes.size() == 0) { continue; }
            for (RateStateProcess& rsp : allowed_processes) { rsp.SetState(state); }

            if (state->IsIsolated())
            {
                RateStateProcess chosen = algorithm_.EvaluateProcesses(allowed_processes);
                chosen_processes.push_back(std::move(chosen));
            } else
            {
                non_isolated_processes.insert(non_isolated_processes.end(), allowed_processes.begin(), allowed_processes.end());
            }
        }
#endif

        //* Evaluate non isolated processes
        if (non_isolated_processes.size() > 0)
        {
            RateStateProcess chosen = algorithm_.EvaluateProcesses(non_isolated_processes);
            chosen_processes.push_back(std::move(chosen));
        }

        for (const RateStateProcess& rsp : chosen_processes)
        {
            //* Check process inconsistencies
            if (rsp.GetTime() < 0)
            {
                std::string error = "Negative time step process selected.\n";
                error_manager_->SSystemError(error);
            }

            //* Update the system according to chosen processes
            this->UpdateSystem(rsp);

            //* Save new states data
            analysis.RecordStateData(rsp, n_step_);
        }
    }
}

void System::SaveMolecules (std::vector<std::unique_ptr<Molecule>>& molecules)
{
    molecule_list_.reserve(molecules.size());
    for (std::unique_ptr<Molecule>& molecule : molecules) { molecule_list_.push_back(std::move(molecule)); }
}

void System::SaveReferenceStates (std::vector<std::unique_ptr<State>>& states, std::unique_ptr<State>& ground_state)
{
    for (std::unique_ptr<State>& state : states)
    {
        std::string label = state->GetLabel();
        if (reference_states_.find(label) == reference_states_.end())
        {
            reference_states_.insert({label, std::move(state)});
        }
    }

    ground_state_ = std::move(ground_state);
}

//* Private Methods

void System::LinkNeighbours (const arma::umat& neighbours) const
{
    for (size_t i = 0; i < neighbours.n_cols; i++)
    {
        size_t m = neighbours(0, i);
        size_t n = neighbours(1, i);
        molecule_list_[m]->AddNeighbour(molecule_list_[n]);
        molecule_list_[n]->AddNeighbour(molecule_list_[m]);
    }
}

void System::UpdateStates ()
{
#if USE_OpenMP
    #pragma omp parallel default(shared)
    {
        #pragma omp for
        for (std::shared_ptr<State> state : state_list_) { state->Update(*this); }
    }
#else
    for (std::shared_ptr<State>& state : state_list_) { state->Update(*this); }
#endif
}

void System::UpdateSystem (const RateStateProcess& chosen)
{
    std::shared_ptr<State> donor_state = chosen.GetState().lock();
    std::vector<std::weak_ptr<Molecule>> process_molecules = donor_state->GetMolecules();

    std::shared_ptr<State> acceptor_state = nullptr;
    if (chosen.GetInteractionMolecule().lock() != nullptr)
    {
        acceptor_state = chosen.GetInteractionMolecule().lock()->GetState().lock();
        if (acceptor_state->GetLabel() == "gs")
        {
            process_molecules.push_back(chosen.GetInteractionMolecule());
        } else
        {
            std::vector<std::weak_ptr<Molecule>> acceptor_molecules = acceptor_state->GetMolecules();
            process_molecules.insert(process_molecules.end(), acceptor_molecules.begin(), acceptor_molecules.end());
        }
    }
    
    bool keep_donor_state = false;
    bool keep_acceptor_state = false;
    size_t current_mol = 0;
    for (const std::string target_label : chosen.GetTargetStateLabels())
    {
        //* Target label is GS
        if (target_label == "gs")
        {
            process_molecules[current_mol].lock()->SetState(ground_state_);
            current_mol++;
            
        }
        //* Target label is donor state
        else if (target_label == donor_state->GetLabel())
        {
            int size = donor_state->GetSize();
            std::vector<std::weak_ptr<Molecule>> sub_molecules {process_molecules.begin()+current_mol, process_molecules.begin()+current_mol+size};
            current_mol += size;

            if (!keep_donor_state)
            {
                keep_donor_state = true;
                donor_state->SetMolecule(sub_molecules);
                for (std::weak_ptr<Molecule>& molecule : sub_molecules) { molecule.lock()->SetState(donor_state); }
            } else
            {
                std::shared_ptr<State> new_state = std::make_shared<State>(*donor_state);
                new_state->SetMolecule(sub_molecules);
                for (std::weak_ptr<Molecule>& molecule : sub_molecules) { molecule.lock()->SetState(new_state); }
                
                state_list_.push_back(std::move(new_state));
            }
        }
        //* Target label is acceptor state
        else if (acceptor_state != nullptr && target_label == acceptor_state->GetLabel())
        {
            int size = acceptor_state->GetSize();
            std::vector<std::weak_ptr<Molecule>> sub_molecules {process_molecules.begin()+current_mol, process_molecules.begin()+current_mol+size};
            current_mol += size;

            if (!keep_acceptor_state)
            {
                keep_acceptor_state = true;
                acceptor_state->SetMolecule(sub_molecules);
                for (std::weak_ptr<Molecule>& molecule : sub_molecules) { molecule.lock()->SetState(acceptor_state); }
            } else
            {
                std::shared_ptr<State> new_state = std::make_shared<State>(*acceptor_state);
                new_state->SetMolecule(sub_molecules);
                for (std::weak_ptr<Molecule> molecule : sub_molecules) { molecule.lock()->SetState(new_state); }
                
                state_list_.push_back(std::move(new_state));
            }
        }
        //* Target label is neither of ground state, donor or acceptor (New state)
        else
        {
            
            std::shared_ptr<State> new_state = std::make_shared<State>(*reference_states_[target_label]);
            int size = new_state->GetSize();
            std::vector<std::weak_ptr<Molecule>> sub_molecules {process_molecules.begin()+current_mol, process_molecules.begin()+current_mol+size};
            current_mol += size;

            new_state->SetMolecule(sub_molecules);
            for (std::weak_ptr<Molecule> molecule : sub_molecules) { molecule.lock()->SetState(new_state); }

            state_list_.push_back(std::move(new_state));
        }
    }

    //* Remove from state_list_ the states that are no more used
    if (!keep_donor_state) { state_list_.erase(std::remove(state_list_.begin(), state_list_.end(), donor_state)); }
    if (!keep_acceptor_state && acceptor_state != nullptr && acceptor_state->GetLabel() != "gs")
    {
        state_list_.erase(std::remove(state_list_.begin(), state_list_.end(), acceptor_state));
    }

    this->UpdateStates();
}

State System::GetCopyReferenceState (const std::string label)
{
    if (label == "gs") { return State(*ground_state_); }
    else { return State(*reference_states_[label]); }
}