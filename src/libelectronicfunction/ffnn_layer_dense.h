/**
 * @file ffnn_layer_dense.h
 * @author Aitor Diaz-Andres (ad.andres@outlook.com)
 * @brief FFNNLayerDense class declaration file.
 * @date 2024-05-20
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#pragma once

#include <armadillo>
#include <string>


enum class ActivationFunctionType
{
    Default,
    ActivationReLU,
    ActivationSigmoid
};

/**
 * @brief Feed Forward Neural Network Dense layer to return a value from an input vector.
 * 
 * The FFNN layer computes a response vector from an input vector multiplied by a matrix of weights
 * and adding a vector of biases (if declared). This layer then returns a vector of values which will be
 * evaluated by the next FFNNLayerDense. The last layer will return a vector of dimension 1 with the
 * result value.
 */
class FFNNLayerDense
{
private:
    arma::dmat weigths_;                                                       // 192
    arma::rowvec biases_;                                                      // 192
    arma::rowvec (FFNNLayerDense::*activation_function_) (const arma::rowvec&);// 8

public:
    /**
     * @brief Construct a new FFNNLayerDense object.
     * 
     */
    FFNNLayerDense () =  default;

    /**
     * @brief Construct a new FFNNLayerDense object.
     * 
     * @param weigths Matrix of weights.
     * @param biases Vector of biases.
     * @param act_func_type String with the target activation function.
     */
    FFNNLayerDense (arma::dmat weigths, arma::dvec biases, std::string act_func_type);

    /**
     * @brief Destroy the FFNNLayerDense object
     * 
     */
    ~FFNNLayerDense () = default;

    /**
     * @brief Get the dimension of the input vector.
     * 
     * @return int 
     */
    int GetNInput () const;

    /**
     * @brief Get the dimension of the output vector.
     * 
     * @return int 
     */
    int GetNOutput () const;

    /**
     * @brief Compute the response (output) vector from the input vector.
     * 
     * @param input Armadillo input row vector.
     * @return arma::rowvec 
     */
    arma::rowvec Predict (const arma::rowvec& input);

private:
    /**
     * @brief Linear activation function.
     * 
     * @param input Armadillo input row vector.
     * @return arma::rowvec 
     */
    arma::rowvec ActivationLinear (const arma::rowvec& input);

    /**
     * @brief Rectified Linear Unit (ReLU) activation function.
     * 
     * @param input Armadillo input row vector.
     * @return arma::rowvec 
     */
    arma::rowvec ActivationReLU (const arma::rowvec& input);

    /**
     * @brief Sigmoid type activation function.
     * 
     * @param input Armadillo input row vector.
     * @return arma::rowvec 
     */
    arma::rowvec ActivationSigmoid (const arma::rowvec& input);

    /**
     * @brief Return ActivationFunctionType according to the string.
     * 
     * @param str String with the target activation function to be interpreted.
     * @return ActivationFunctionType 
     */
    ActivationFunctionType StringToActivationFunctionEnum (const std::string str) const;
};