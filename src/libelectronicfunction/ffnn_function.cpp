#include "ffnn_function.h"

#include "../global_functions.h"
#include "../global_namespaces.h"
#include "../libmolecule/molecule.h"


FFNNFunction::FFNNFunction (std::vector<std::unique_ptr<FFNNLayerDense>>& hidden_layers)
{
    for (std::unique_ptr<FFNNLayerDense>& layer : hidden_layers) { hidden_layers_.push_back(std::move(layer)); }
}

double FFNNFunction::GetEnergy (const std::vector<Molecule>& molecules) const
{
    Molecule target_molecule(molecules);
    
    arma::dmat cm = target_molecule.GetCoulombMatrix();
    cm = cm - cm.min();
    cm = cm / cm.max();
    arma::uvec uu_ind = arma::trimatu_ind(arma::size(cm), 0); //* Upper triangule indexes (without diagonal)
    arma::rowvec output = cm(uu_ind).t();
    
    for (const std::shared_ptr<FFNNLayerDense>& layer : hidden_layers_)
    {
        output = layer->Predict(output);
    }

    return output(0);
}

double FFNNFunction::GetEnergy (const std::vector<std::weak_ptr<Molecule>>& molecules) const
{
    std::vector<Molecule> ref_molecules;
    for (const std::weak_ptr<Molecule>& mol : molecules) { ref_molecules.push_back(*mol.lock()); }

    return this->GetEnergy(ref_molecules);
}