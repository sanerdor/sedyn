/**
 * @file base_electronic_function.h
 * @author Aitor Diaz-Andres (ad.andres@outlook.com)
 * @brief BaseElectronicFunction class declaration.
 * @date 2024-05-20
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#pragma once

#include <memory>
#include <vector>


class Molecule;

/**
 * @brief Abstract electronic function class to define the possible energy function calls.
 * 
 * The aim of this abstraction is to unify the functions for all the electronic functions. Besides,
 * this allows to form vectors.
 */
class BaseElectronicFunction
{
public:
    /**
     * @brief Construct a new Base Electronic Function object
     * 
     */
    BaseElectronicFunction () = default;

    /**
     * @brief Destroy the Base Electronic Function object
     * 
     */
    virtual ~BaseElectronicFunction () = default;

    /**
     * @brief Get the energy.
     * 
     * @param molecules Vector of Molecule.
     * @return double 
     */
    virtual double GetEnergy (const std::vector<Molecule>& molecules) const = 0;

    /**
     * @brief Get the energy.
     * 
     * @param molecules Vector of Molecule pointers.
     * @return double 
     */
    virtual double GetEnergy (const std::vector<std::weak_ptr<Molecule>>& molecules) const = 0;
};