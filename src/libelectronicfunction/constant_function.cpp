#include "constant_function.h"

#include <math.h>


ConstantFunction::ConstantFunction (double energy)
: energy_(energy)
{}

double ConstantFunction::GetEnergy (const std::vector<Molecule>& molecules) const
{
    return energy_;
}

double ConstantFunction::GetEnergy (const std::vector<std::weak_ptr<Molecule>>& molecules) const
{
    return energy_;
}