/**
 * @file ffnn_function.h
 * @author Aitor Diaz-Andres (ad.andres@outlook.com)
 * @brief FFNNFunction class declaration.
 * @date 2024-05-20
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#pragma once

#include "base_electronic_function.h"
#include "ffnn_layer_dense.h"

#include <armadillo>
#include <memory>
#include <string>
#include <vector>


/**
 * @brief Feed Forward Neural Network to return the energy for a group of molecules.
 * 
 * This algorithm evaluates the input vectorized Coulomb Matrix of molecule to return the energy. The FFNN algorithm
 * predicts the output value by passing the input through several FFNNLayerDense. The last layer will return the energy
 * associated to the target system. See
 * <a href="https://en.wikipedia.org/wiki/Feedforward_neural_network">Wikipedia</a>.
 */
class FFNNFunction : public BaseElectronicFunction
{
private:
    std::vector<std::shared_ptr<FFNNLayerDense>> hidden_layers_;               //* 24

public:
    /**
     * @brief Construct a new FFNNFunction object
     * 
     */
    FFNNFunction () = default;

    /**
     * @brief Construct a new FFNNFunction object from a colletion of FFNNLayerDense.
     * 
     * @param hidden_layers Vector of FFNNLayerDense object pointers.
     */
    FFNNFunction (std::vector<std::unique_ptr<FFNNLayerDense>>& hidden_layers);

    /**
     * @brief Destroy the FFNNFunction object.
     * 
     */
    ~FFNNFunction () = default;

    /**
     * @brief Get the energy from the FFNN.
     * 
     * @param molecules Vector of Molecule.
     * @return double 
     */
    virtual double GetEnergy (const std::vector<Molecule>& molecules) const override final;

    /**
     * @brief Get the energy from the FFNN.
     * 
     * @param molecules Vector of Molecule object pointers.
     * @return double 
     */
    virtual double GetEnergy (const std::vector<std::weak_ptr<Molecule>>& molecules) const override final;
};