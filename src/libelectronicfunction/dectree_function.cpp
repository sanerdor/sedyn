#include "dectree_function.h"

#include "../libmolecule/molecule.h"

#include <iomanip>


DecTreeFunction::DecTreeFunction (std::unique_ptr<DecTreeLayer>& tree_root)
: tree_root_(std::move(tree_root))
{}

double DecTreeFunction::GetEnergy (const std::vector<Molecule>& molecules) const
{
    Molecule target_molecule(molecules);

    arma::dmat cm = target_molecule.GetCoulombMatrix();
    arma::uvec uu_ind = arma::trimatl_ind(arma::size(cm), -1);
    arma::dvec data = cm(uu_ind);

    return tree_root_->GetDecision(data);
}

double DecTreeFunction::GetEnergy (const std::vector<std::weak_ptr<Molecule>>& molecules) const
{
    std::vector<Molecule> ref_molecules;
    for (const std::weak_ptr<Molecule>& mol : molecules) { ref_molecules.push_back(*mol.lock()); }

    return this->GetEnergy(ref_molecules);
}