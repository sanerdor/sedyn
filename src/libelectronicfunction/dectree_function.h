/**
 * @file dectree_function.h
 * @author Aitor Diaz-Andres (ad.andres@outlook.com)
 * @brief DecTreeFunction class declaration file.
 * @date 2024-05-20
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#pragma once

#include "base_electronic_function.h"
#include "dectree_layer.h"

#include <armadillo>
#include <memory>


/**
 * @brief Decision tree function to return the energy for a group of molecules.
 * 
 * This algorithm evaluates the input molecules (or single molecule) to return the energy. The decision tree algorithm
 * predicts a group for the target system and returns the value according to their group. The algorithm follows a simple
 * conditional evaluation and finds the solution suitable for the input. See
 * <a href="https://en.wikipedia.org/wiki/Decision_tree">Wikipedia</a>.
 */
class DecTreeFunction : public BaseElectronicFunction
{
private:
    std::shared_ptr<DecTreeLayer> tree_root_;                                  // 8

public:
    /**
     * @brief Construct a new DecTreeFunction object
     * 
     */
    DecTreeFunction () = default;

    /**
     * @brief Construct a new DecTreeFunction object.
     * 
     * The input DecTreeLayer is used as the root layer to predict the energy value.
     * 
     * @param tree_root DecTreeLayer object reference.
     */
    DecTreeFunction (std::unique_ptr<DecTreeLayer>& tree_root);

    /**
     * @brief Destroy the DecTreeFunction object.
     * 
     */
    ~DecTreeFunction () = default;

    /**
     * @brief Get the energy from the decision tree.
     * 
     * @param molecules Vector of Molecule.
     * @return double 
     */
    virtual double GetEnergy (const std::vector<Molecule>& molecules) const override final;

    /**
     * @brief Get the energy from the decision tree.
     * 
     * @param molecules Vector of Molecule pointers.
     * @return double 
     */
    virtual double GetEnergy (const std::vector<std::weak_ptr<Molecule>>& molecules) const override final;
};