/**
 * @file constant_function.h
 * @author Aitor Diaz-Andres (ad.andres@outlook.com)
 * @brief ConstantFunction class declaration.
 * @date 2024-05-20
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#pragma once

#include "base_electronic_function.h"


/**
 * @brief Constant function to return a constant energy.
 * 
 * This algorithm does not evalue the input molecules to return a value. The energy
 * (energy) is a constant parameter declared in the input file.
 *
 * @warning Equation not working.
 */
class ConstantFunction : public BaseElectronicFunction
{
private:
    double energy_;                                                             // 8

public:
    /**
     * @brief Construct a new ConstantFunction object.
     * 
     */
    ConstantFunction () = default;

    /**
     * @brief Construct a new ConstantFunction object.
     * 
     * @param energy Constant energy value.
     */
    ConstantFunction (double energy);

    /**
     * @brief Destroy the ConstantFunction object.
     * 
     */
    ~ConstantFunction () = default;

    /**
     * @brief Get the energy from a constant value.
     * 
     * @param molecules Vector of Molecule.
     * @return double 
     */
    virtual double GetEnergy (const std::vector<Molecule>& molecules) const override final;

    /**
     * @brief Get the energy from a constant value.
     * 
     * @param molecules Vector of Molecule pointers.
     * @return double 
     */
    virtual double GetEnergy (const std::vector<std::weak_ptr<Molecule>>& molecules) const override final;
};