/**
 * @file dectree_layer.h
 * @author Aitor Diaz-Andres (ad.andres@outlook.com)
 * @brief DecTreeLayer class declaration.
 * @date 2024-05-20
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#pragma once

#include <armadillo>
#include <memory>
#include <string>


enum class ConditionType
{
    Default,
    Equal,
    LessEqual,
    LessThan,
    GreaterEqual,
    GreaterThan
};

/**
 * @brief Decision Tree layer to return a value from a condition.
 * 
 * The decision tree layer evaluates a condition given an input vector and returns
 * a double (energy). However, if the return value is of type DecTreeLayer, this
 * layer is called to return its decision. The decistion tree layer will at the end
 * return a double corresponding to the predicted group energy.
 * 
 * @code {.markdown}
 *   2 <= 0.87              (DecTreeLayer)
 *     true: 38 <= 0.1      (DecTreeLayer)
 *       true: 0.8          (double)
 *       false: 0.02        (double)
 *     false: 0.0           (double)
 * @endcode
 */
class DecTreeLayer
{
private:
    double right_value_;                                                       // 8
    double false_value_;                                                       // 8
    double true_value_;                                                        // 8
    std::shared_ptr<DecTreeLayer> false_layer_;                                // 8
    std::shared_ptr<DecTreeLayer> true_layer_;                                 // 8
    bool (DecTreeLayer::*condition_) (const double, const double);             // 8
    int left_index_;                                                           // 4

public:
    /**
     * @brief Construct a new DecTreeLayer object.
     * 
     */
    DecTreeLayer () = default;

    /**
     * @brief Construct a new DecTreeLayer object.
     * 
     * @param right_value Comparison's right hand side.
     * @param false_value False return value.
     * @param true_value True return value.
     * @param false_layer False DecTreeLayer object reference.
     * @param true_layer True DecTreeLayer object reference.
     * @param left_index Comparison's left hand side.
     * @param condition Comparison string.
     */
    DecTreeLayer (double right_value, double false_value, double true_value, std::unique_ptr<DecTreeLayer>& false_layer,
                    std::unique_ptr<DecTreeLayer>& true_layer, int left_index, std::string condition);

    /**
     * @brief Destroy the DecTreeLayer object
     * 
     */
    ~DecTreeLayer () = default;

    /**
     * @brief Get the decision from this layer.
     * 
     * This function will be recursively called if the comparison has stored false_layer or true_layer.
     * The last layer in the chain will always return a double.
     * 
     * @return double 
     */
    double GetDecision (const arma::dvec&);

private:
    /**
     * @brief a == b ? true : false
     * 
     * @param a 
     * @param b 
     * @return true 
     * @return false 
     */
    bool Equal (const double a, const double b) { return a == b; }

    /**
     * @brief a <= b ? true : false
     * 
     * @param a 
     * @param b 
     * @return true 
     * @return false 
     */
    bool LessEqual (const double a, const double b) { return a <= b; }

    /**
     * @brief a < b ? true : false
     * 
     * @param a 
     * @param b 
     * @return true 
     * @return false 
     */
    bool LessThan (const double a, const double b) { return a < b; }

    /**
     * @brief a >= b ? true : false
     * 
     * @param a 
     * @param b 
     * @return true 
     * @return false 
     */
    bool GreaterEqual (const double a, const double b) { return a >= b; }

    /**
     * @brief a > b ? true : false
     * 
     * @param a 
     * @param b 
     * @return true 
     * @return false 
     */
    bool GreaterThan (const double a, const double b) { return a > b; }

    /**
     * @brief Saves in local DecTreeLayer::*condition_ variable the target conditional function.
     * 
     * @param condition String with the keyword to be interpreted.
     */
    void ParseCondition (const std::string condition);

    /**
     * @brief Return the ConditionType according to the string.
     * 
     * @param str String with the keyword to be interpreted.
     * @return ConditionType 
     */
    ConditionType StringToConditionEnum (const std::string str) const;
};