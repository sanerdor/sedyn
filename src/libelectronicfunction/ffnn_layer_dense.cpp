#include "ffnn_layer_dense.h"


FFNNLayerDense::FFNNLayerDense (arma::dmat weigths, arma::dvec biases, std::string act_func_type)
: weigths_(weigths), biases_(biases.t())
{
    switch (this->StringToActivationFunctionEnum(act_func_type))
    {
        case ActivationFunctionType::ActivationReLU:
        {
            activation_function_ = &FFNNLayerDense::ActivationReLU;
            break;
        }

        case ActivationFunctionType::ActivationSigmoid:
        {
            activation_function_ = &FFNNLayerDense::ActivationSigmoid;
            break;
        }

        default:
        {
            activation_function_ = &FFNNLayerDense::ActivationLinear;
            break;
        }
    }
}

//* Getters

int FFNNLayerDense::GetNInput () const { return weigths_.n_rows; }
int FFNNLayerDense::GetNOutput () const { return weigths_.n_cols; }

//* Public Methods

arma::rowvec FFNNLayerDense::Predict (const arma::rowvec& input)
{
    arma::rowvec output = input * weigths_ + biases_;

    return (this->*activation_function_)(output);
}

//* Private Methods

arma::rowvec FFNNLayerDense::ActivationLinear (const arma::rowvec& input)
{
    return input;
}

arma::rowvec FFNNLayerDense::ActivationReLU (const arma::rowvec& input)
{
    arma::rowvec output = input;
    output.for_each( [](double& val)
        { if (val < 1e-8) { val = 0; } } );

    return output;
}

arma::rowvec FFNNLayerDense::ActivationSigmoid (const arma::rowvec& input)
{
    arma::rowvec output = input;
    output = 1 / (1 + arma::exp(-output));

    return output;
}

ActivationFunctionType FFNNLayerDense::StringToActivationFunctionEnum (const std::string str) const
{
    if (str == "relu") { return ActivationFunctionType::ActivationReLU; }
    if (str == "sigmoid") { return ActivationFunctionType::ActivationSigmoid; }

    return ActivationFunctionType::Default;
}