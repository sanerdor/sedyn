#include "dectree_layer.h"


DecTreeLayer::DecTreeLayer (double right_value, double false_value, double true_value, std::unique_ptr<DecTreeLayer>& false_layer,
                            std::unique_ptr<DecTreeLayer>& true_layer, int left_index, std::string condition)
: right_value_(right_value), false_value_(false_value), true_value_(true_value), false_layer_(std::move(false_layer)), 
    true_layer_(std::move(true_layer)), left_index_(left_index)
{
    this->ParseCondition(condition);
}

double DecTreeLayer::GetDecision (const arma::dvec& data)
{
    bool is_true = (this->*condition_)(data(left_index_), right_value_);

    if (is_true)
    {
        if (true_layer_ ) { return true_layer_->GetDecision(data); }
        else { return true_value_; }
    } else
    {
        if (false_layer_ ) { return false_layer_->GetDecision(data); }
        else { return false_value_; }
    }
}

void DecTreeLayer::ParseCondition (const std::string condition)
{
    switch (this->StringToConditionEnum(condition))
    {
        case ConditionType::Equal:
        {
            condition_ = &DecTreeLayer::Equal;
            break;
        }

        case ConditionType::LessEqual:
        {
            condition_ = &DecTreeLayer::LessEqual;
            break;
        }

        case ConditionType::LessThan:
        {
            condition_ = &DecTreeLayer::LessThan;
            break;
        }

        case ConditionType::GreaterEqual:
        {
            condition_ = &DecTreeLayer::GreaterEqual;
            break;
        }

        case ConditionType::GreaterThan:
        {
            condition_ = &DecTreeLayer::GreaterThan;
            break;
        }

        default:
            break;
    }
}

ConditionType DecTreeLayer::StringToConditionEnum (const std::string str) const
{
    if (str == "eq") { return ConditionType::Equal; }
    if (str == "le") { return ConditionType::LessEqual; }
    if (str == "lt") { return ConditionType::LessThan; }
    if (str == "ge") { return ConditionType::GreaterEqual; }
    if (str == "gt") { return ConditionType::GreaterThan; }

    return ConditionType::Default;
}