/**
 * @file global_functions.h
 * @author Aitor Diaz-Andres (ad.andres@outlook.com)
 * @brief File containing global functions
 * @date 2024-05-20
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#pragma once

#include <armadillo>
#include <memory>
#include <vector>

static arma::dvec NearImageTransform (const arma::dvec& com1, const arma::dvec& com2, const arma::uvec& boundaries,
                                        const arma::dvec& cell_size)
{
    arma::dvec distance = com2 - com1;
    arma::dvec transform = arma::dvec(3, arma::fill::zeros);

    for (size_t i = 0; i < 3; i++)
    {
        if (boundaries(i) == 1 && std::abs(distance(i)) > cell_size(i)/2)
        {
            if (distance(i) >= 0) { transform(i) = cell_size(i); }
            else { transform(i) = -cell_size(i); }
        }
    }

    return transform;
}

template <typename T>
static bool VectorsIncluded (const std::vector<T>& va, const std::vector<T>& vb)
{
    if (va.size() != vb.size()) { return false; }
    
    for (const T& a : va)
    {
        bool is_included = false;
        for (const T& b : vb)
        {
            if (a == b)
            {
                is_included = true;
                break;
            }
        }

        if (!is_included) { return false; }
    }

    for (const T& a : vb)
    {
        bool is_included = false;
        for (const T& b : va)
        {
            if (a == b)
            {
                is_included = true;
                break;
            }
        }

        if (!is_included) { return false; }
    }

    return true;
}

template <typename T>
static bool VectorsIncluded (const std::vector<std::weak_ptr<T>>& va,
                                const std::vector<std::weak_ptr<T>>& vb)
{
    if (va.size() != vb.size()) { return false; }
    
    for (const std::weak_ptr<T>& a : va)
    {
        bool is_included = false;
        for (const std::weak_ptr<T>& b : vb)
        {
            if (a.lock() == b.lock())
            {
                is_included = true;
                break;
            }
        }

        if (!is_included) { return false; }
    }

    for (const std::weak_ptr<T>& a : vb)
    {
        bool is_included = false;
        for (const std::weak_ptr<T>& b : va)
        {
            if (a.lock() == b.lock())
            {
                is_included = true;
                break;
            }
        }

        if (!is_included) { return false; }
    }

    return true;
}

/* static void UniqueCombinations (arma::umat& combinations, int size, int req_size)
{
    int max_comb = 0.5 * size * (size - 1);
    int n_comb = req_size * (size - 0.5 * (req_size + 1));
    combinations = arma::umat(2, n_comb, arma::fill::zeros);

    size_t curr_pos = 0;
    for (size_t i = 0; i < size-1; i++)
    {
        arma::uvec rspace = arma::regspace<arma::uvec>(i+1, 1, size-1);
        combinations(0, arma::span(curr_pos, curr_pos+rspace.n_rows-1)).fill(i);
        combinations(1, arma::span(curr_pos, curr_pos+rspace.n_rows-1)) = rspace.t();

        curr_pos += rspace.n_rows;
    }
} */