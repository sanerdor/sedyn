#pragma once

#include "hamiltonian_vector.h"

#include <complex>
#include <memory>
#include <vector>


class HamiltonianBasis
{
public:
    //* Constructors
    HamiltonianBasis () = default;
    HamiltonianBasis (std::vector<HamiltonianVector>, std::complex<double>);
    HamiltonianBasis (const HamiltonianBasis&);
    HamiltonianBasis (HamiltonianBasis&&);

    //* Destructors
    ~HamiltonianBasis () = default;

    //* Operator Overloading
    double operator* (const HamiltonianBasis&) const;

    //* Setters
    void SetCoefficient (std::complex<double>);

    //* Public Methods
    void ComputePM ();
    void ComputeMP ();
    void ComputeZZ ();

private:
    //* Private Variables
    std::vector<HamiltonianVector> vectors_;                                   // 24
    std::complex<double> coefficient_;                                         // 16
};