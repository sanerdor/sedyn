#include "hamiltonian.h"

#include "hamiltonian_vector.h"
#include "vector_basis.h"
#include "../global_namespaces.h"
#include "../global_objects.h"
#include "../libsystem/system.h"


//* Constructors

Hamiltonian::Hamiltonian (std::unique_ptr<BaseElectronicFunction>& exchange_function, std::string character_function)
: character_coefficient_(arma::dvec(3, arma::fill::zeros)), exchange_function_(std::move(exchange_function))
{
    switch (this->StringToHamiltonianProbabilityTypeEnum(character_function))
    {
        case HamiltonianProbabilityType::BoltzmannProbability:
        {
            character_function_ = &Hamiltonian::BoltzmannProbability;
            break;
        }

        case HamiltonianProbabilityType::CharacterProbability:
        {
            character_function_ = &Hamiltonian::CharacterProbability;
            break;
        }
        
        default:
        {
            character_function_ = &Hamiltonian::CharacterProbability;
            break;
        }
    }

    /* The Hamiltonian is defined by the interaction of two triplet states to form a TT state.
        The dimension is fixed to nine, this means, nine basis functions. */
    
    this->BuildHamiltonian();
    this->BuildPureSpinVectors();
}

Hamiltonian::Hamiltonian (const Hamiltonian& other)
: character_coefficient_(arma::dvec(3, arma::fill::zeros)), character_function_(other.character_function_),
    exchange_function_(other.exchange_function_)
{
    this->BuildHamiltonian();
    this->BuildPureSpinVectors();
}

//* Operator overloading

bool Hamiltonian::operator== (const Hamiltonian& other) const
{
    if (character_function_ != other.character_function_ ||
        exchange_function_ != other.exchange_function_)
    {
        return false;
    }

    return true;
}

bool Hamiltonian::operator!= (const Hamiltonian& other) const
{
    return !(*this == other);
}

//* Public Methods

double Hamiltonian::GetProcessCoefficient (const int coefficient) const
{
    return character_coefficient_.at(coefficient);
}

double Hamiltonian::UpdateHamiltonianState (const System& system, const double state_energy, const std::vector<std::weak_ptr<Molecule>>& molecules)
{
    int dim = 9;

    //* Build Hamiltonian
    arma::dmat H = arma::dmat(dim, dim, arma::fill::eye) * state_energy;
    if (exchange_function_ != nullptr)
    {
        double exchange_energy = exchange_function_->GetEnergy(molecules);
        this->ComputeExchangeHamiltonian(H, exchange_energy);
    }

    //* Diagonalize Hamiltonian
    arma::dvec eigval;
    arma::dmat eigvec;
    arma::eig_sym(eigval, eigvec, H, "std");
    eigval = arma::real(eigval);
    eigvec = arma::real(eigvec);

    //* Clean eigenvector and eigenvalues from noise values
    eigval.for_each( [] (double& val) { if (std::abs(val) < 1e-8) { val = 0.0; } } );
    eigvec.for_each( [] (double& val) { if (std::abs(val) < 1e-8) { val = 0.0; } } );

    //* Build eigenvector system
    std::vector<std::vector<HamiltonianBasis>> eigvec_system;
    for (size_t i = 0; i < dim; i++)
    {
        std::vector<HamiltonianBasis> e_vec;
        for (size_t j = 0; j < dim; j++)
        {
            double coefficient = eigvec(j, i);
            if (std::abs(coefficient) < 1e-8) { continue; }

            HamiltonianBasis h_basis(hamiltonian_basis_[j]);
            h_basis.SetCoefficient(std::complex<double>(coefficient, 0.0));
            e_vec.push_back(std::move(h_basis));
        }
        eigvec_system.push_back(std::move(e_vec));
    }

    //* Get Hamiltonian Character (This is prestablished at construction)
    //int character = (this->*character_function_)();
    int character = 0;

    //* Define Hamiltonian State Character
    character_coefficient_.fill(0.0);
    double energy = eigval(character);
    const std::vector<HamiltonianBasis>& evec_sys_character = eigvec_system[character];

    // Singlet Character
    for (const std::vector<HamiltonianBasis>& singlet : pure_singlet_)
    {
        double coefficient = 0.0;
        for (const HamiltonianBasis& bra : singlet)
        {
            for (const HamiltonianBasis& ket : evec_sys_character)
            {
                coefficient += bra * ket;
            }
        }
        character_coefficient_(0) += std::pow(coefficient, 2);
    }

    // Triplet Character
    for (const std::vector<HamiltonianBasis>& triplet : pure_triplet_)
    {
        double coefficient = 0.0;
        for (const HamiltonianBasis& bra : triplet)
        {
            for (const HamiltonianBasis& ket : evec_sys_character)
            {
                coefficient += bra * ket;
            }
        }
        character_coefficient_(1) += std::pow(coefficient, 2);
    }

    // Quintet Character
    for (const std::vector<HamiltonianBasis>& quintet : pure_quintet_)
    {
        double coefficient = 0.0;
        for (const HamiltonianBasis& bra : quintet)
        {
            for (const HamiltonianBasis& ket : evec_sys_character)
            {
                coefficient += bra * ket;
            }
        }
        character_coefficient_(2) += std::pow(coefficient, 2);
    }

    character_coefficient_.for_each([] (double& val) { if (std::abs(val) < 1e-8) { val = 0.0; } });

    return energy;
}

//* Private Methods

int Hamiltonian::BoltzmannProbability (const System& system, const arma::dvec& eigval)
{
    double T = system.GetTemperature();
    
    arma::dvec ev = arma::exp(-eigval/(ncon::kb * T));
    arma::dvec prob = arma::pow(ev / arma::norm(ev), 2);

    std::uniform_real_distribution<double> r_dist(0.0, 1.0);
    double r_val = arma::sum(prob) * r_dist(GlobalObjects::randomizer);

    int chosen_character = -1;
    arma::dvec cumsum = arma::cumsum(prob);
    for (size_t i = 0; i < cumsum.n_rows; i++)
    {
        if (r_val < cumsum(i))
        {
            chosen_character = i;
            break;
        }
    }

    if (chosen_character > 0 && chosen_character < 4) { chosen_character = 2; }
    else if (chosen_character > 4) { chosen_character = 2; }

    // TODO : Sanerdor : Temporal fix for non character = 2 solution
    if (chosen_character == 2) { chosen_character = 1; }

    return chosen_character;
}

void Hamiltonian::BuildHamiltonian ()
{
    VectorBasis b1(1, -1);     // |1,-1>
    VectorBasis b2(1, 1);      // |1,1>
    VectorBasis b3(1, 0);      // |1,0>

    // Vector x 
    std::vector<CoefficientVectorBasis> x_vector;
    
    CoefficientVectorBasis x_b1;
    x_b1.coefficient_ = std::complex<double>(1.0 / std::sqrt(2), 0.0);
    x_b1.vector_basis_ = b1;
    x_vector.push_back(std::move(x_b1));

    CoefficientVectorBasis x_b2;
    x_b2.coefficient_ = std::complex<double>(-1.0 / std::sqrt(2), 0.0);
    x_b2.vector_basis_ = b2;
    x_vector.push_back(std::move(x_b2));

    HamiltonianVector xa(x_vector, 'a');
    HamiltonianVector xb(x_vector, 'b');

    // Vector y
    std::vector<CoefficientVectorBasis> y_vector;
    
    CoefficientVectorBasis y_b1;
    y_b1.coefficient_ = std::complex<double>(0.0, 1.0 / std::sqrt(2));
    y_b1.vector_basis_ = b1;
    y_vector.push_back(std::move(y_b1));

    CoefficientVectorBasis y_b2;
    y_b2.coefficient_ = std::complex<double>(0.0, 1.0 / std::sqrt(2));
    y_b2.vector_basis_ = b2;
    y_vector.push_back(std::move(y_b2));

    HamiltonianVector ya(y_vector, 'a');
    HamiltonianVector yb(y_vector, 'b');

    // Vector z
    std::vector<CoefficientVectorBasis> z_vector;
    
    CoefficientVectorBasis z_b3;
    z_b3.coefficient_ = std::complex<double>(1.0, 0.0);
    z_b3.vector_basis_ = b3;
    z_vector.push_back(std::move(z_b3));

    HamiltonianVector za(z_vector, 'a');
    HamiltonianVector zb(z_vector, 'b');

    // Hamiltonian basis
    hamiltonian_basis_.push_back(HamiltonianBasis(std::vector<HamiltonianVector>{xa,xb}, std::complex<double>(1,0)));
    hamiltonian_basis_.push_back(HamiltonianBasis(std::vector<HamiltonianVector>{xa,yb}, std::complex<double>(1,0)));
    hamiltonian_basis_.push_back(HamiltonianBasis(std::vector<HamiltonianVector>{xa,zb}, std::complex<double>(1,0)));
    hamiltonian_basis_.push_back(HamiltonianBasis(std::vector<HamiltonianVector>{ya,xb}, std::complex<double>(1,0)));
    hamiltonian_basis_.push_back(HamiltonianBasis(std::vector<HamiltonianVector>{ya,yb}, std::complex<double>(1,0)));
    hamiltonian_basis_.push_back(HamiltonianBasis(std::vector<HamiltonianVector>{ya,zb}, std::complex<double>(1,0)));
    hamiltonian_basis_.push_back(HamiltonianBasis(std::vector<HamiltonianVector>{za,xb}, std::complex<double>(1,0)));
    hamiltonian_basis_.push_back(HamiltonianBasis(std::vector<HamiltonianVector>{za,yb}, std::complex<double>(1,0)));
    hamiltonian_basis_.push_back(HamiltonianBasis(std::vector<HamiltonianVector>{za,zb}, std::complex<double>(1,0)));
}

void Hamiltonian::BuildPureSpinVectors ()
{
    arma::dmat H = arma::dmat(9, 9, arma::fill::zeros);
    this->ComputeExchangeHamiltonian(H, 1.0);

    arma::dvec eigval;
    arma::dmat eigvec;
    arma::eig_sym(eigval, eigvec, H, "std");
    eigval = arma::real(eigval);
    eigvec = arma::real(eigvec);

    for (size_t i = 0; i < 9; i++)
    {
        std::vector<HamiltonianBasis> e_vec;
        for (size_t j = 0; j < 9; j++)
        {
            double coefficient = eigvec(j, i);
            if (std::abs(coefficient) < 1e-8) { continue; }

            HamiltonianBasis h_basis(hamiltonian_basis_[j]);
            h_basis.SetCoefficient(std::complex<double>(coefficient, 0.0));
            e_vec.push_back(std::move(h_basis));
        }

        if (round(eigval(i)) == -2) { pure_singlet_.push_back(std::move(e_vec)); }
        else if (round(eigval(i)) == -1) { pure_triplet_.push_back(std::move(e_vec)); }
        else if (round(eigval(i)) == 1) { pure_quintet_.push_back(std::move(e_vec)); }
    }
}

int Hamiltonian::CharacterProbability (const System& system, const arma::dvec& eigval)
{
    std::uniform_int_distribution<int> i_dist(0, 1);

    return i_dist(GlobalObjects::randomizer);
}

void Hamiltonian::ComputeExchangeHamiltonian (arma::dmat& H, const double energy) const
{
    arma::dmat exchange = arma::dmat(9, 9, arma::fill::zeros);
    for (size_t i = 0; i < 9; i++)
    {
        for (size_t j = 0; j < 9; j++)
        {
            double result = 0;
            std::vector<HamiltonianBasis> operated_ket;

            // Sa+ Sb-
            HamiltonianBasis pm_ket(hamiltonian_basis_[j]);
            pm_ket.ComputePM();
            operated_ket.push_back(std::move(pm_ket));

            // Sa- Sb+
            HamiltonianBasis mp_ket(hamiltonian_basis_[j]);
            mp_ket.ComputeMP();
            operated_ket.push_back(std::move(mp_ket));

            // Saz Sbz
            HamiltonianBasis zz_ket(hamiltonian_basis_[j]);
            zz_ket.ComputeZZ();
            operated_ket.push_back(std::move(zz_ket));

            for (const HamiltonianBasis& ket : operated_ket)
            {
                result += hamiltonian_basis_[i] * ket;
            }

            exchange(i, j) = result;
        }
    }

    H += exchange * energy;
}

HamiltonianProbabilityType Hamiltonian::StringToHamiltonianProbabilityTypeEnum (const std::string str) const
{
    if (str == "boltzmann") { return HamiltonianProbabilityType::BoltzmannProbability; }
    if (str == "character") { return HamiltonianProbabilityType::CharacterProbability; }

    return HamiltonianProbabilityType::Default;
}