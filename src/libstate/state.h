#pragma once

#include "rate_state_process.h"
#include "hamiltonian.h"
#include "../libelectronicfunction/base_electronic_function.h"
#include "../libmolecule/molecule.h"
#include "../libprocessfunction/base_process_function.h"
#include "../libtransitionfunction/transition_function.h"
#include "../libmanager/error_manager.h"

#include <memory>
#include <string>
#include <vector>


class Hamiltonian;
class Molecule;
class RateStateProcess;
class System;

class State
{
public:
    //* Constructor
    State () = default;
    State (std::string, std::vector<std::unique_ptr<BaseProcessFunction>>&, std::unique_ptr<BaseElectronicFunction>&,
            std::vector<std::unique_ptr<TransitionFunction>>&, std::unique_ptr<Hamiltonian>&, int, int,
            ErrorManager* error_manager_);
    State (const State&);

    //* Destructor
    ~State () = default;

    //* Operator overloading
    bool operator== (const State&) const;
    bool operator!= (const State&) const;

    //* Getters
    int GetInitialNumber () const;
    double GetEnergy () const;
    std::string GetLabel () const;
    std::vector<std::weak_ptr<Molecule>> GetMolecules () const;
    int GetMolSize () const { return molecules_.size(); }
    int GetSize () const;

    //* Setters
    void SetMolecule (const std::shared_ptr<Molecule>&);
    void SetMolecule (const std::vector<std::shared_ptr<Molecule>>&);
    void SetMolecule (const std::vector<std::weak_ptr<Molecule>>&);

    //* Public Methods
    std::vector<RateStateProcess> GetAllowedProcesses (System& system) const;
    double GetTransitionReorgEnergy (std::string) const;
    bool IsIsolated () const;
    void Update (const System& system);

private:
    //* Variables
    std::string label_;                                                        // 32
    std::vector<std::shared_ptr<BaseProcessFunction>> process_list_;           // 24
    std::shared_ptr<BaseElectronicFunction> electronic_function_;              // 8
    std::vector<std::shared_ptr<TransitionFunction>> transition_list_;         // 24
    double energy_;                                                            // 8
    std::vector<std::weak_ptr<Molecule>> molecules_;                           // 24
    std::unique_ptr<Hamiltonian> hamiltonian_;                                 // 8
    int initial_num_;                                                          // 4
    int size_;                                                                 // 4
    ErrorManager* error_manager_;
};