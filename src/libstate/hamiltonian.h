#pragma once

#include "../libelectronicfunction/base_electronic_function.h"
#include "hamiltonian_basis.h"
#include "../libmolecule/molecule.h"
#include "../libprocessfunction/base_process_function.h"

#include <armadillo>
#include <memory>
#include <string>


class System;

enum class HamiltonianProbabilityType
{
    Default,
    BoltzmannProbability,
    CharacterProbability
};

class Hamiltonian
{
public:
    //* Constructors
    Hamiltonian () = default;
    Hamiltonian (std::unique_ptr<BaseElectronicFunction>&, std::string);
    Hamiltonian (const Hamiltonian&);

    //* Destructors
    ~Hamiltonian () = default;

    //* Operator overloading
    bool operator== (const Hamiltonian&) const;
    bool operator!= (const Hamiltonian&) const;

    //* Public Methods
    double GetProcessCoefficient (const int) const;
    double UpdateHamiltonianState (const System&, const double, const std::vector<std::weak_ptr<Molecule>>&);

private:
    //* Private Variables
    arma::dvec character_coefficient_;                                         // 192
    std::vector<HamiltonianBasis> hamiltonian_basis_;                          // 24
    std::vector<std::vector<HamiltonianBasis>> pure_singlet_;                  // 24
    std::vector<std::vector<HamiltonianBasis>> pure_triplet_;                  // 24
    std::vector<std::vector<HamiltonianBasis>> pure_quintet_;                  // 24
    int (Hamiltonian::*character_function_) (const System&, const arma::dvec&);// 8
    std::shared_ptr<BaseElectronicFunction> exchange_function_;                // 8
    
    //* Private Methods
    int BoltzmannProbability (const System& system, const arma::dvec&);
    void BuildHamiltonian ();
    void BuildPureSpinVectors ();
    int CharacterProbability (const System& system, const arma::dvec&);
    void ComputeExchangeHamiltonian (arma::dmat&, const double) const;
    HamiltonianProbabilityType StringToHamiltonianProbabilityTypeEnum (const std::string) const;
};