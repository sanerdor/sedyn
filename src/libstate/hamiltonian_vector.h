#pragma once

#include "vector_basis.h"

#include <memory>
#include <vector>


struct CoefficientVectorBasis
{
    std::complex<double> coefficient_;                                         // 16
    VectorBasis vector_basis_;                                                 // 8
};

class HamiltonianVector
{
public:
    //* Constructors
    HamiltonianVector () = default;
    HamiltonianVector (std::vector<CoefficientVectorBasis>, char);
    HamiltonianVector (const HamiltonianVector&);
    HamiltonianVector (HamiltonianVector&&);

    //* Destructors
    ~HamiltonianVector () = default;

    //* Operator Overloading
    std::complex<double> operator* (const HamiltonianVector&) const;

    //* Getters
    char GetSite () const;

    //* Public Methods
    void SP ();
    void SM ();
    void SZ ();

private:
    //* Private Variables
    std::vector<CoefficientVectorBasis> basis_functions_;                      // 24
    char site_;                                                                // 1
};