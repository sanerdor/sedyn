#pragma once

#include "state.h"
#include "../libmolecule/molecule.h"
#include "../libprocessfunction/base_process_function.h"

#include <armadillo>
#include <memory>
#include <string>
#include <vector>


class BaseProcessFunction;
class Molecule;
class State;

class RateStateProcess
{
public:
    //* Constructor
    RateStateProcess () = default;
    RateStateProcess (arma::dvec, std::vector<std::string>, std::shared_ptr<Molecule>, std::weak_ptr<BaseProcessFunction>,
                        std::shared_ptr<State>, double, double);
    RateStateProcess (const RateStateProcess&);

    //* Destructor
    ~RateStateProcess () = default;

    //* Operator Overloading
    bool operator== (const RateStateProcess&) const;

    //* Getters
    std::weak_ptr<Molecule> GetInteractionMolecule () const;
    arma::dvec GetPeriodicTranformation () const;
    double GetRate () const;
    std::weak_ptr<State> GetState () const;
    std::vector<std::string> GetTargetStateLabels () const;
    double GetTime () const;

    //* Setters
    void SetState (const std::shared_ptr<State>&);
    void SetTime (const double);

private:
    //* Private Variables
    arma::dvec periodic_transformation_;                                       // 192
    std::vector<std::string> target_state_labels_;                             // 24
    std::shared_ptr<Molecule> interaction_molecule_;                             // 8
    std::weak_ptr<BaseProcessFunction> process_;                               // 8
    std::shared_ptr<State> state_;                                             // 8
    double time_;                                                              // 8
    double rate_;                                                              // 8
};