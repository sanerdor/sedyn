#include "rate_state_process.h"

#include "../global_functions.h"


//* Constructor

RateStateProcess::RateStateProcess (arma::dvec perioric_transformation, std::vector<std::string> target_state_labels,
                                    std::shared_ptr<Molecule> interaction_molecule, std::weak_ptr<BaseProcessFunction> process,
                                    std::shared_ptr<State> state, double time, double rate)
: periodic_transformation_(perioric_transformation), target_state_labels_(target_state_labels),
    interaction_molecule_(interaction_molecule), process_(process), state_(state), time_(time), rate_(rate)
{
}

RateStateProcess::RateStateProcess (const RateStateProcess& other)
: periodic_transformation_(other.periodic_transformation_), target_state_labels_(other.target_state_labels_),
    interaction_molecule_(other.interaction_molecule_), process_(other.process_), state_(other.state_), time_(other.time_),
    rate_(other.rate_)
{
}

//* Operator Overloading

bool RateStateProcess::operator== (const RateStateProcess& other) const
{
    if (!VectorsIncluded<std::string>(target_state_labels_, other.target_state_labels_) ||
        interaction_molecule_ != other.interaction_molecule_ ||
        process_.lock() != other.process_.lock() ||
        state_ != other.state_ ||
        time_ != other.time_ ||
        rate_ != other.rate_)
    {
        return false;
    }

    return true;
}

//* Getters

std::weak_ptr<Molecule> RateStateProcess::GetInteractionMolecule () const { return interaction_molecule_; }
arma::dvec RateStateProcess::GetPeriodicTranformation () const { return periodic_transformation_; }
double RateStateProcess::GetRate () const { return rate_; }
std::weak_ptr<State> RateStateProcess::GetState () const { return state_; }
std::vector<std::string> RateStateProcess::GetTargetStateLabels () const { return target_state_labels_; }
double RateStateProcess::GetTime () const { return time_; }

//* Setters

void RateStateProcess::SetState (const std::shared_ptr<State>& state) { state_ = state; }
void RateStateProcess::SetTime (const double time) { time_ = time; }