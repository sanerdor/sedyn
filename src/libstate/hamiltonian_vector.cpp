#include "hamiltonian_vector.h"

#include <algorithm>


//* Constructors

HamiltonianVector::HamiltonianVector (std::vector<CoefficientVectorBasis> basis_functions, char site)
: site_(site)
{
    for (CoefficientVectorBasis& b_function : basis_functions) { basis_functions_.push_back(b_function); }
}

HamiltonianVector::HamiltonianVector (const HamiltonianVector& other)
: basis_functions_(other.basis_functions_), site_(other.site_)
{
}

HamiltonianVector::HamiltonianVector (HamiltonianVector&& other)
: basis_functions_(std::move(other.basis_functions_)), site_(other.site_)
{
}

//* Operator Overloading

std::complex<double> HamiltonianVector::operator* (const HamiltonianVector& other) const
{
    std::complex<double> result (0, 0);
    for (const CoefficientVectorBasis& t_vb : basis_functions_)
    {
        for (const CoefficientVectorBasis& o_vb : other.basis_functions_)
        {
            result += std::conj(t_vb.coefficient_) * o_vb.coefficient_ * (t_vb.vector_basis_ * o_vb.vector_basis_);
        }
    }

    return result;
}

//* Getters

char HamiltonianVector::GetSite () const { return site_; }

//* Public Methods

void HamiltonianVector::SP ()
{
    basis_functions_.erase(std::remove_if(basis_functions_.begin(), basis_functions_.end(),
        [] (CoefficientVectorBasis& coeff_basis)
        {
            double constant = coeff_basis.vector_basis_.SP();
            if (std::abs(constant) < 1e-8) { return true; }
            coeff_basis.coefficient_ *= constant;
            return false;
        }), basis_functions_.end());
}

void HamiltonianVector::SM ()
{
    basis_functions_.erase(std::remove_if(basis_functions_.begin(), basis_functions_.end(),
        [] (CoefficientVectorBasis& coeff_basis)
        {
            double constant = coeff_basis.vector_basis_.SM();
            if (std::abs(constant) < 1e-8) { return true; }
            coeff_basis.coefficient_ *= constant;
            return false;
        }), basis_functions_.end());
}

void HamiltonianVector::SZ ()
{
    basis_functions_.erase(std::remove_if(basis_functions_.begin(), basis_functions_.end(),
        [] (CoefficientVectorBasis& coeff_basis)
        {
            double constant = coeff_basis.vector_basis_.SZ();
            if (std::abs(constant) < 1e-8) { return true; }
            coeff_basis.coefficient_ *= constant;
            return false;
        }), basis_functions_.end());
}