#include "vector_basis.h"


//* Constructors

VectorBasis::VectorBasis (int j, int m)
: j_(j), m_(m)
{
}

//* Operator Overloading

bool VectorBasis::operator== (const VectorBasis& other) const
{
    if (j_ != other.j_ || m_ != other.m_)
    {
        return false;
    }

    return true;
}

bool VectorBasis::operator!= (const VectorBasis& other) const
{
    return !(*this == other);
}

std::complex<double> VectorBasis::operator* (const VectorBasis& other) const
{
    if (*this == other) { return std::complex<double> (1,0); }
    else { return std::complex<double> (0,0); }
}

//* Public Methods

double VectorBasis::SP ()
{
    if (m_ > std::max(-j_, j_)) { return 0.0; }
    double coefficient = std::sqrt(j_*(j_+1) - m_*(m_+1));
    m_++;

    return coefficient;
}

double VectorBasis::SM ()
{
    if (m_ < std::min(-j_, j_)) { return 0.0; }
    double coefficient = std::sqrt(j_*(j_+1) - m_*(m_-1));
    m_--;
    
    return coefficient;
}

double VectorBasis::SZ ()
{
    return m_;
}