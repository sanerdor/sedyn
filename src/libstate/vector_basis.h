#pragma once

#include <complex>


class VectorBasis
{
public:
    //* Constructors
    VectorBasis () = default;
    VectorBasis (int, int);

    //* Destructors
    ~VectorBasis () = default;

    //* Operator Overloading
    bool operator== (const VectorBasis&) const;
    bool operator!= (const VectorBasis&) const;
    std::complex<double> operator* (const VectorBasis&) const;

    //* Public Methods
    double SP ();
    double SM ();
    double SZ ();

private:
    //* Private Variables
    int j_;
    int m_;
};