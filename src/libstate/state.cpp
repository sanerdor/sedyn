#include "state.h"

#include "../global_functions.h"
#include "../libmanager/error_manager.h"
#include "../libsystem/system.h"

#include <algorithm>


//* Constructor

State::State (std::string label, std::vector<std::unique_ptr<BaseProcessFunction>>& process_list,
                std::unique_ptr<BaseElectronicFunction>& electronic_function,
                std::vector<std::unique_ptr<TransitionFunction>>& transition_list,
                std::unique_ptr<Hamiltonian>& hamiltonian, int initial_num, int size,
                ErrorManager* error_manager)
: label_(label), electronic_function_(std::move(electronic_function)), energy_(0.0), initial_num_(initial_num), size_(size),
    error_manager_(error_manager)
{
    if (hamiltonian != nullptr) { hamiltonian_ = std::move(hamiltonian); }

    for (std::unique_ptr<BaseProcessFunction>& ptr : process_list) { process_list_.push_back(std::move(ptr)); }
    for (std::unique_ptr<TransitionFunction>& ptr : transition_list) { transition_list_.push_back(std::move(ptr)); }
}

State::State (const State& other)
: label_(other.label_), electronic_function_(other.electronic_function_), energy_(0.0), initial_num_(other.initial_num_),
    size_(other.size_), error_manager_(other.error_manager_)
{
    if (other.hamiltonian_ != nullptr) { hamiltonian_ = std::make_unique<Hamiltonian>(*other.hamiltonian_); }

    for (const std::shared_ptr<BaseProcessFunction>& ptr : other.process_list_) { process_list_.push_back(ptr); }
    for (const std::shared_ptr<TransitionFunction>& ptr : other.transition_list_) { transition_list_.push_back(ptr); }
    for (const std::weak_ptr<Molecule>& ptr : other.molecules_) { molecules_.push_back(ptr); }
}

//* Operator overloading

bool State::operator== (const State& other) const
{
    if (label_ != other.label_ ||
        !VectorsIncluded<std::shared_ptr<BaseProcessFunction>>(process_list_, other.process_list_) ||
        electronic_function_ != other.electronic_function_ ||
        !VectorsIncluded<std::shared_ptr<TransitionFunction>>(transition_list_, other.transition_list_) ||
        !VectorsIncluded<Molecule>(molecules_, other.molecules_) ||
        hamiltonian_ != other.hamiltonian_ ||
        initial_num_ != other.initial_num_ ||
        size_ != other.size_)
    {
        return false;
    }

    return true;
}

bool State::operator!= (const State& other) const
{
    return !(*this == other);
}

//* Getters

int State::GetInitialNumber () const { return initial_num_; }
double State::GetEnergy () const { return energy_; }
std::string State::GetLabel () const { return label_; }
std::vector<std::weak_ptr<Molecule>> State::GetMolecules () const { return molecules_; }
int State::GetSize () const { return size_; }

//* Setters

void State::SetMolecule (const std::shared_ptr<Molecule>& molecule)
{
    molecules_.clear();
    molecules_.push_back(molecule);
}

void State::SetMolecule (const std::vector<std::shared_ptr<Molecule>>& molecules)
{
    molecules_.clear();
    for (const std::shared_ptr<Molecule>& molecule : molecules) { molecules_.push_back(molecule); }
}

void State::SetMolecule (const std::vector<std::weak_ptr<Molecule>>& molecules)
{
    molecules_.clear();
    for (const std::weak_ptr<Molecule>& molecule : molecules) { molecules_.push_back(molecule); }
}

void State::Update (const System& system)
{
    energy_ = electronic_function_->GetEnergy(molecules_);
    if (hamiltonian_ != nullptr)
    {
        energy_ = hamiltonian_->UpdateHamiltonianState(system, energy_, molecules_);
    }
}

//* Public Methods

std::vector<RateStateProcess> State::GetAllowedProcesses (System& system) const
{
    std::vector<RateStateProcess> allowed_processes;
    for (const std::shared_ptr<BaseProcessFunction>& process : process_list_)
    {
        //* Need to include Hamiltonian information
        double hamiltonian_coefficient = 1.0;
        int process_hamiltonian_coefficient = process->GetHamiltonianCoefficient();
        if (hamiltonian_ != nullptr && process_hamiltonian_coefficient != -1)
        {
            hamiltonian_coefficient = hamiltonian_->GetProcessCoefficient(process_hamiltonian_coefficient);
        }

        //* Check hamiltonian_coefficient larger than zero
        if (hamiltonian_coefficient < 1e-8) { continue; }

        bool do_interact = process->GetInteractionStateSize() == 0 ? false : true;
        for (std::vector<std::weak_ptr<Molecule>>::const_iterator mol_it = molecules_.begin(); mol_it != molecules_.end(); mol_it++)
        {
            const Molecule& mol = *(*mol_it).lock();

            if (do_interact)
            {
                for (const std::weak_ptr<Molecule>& neighbour : mol.GetNeighbours())
                {
                    const Molecule& neigh = *neighbour.lock();

                    //* Check neighbour is not in molecules_
                    auto it = std::find_if(molecules_.begin(), molecules_.end(),
                        [&] (const std::weak_ptr<Molecule>& s_mol)
                        {
                            return *s_mol.lock() == neigh;
                        });
                    if (it != molecules_.end()) { continue; } // Neighbour is in molecules_ : we need a free neighbour

                    //* Check neighbour state interaction
                    State& target_state = *neigh.GetState().lock();
                    if (!process->DoesInteract(target_state.GetLabel())) { continue; } // No interaction with target state

                    //* Transform molecule to nearest image
                    Molecule trans_neigh(neigh);
                    arma::dvec transformation = NearImageTransform(mol.GetCenterOfMass(), trans_neigh.GetCenterOfMass(),
                                                                    system.GetBoundaries(), system.GetCellSize());
                    trans_neigh.Transform(transformation);

                    double rate = hamiltonian_coefficient * process->GetRateConstant(system, *this, target_state, std::vector<Molecule>{mol, trans_neigh});
                    //* Check if rate is large enough to be considered
                    if (rate < 1e-8) { continue; }

                    //* Store process information
                    RateStateProcess rate_state_process {transformation, process->GetTargetStates(), neighbour.lock(), process,
                                                            nullptr, process->GetProcessTime(), rate};
                    allowed_processes.push_back(std::move(rate_state_process));
                }
            } else
            {
                //* Get copy of target state
                size_t mol_index = mol_it - molecules_.begin();
                std::vector<std::string> target_labels = process->GetTargetStates();
                State target_state = system.GetCopyReferenceState(target_labels[mol_index]);

                //* Link molecule to target state
                target_state.SetMolecule(std::vector<std::weak_ptr<Molecule>>{*mol_it});

                std::vector<Molecule> interacting_molecules;
                for (std::weak_ptr<Molecule> s_mol : molecules_) { interacting_molecules.push_back(*s_mol.lock()); }
                double rate = hamiltonian_coefficient * process->GetRateConstant(system, *this, target_state, interacting_molecules);

                //* Check if rate is large enough to be considered
                if (rate < 1e-8) { continue; }

                //* Store process information
                arma::dvec transformation(3, arma::fill::zeros);
                RateStateProcess rate_state_process {transformation, process->GetTargetStates(), nullptr, process,
                                                        nullptr, process->GetProcessTime(), rate};
                allowed_processes.push_back(std::move(rate_state_process));

            }
        }
    }

    return allowed_processes;
}

double State::GetTransitionReorgEnergy (std::string label) const
{
    auto it = std::find_if(transition_list_.begin(), transition_list_.end(),
        [&] (const std::shared_ptr<TransitionFunction>& transition)
        {
            return transition->GetInteractionStateLabel() == label;
        });

    if (it == transition_list_.end())
    {
        std::string error = "Transition between states " + label_ + " and " + label + " not declared";
        error_manager_->SStateError(error);
    }

    return (*it)->GetEnergy(molecules_);
}

bool State::IsIsolated () const
{
    for (const std::weak_ptr<Molecule>& molecule : molecules_)
    {
        for (const std::weak_ptr<Molecule>& neighbour : molecule.lock()->GetNeighbours())
        {
            const std::shared_ptr<Molecule> neigh = neighbour.lock();
            const std::shared_ptr<State> st = neigh->GetState().lock();
            if (st->GetLabel() != "gs" && *st != *this) { return false; }

            for (const std::weak_ptr<Molecule>& second_neightbour : neigh->GetNeighbours())
            {
                const std::shared_ptr<Molecule> s_neigh = second_neightbour.lock();
                const std::shared_ptr<State> s_st = s_neigh->GetState().lock();
                if (s_st->GetLabel() != "gs" && *s_st != *this) { return false; }
            }
        }
    }

    return true;
}