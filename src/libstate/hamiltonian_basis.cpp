#include "hamiltonian_basis.h"


//* Constructors

HamiltonianBasis::HamiltonianBasis (std::vector<HamiltonianVector> vectors, std::complex<double> coefficient)
: vectors_(vectors), coefficient_(coefficient)
{
}

HamiltonianBasis::HamiltonianBasis (const HamiltonianBasis& other)
: vectors_(other.vectors_), coefficient_(other.coefficient_)
{
}

HamiltonianBasis::HamiltonianBasis (HamiltonianBasis&& other)
: vectors_(std::move(other.vectors_)), coefficient_(other.coefficient_)
{
}

//* Operator Overloading

double HamiltonianBasis::operator* (const HamiltonianBasis& other) const
{
    std::complex<double> result(1.0, 1.0);
    for (const HamiltonianVector& v1 : vectors_)
    {
        for (const HamiltonianVector& v2 : other.vectors_)
        {
            if (v1.GetSite() == v2.GetSite())
            {
                result *= v1 * v2;
            }
        }
    }

    return std::real(std::conj(coefficient_) * other.coefficient_ * result);
}

//* Setters

void HamiltonianBasis::SetCoefficient (std::complex<double> coefficient) { coefficient_ = coefficient; }

//* Public Methods

void HamiltonianBasis::ComputePM ()
{
    coefficient_ = std::complex<double>(0.5, 0.0);
    vectors_[0].SP();
    vectors_[1].SM();
}

void HamiltonianBasis::ComputeMP ()
{
    coefficient_ = std::complex<double>(0.5, 0.0);
    vectors_[0].SM();
    vectors_[1].SP();
}

void HamiltonianBasis::ComputeZZ ()
{
    coefficient_ = std::complex<double>(1.0, 0.0);
    vectors_[0].SZ();
    vectors_[1].SZ();
}