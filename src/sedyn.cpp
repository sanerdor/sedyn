#include "global_objects.h"
#include "libmanager/error_manager.h"
#include "libmanager/file_manager.h"
#include "libsystem/system.h"

#include <chrono>
#include <memory>
#include <map>
#include <string>

//* Global Objects Declaration ================================================

std::map<std::string, double> GlobalObjects::AtomMasses = 
{
    {"H", 1.00784},
    {"C", 12.011}
};

#if DO_TEST
std::mt19937 GlobalObjects::randomizer(0);
#else
std::random_device rd;
std::mt19937 GlobalObjects::randomizer(rd());
#endif

//* ===========================================================================

int main(int argc, char* argv[])
{
    //* Time measurement for simulation
    auto simulation_begin = std::chrono::high_resolution_clock::now();

    //* Error Manager
    ErrorManager error_manager;

    //* File Manager (Input and Output)
    FileManager file_manager(argc, argv, &error_manager);
    error_manager.SetFileManager(&file_manager);

    //* Write intro to the Output file
    file_manager.Initialize();

    //* System and Analysis object
    System system;
    Analysis analysis;

    //* Read Input and Structure Files
    file_manager.ReadInput(analysis, system);
    file_manager.ReadStructure(system);

    //* Process System Neighbours
    system.ProcessNeighbours();

    //* Trajectories
    file_manager.WriteTrajectorySection(system);
    size_t n_traj = system.GetNTraj();
    for (size_t traj = 0; traj < n_traj; traj++)
    {
        //* Time measurement for trajectory
        auto trajectory_begin = std::chrono::high_resolution_clock::now();

        //* Initialize System
        system.InitializeForTrajectory();

        //* Perform steps
        system.RunTrajectory(analysis);

        //* Analyse trajectory
        std::string trajectory_out_str = analysis.AnalyseTrajectory(system);

        //* Time measurement for trajectory
        auto trajectory_end = std::chrono::high_resolution_clock::now();
        auto trajectory_elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(trajectory_end - trajectory_begin);

        //* Write Trajectory
        file_manager.WriteTrajectory(traj, trajectory_out_str, trajectory_elapsed.count()*1e-9);

        //* End Trajectory
        system.EndTrajectory();
    }

    file_manager.WriteTrajectorySectionEnd();

    //* Perform analysis section
    std::string simulation_out_str = analysis.AnalyseSimulation();

    //* Time measurement for simulation
    auto simulation_end = std::chrono::high_resolution_clock::now();
    auto simulation_elapsed = std::chrono::duration_cast<std::chrono::nanoseconds>(simulation_end - simulation_begin);

    //* Write Results
    file_manager.WriteSimulationResults(simulation_out_str, simulation_elapsed.count()*1e-9);

    //* End simulation
    file_manager.ExitSimulation();

    return 0;
}