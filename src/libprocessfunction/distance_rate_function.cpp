#include "distance_rate_function.h"

#include "../libmanager/error_manager.h"


//* Constructor

DistanceRateFunction::DistanceRateFunction (std::string interaction_state, std::vector<std::string> target_states,
                                            ErrorManager* error_manager, double process_time, int hamiltonian_coefficient,
                                            double distance, double rate_constant)
: BaseProcessFunction(interaction_state, target_states, error_manager, process_time, hamiltonian_coefficient),
    distance_(distance), rate_constant_(rate_constant)
{}

//* Public Methods

double DistanceRateFunction::GetRateConstant (const System& system, const State& sta, const State& stb, const std::vector<Molecule>& molecules) const
{
    if (molecules.size() != 2)
    {
        error_manager_->SStateError("Incompatible dimension of molecules for DistanceRateFunction");
    }

    if (arma::norm(molecules[0].GetCenterOfMass() - molecules[1].GetCenterOfMass()) <= distance_) { return rate_constant_; }
    else { return 0.0; }
}