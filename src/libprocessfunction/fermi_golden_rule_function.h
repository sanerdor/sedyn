/**
 * @file fermi_golden_rule_function.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2024-05-19
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#pragma once

#include "base_process_function.h"
#include "../libelectronicfunction/base_electronic_function.h"
#include "../libvibrationfunction/marcus_function.h"
#include "../libsystem/system.h"

#include "memory"

/**
 * @brief This is the Fermi's Golden Rule
 * 
 */
class FermiGoldenRuleFunction : public BaseProcessFunction
{
public:
    //* Constructor
    /**
     * @brief Construct a new Fermi Golden Rule Function object
     * 
     */
    FermiGoldenRuleFunction () = default;

    /**
     * @brief Construct a new Fermi Golden Rule Function object
     * 
     * @param interaction_state 
     * @param target_states 
     * @param error_manager
     * @param process_time 
     * @param hamiltonian_coefficient 
     * @param electronic_function Electronic function type
     * @param vibration_function 
     */
    FermiGoldenRuleFunction (std::string interaction_state, std::vector<std::string> target_states, ErrorManager* error_manager,
                                                    double process_time, int hamiltonian_coefficient,
                                                    std::unique_ptr<BaseElectronicFunction>& electronic_function,
                                                    std::unique_ptr<MarcusFunction>& vibration_function);

    //* Destructor
    virtual ~FermiGoldenRuleFunction () = default;

    //* Public Methods
    virtual double GetRateConstant (const System& system, const State&, const State&, const std::vector<Molecule>& molecules) const override final;

private:
    //* Variables
    std::shared_ptr<BaseElectronicFunction> electronic_function_;              // 8
    std::shared_ptr<MarcusFunction> vibration_function_;                       // 8

    //* Private Methods
    double GetFCWD (const State&, const State&, const double) const;
};