#pragma once

#include "../libmanager/error_manager.h"
#include "../libmolecule/molecule.h"
#include "../libstate/state.h"

#include <memory>
#include <string>
#include <vector>


class Molecule;
class State;
class System;

class BaseProcessFunction
{
public:
    //* Constructor
    BaseProcessFunction () = default;
    BaseProcessFunction (std::string, std::vector<std::string>, ErrorManager* error_manager, double, int);

    //* Destructor
    virtual ~BaseProcessFunction () = default;

    //* Getters
    int GetHamiltonianCoefficient () const;
    double GetProcessTime () const;
    std::vector<std::string> GetTargetStates () const;

    //* Public Methods
    bool DoesInteract (const std::string) const;
    int GetInteractionStateSize () const;
    int GetTargetStateSize () const;
    virtual double GetRateConstant (const System& system, const State&, const State&, const std::vector<Molecule>&) const = 0;

protected:
    //* Variables
    std::string interaction_state_;                                            // 32
    std::vector<std::string> target_states_;                                   // 24
    ErrorManager* error_manager_;                                              // 8
    double process_time_;                                                      // 8
    int hamiltonian_coefficient_;                                              // 4
};