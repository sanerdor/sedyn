#include "base_process_function.h"


//* Constructor

BaseProcessFunction::BaseProcessFunction (std::string interaction_state, std::vector<std::string> target_states, ErrorManager* error_manager,
                                            double process_time, int hamiltonian_coefficient)
: interaction_state_(interaction_state), target_states_(target_states), error_manager_(error_manager), process_time_(process_time),
    hamiltonian_coefficient_(hamiltonian_coefficient)
{}

//* Getters

int BaseProcessFunction::GetHamiltonianCoefficient () const { return hamiltonian_coefficient_; }
double BaseProcessFunction::GetProcessTime () const { return process_time_; }
std::vector<std::string> BaseProcessFunction::GetTargetStates () const { return target_states_; }

//* Public Methods

bool BaseProcessFunction::DoesInteract (const std::string target_label) const
{
    if (target_label == interaction_state_) { return true; }
    else { return false; }
}

int BaseProcessFunction::GetInteractionStateSize () const
{
    return interaction_state_.size();
}

int BaseProcessFunction::GetTargetStateSize () const
{
    return target_states_.size();
}