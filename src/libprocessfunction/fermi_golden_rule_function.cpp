#include "fermi_golden_rule_function.h"

#include "../global_namespaces.h"
#include "../libmanager/error_manager.h"

#include <armadillo>


//* Constructor

FermiGoldenRuleFunction::FermiGoldenRuleFunction (std::string interaction_state, std::vector<std::string> target_states,
                                                    ErrorManager* error_manager, double process_time, int hamiltonian_coefficient,
                                                    std::unique_ptr<BaseElectronicFunction>& electronic_function,
                                                    std::unique_ptr<MarcusFunction>& vibration_function)
: BaseProcessFunction(interaction_state, target_states, error_manager, process_time, hamiltonian_coefficient),
    electronic_function_(std::move(electronic_function)), vibration_function_(std::move(vibration_function))
{
}

//* Public Methods

double FermiGoldenRuleFunction::GetRateConstant (const System& system, const State& sta, const State& stb, const std::vector<Molecule>& molecules) const
{
    if (molecules.size() != 2)
    {
        error_manager_->SStateError("Incompatible dimension of molecules for FermiGoldenRuleFunction");
    }

    double electronic_coupling = electronic_function_->GetEnergy(molecules);
    double spectral_overlap = this->GetFCWD(sta, stb, system.GetTemperature());
    double result = 2 * ncon::pi / ncon::hbar * std::pow(electronic_coupling, 2) * spectral_overlap;

    return result;
}

//* Private Methods

double FermiGoldenRuleFunction::GetFCWD (const State& sta, const State& stb, const double temperature) const
{
    double delta_energy = stb.GetEnergy() - sta.GetEnergy();
    double range = std::abs(-delta_energy) + ncon::kb * 1e4;
    arma::dvec X = arma::linspace<arma::dvec> (-range, range, 1e4);

    double donor_reorg_energy = sta.GetTransitionReorgEnergy(stb.GetLabel());
    double acceptor_reorg_energy = donor_reorg_energy;
    if (stb.GetLabel() != "gs")
    {
        acceptor_reorg_energy = stb.GetTransitionReorgEnergy(sta.GetLabel());
    }
    
    arma::dvec donor_vib_spec = vibration_function_->GetVibrationSpectrum(X, donor_reorg_energy, delta_energy, temperature);
    arma::dvec acceptor_vib_spec = vibration_function_->GetVibrationSpectrum(X, acceptor_reorg_energy, -delta_energy, temperature);
    arma::dvec res = arma::trapz(X, donor_vib_spec % acceptor_vib_spec, 0);

    return res(0, 0);
}