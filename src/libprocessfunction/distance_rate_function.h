#pragma once

#include "base_process_function.h"


class DistanceRateFunction : public BaseProcessFunction
{
public:
    //* Constructor
    DistanceRateFunction () = default;
    DistanceRateFunction (std::string, std::vector<std::string>, ErrorManager* error_manager, double, int, double, double);

    //* Destructor
    virtual ~DistanceRateFunction () = default;

    //* Public Methods
    virtual double GetRateConstant (const System& system, const State&, const State&, const std::vector<Molecule>& molecules) const override final;

private:
    //* Variables
    double distance_;                                                          // 8
    double rate_constant_;                                                     // 8
};