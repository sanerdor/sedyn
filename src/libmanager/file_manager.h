/**
 * @file file_manager.h
 * @author Aitor Diaz-Andres (ad.andres@outlook.com)
 * @brief FileManager class declaration.
 * @date 2024-05-21
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#pragma once

#include "input_enums.h"
#include "error_manager.h"
#include "../libanalysis/analysis.h"
#include "../libelectronicfunction/base_electronic_function.h"
#include "../libmolecule/molecule.h"
#include "../libprocessfunction/base_process_function.h"
#include "../libstate/hamiltonian.h"
#include "../libsystem/system.h"
#include "../libtransitionfunction/transition_function.h"

#include <armadillo>
#include <memory>
#include <string>
#include <vector>


/**
 * @brief File Manager to manage all input and output files.
 * 
 * This class is designed to be the intermediate between the user and the software.
 * Input information, system and extra files are read and interpreted to construct
 * all the objects for the simulation. Similarly, the results and output information
 * is written to the output file through this class. Besides, this class manages the
 * checkpoint file which can be read in new simulations and avoid costly computation.
 * 
 * @warning Inherit from Singleton in the future.
 */
class FileManager
{
private:
    std::string in_name_;                                                      // 32
    std::string mol_name_;                                                     // 32
    std::string out_name_;                                                     // 32
    std::string sdn_name_;                                                     // 32
    ErrorManager* error_manager_;                                              // 8

public:
    /**
     * @brief Construct a new FileManager object.
     * 
     */
    FileManager () = default;

    /**
     * @brief Construct a new FileManager object.
     * 
     * @param argc Input argument number.
     * @param argv Input argument array.
     * @param error_manager ErrorManager object reference.
     */
    FileManager (int argc, char* argv[], ErrorManager* error_manager);

    /**
     * @brief Destroy the FileManager object.
     * 
     */
    ~FileManager () = default;

    /**
     * @brief Writes the last message to the output file.
     * 
     */
    void ExitSimulation ();

    /**
     * @brief Generate output file, writes the wellcome message and tries to open
     * input files.
     * 
     */
    void Initialize ();

    /**
     * @brief Reads input file sections and generate needed objects for the simulation.
     * 
     * This is a heavy algorithm to read the input file. There are nested private functions
     * to read all the properties available in the input file. The algorithm exists correctly
     * when all the properties for the simulation are stored and the state references (including
     * the ground state) have been generated and successfully stored.
     * 
     * @param analysis Analysis object reference.
     * @param system System object reference.
     * 
     * @warning It is a highly nested algorithm with private function. There must be a way to make
     * is simpler.
     */
    void ReadInput (Analysis& analysis, System& system);

    /**
     * @brief Reads from the checkpoint file (.h5 format) the neighbours for each Molecule.
     * 
     * @param neighbours Armadillo matrix with neighbours declared in binary.
     */
    void ReadNeighbours (arma::umat& neighbours);

    /**
     * @brief Reads the Molecule system file (.pdb format) and generate one unique Molecule for
     * each entry. This Molecule list is then stored in the System object.
     * 
     * @param system System object reference.
     */
    void ReadStructure (System& system);

    /**
     * @brief Writes the neighbours for each Molecule into the checkpoint file (.h5 format).
     * 
     * @param neighbours Armadillo matrix with neighbours declared in binary.
     */
    void WriteNeighbours (const arma::umat& neighbours);

    /**
     * @brief Writes the elapsed time to the output file.
     * 
     * @param str String with a custom message.
     * @param elapsed_time Delta time to be written in the output file in seconds.
     */
    void WriteSimulationResults (std::string str, double elapsed_time);

    /**
     * @brief Standard function to write any string into the output file.
     * 
     * @param str String with a custom message.
     */
    void WriteString (std::string str);

    /**
     * @brief Standard funtion to write trajectory execution information to the output file.
     * 
     * @param current_trajectory Index of the current trajectory.
     * @param trajectory_str String with a custom trajectory message.
     * @param elapsed_time Delta time to be written in the output file in seconds.
     */
    void WriteTrajectory (int current_trajectory, std::string trajectory_str, double elapsed_time);

    /**
     * @brief Standard function to write trajectory information before execution.
     * 
     * @param system System object reference.
     */
    void WriteTrajectorySection (System& system);

    /**
     * @brief Standard function to write trajectory information after execution.
     * 
     */
    void WriteTrajectorySectionEnd ();

private:
    /**
     * @brief Generates a new Molecule object and append it to the molecule list.
     * 
     * @param molecule_list Vector of Molecule object pointers.
     * @param v_coord Armadillo matrix with the coordinates of the new Molecule.
     * @param elements Vector with the elements of the new Molecule.
     */
    void AddMoleculeToList (std::vector<std::unique_ptr<Molecule>>& molecule_list, std::vector<double>& v_coord,
                            std::vector<std::string>& elements) const;

    /**
     * @brief Generate an empty group state with energy 0.0. This is only called when
     * the ground state have not been declared in the input file.
     * 
     * @param ground_state State object reference.
     */
    void GenerateGroundState (std::unique_ptr<State>& ground_state) const;

    /**
     * @brief Get the vector of strings following a character pattern in a string.
     * 
     * @param line Line to be splitted.
     * @param split Character to split.
     * @return std::vector<std::string> 
     */
    std::vector<std::string> GetStringList (std::string line, char split) const;

    /**
     * @brief Checks if the file name contains the extension provided.
     * 
     * @param file_name Name of the file.
     * @param target_extension Extension of the file.
     * @return true 
     * @return false 
     */
    bool IsFileExtension (std::string file_name, std::string target_extension) const;

    /**
     * @brief Reads the input Analysis section and returns the information found.
     * 
     * @param in_file Input file.
     * @param analysis_keys All the requested analysis keys paired with the states to be analysed.
     * @param anal_param_int Analysis integer parameters.
     */
    void ProcessAnalysisSection (std::ifstream& in_file, std::map<Analysis::AnalysisTypes, std::vector<std::string>>& analysis_keys,
                                    std::map<Analysis::AnalysisParameters, int>& anal_param_int) const;

    /**
     * @brief Evaluates the electronic function type and calls the correct function to read the input.
     * 
     * @param electronic_function BaseElectronicFunction object reference.
     * @param type String with the electronic function to be evaluated.
     * @param argument String with the value of the electronic function or file name to be read.
     */
    void ProcessElectronicFunction (std::unique_ptr<BaseElectronicFunction>& electronic_function,
                                    const std::string type, const std::string argument) const;

    /**
     * @brief Reads the input and generates a Hamiltonian object with that information.
     * 
     * @param in_file Input file.
     * @param hamiltonian Empty Hamiltonian object reference.
     */
    void ProcessHamiltonian (std::ifstream& in_file, std::unique_ptr<Hamiltonian>& hamiltonian) const;

    /**
     * @brief Checks the readability of the line in the input file. The line is skipped
     * if it is empty or commented.
     * 
     * @param line String with the line to be checked.
     * @return true 
     * @return false 
     */
    bool ProcessLine (const std::string line) const;

    /**
     * @brief Reads the input Process section and generates the target process function.
     * 
     * All the process functions have shared variables and then there are process function 
     * specific variables. For each new specific process function, new variables should be declared
     * and read accordingly.
     * 
     * @param in_file Input file.
     * @param process_function Empty BaseProcessFunction object reference.
     * 
     * @warning Under development to make if more flexible for new process functions. Each process
     * function should have its unique reader.
     */
    void ProcessProcessFunction (std::ifstream& in_file, std::unique_ptr<BaseProcessFunction>& process_function) const;

    /**
     * @brief Reads the input Run section for general simulation information.
     * 
     * @param in_file Input file.
     * @param n_step Empty number of steps per trajectory.
     * @param n_traj Empty number of trajectories.
     * @param read_sdn Switcher to read or not the checkpoint file.
     * @param temperature Empty system temperature.
     * @param boundaries Armadillo vector with Perioric Boundary Conditions in binary.
     * @param cut_off Armadillo vector with cut off radius to compute the neighbours.
     */
    void ProcessRunSection (std::ifstream& in_file, int& n_step, int& n_traj, bool& read_sdn,
                            double& temperature, arma::uvec& boundaries, arma::dvec& cut_off) const;

    /**
     * @brief Reads the input State section and generates a new reference State object.
     * 
     * This algorithm reads all the properties of a state from the processes to the hamiltonian.
     * The complexity of this algorithm leads to nested function calls to read all the properties.
     * 
     * @param in_file Input file.
     * @param initial_num Empty number of initial number of this State.
     * @param size Empty size of the state (at least 1).
     * @param label Empty label of the state.
     * @param electronic_function Empty BaseElectronicFunction object reference.
     * @param hamiltonian Empty Hamiltonian object reference.
     * @param process_list Empty vector of BaseProcessFunction object references.
     * @param transition_list Empty vector of TransitionFunction object references.
     */
    void ProcessStateSection (std::ifstream& in_file, int& initial_num, int& size, std::string& label,
                                std::unique_ptr<BaseElectronicFunction>& electronic_function,
                                std::unique_ptr<Hamiltonian>& hamiltonian,
                                std::vector<std::unique_ptr<BaseProcessFunction>>& process_list,
                                std::vector<std::unique_ptr<TransitionFunction>>& transition_list) const;

    /**
     * @brief Reads the input Transition section and generates a new object.
     * 
     * @param in_file Input file.
     * @param transition_function Empty TransitionFunction object reference. 
     */
    void ProcessTransitionFunction (std::ifstream& in_file, std::unique_ptr<TransitionFunction>& transition_function) const;

    AnalysisKeys StringToAnalysisKeysEnum (const std::string) const;
    ElectronicFunctionType StringToElectronicFunctionTypeEnum (const std::string) const;
    HamiltonianType StringToHamiltonianTypeEnum (const std::string) const;
    ProcessFunctionType StringToProcessFunctionTypeEnum (const std::string) const;
    RunKeys StringToRunKeysEnum (const std::string) const;
    StateKeys StringToStateKeysEnum (const std::string) const;
    Sections StringToSectionsEnum (const std::string) const;
    TransitionFunctionType StringToTransitionFunctionTypeEnum (const std::string) const;
};