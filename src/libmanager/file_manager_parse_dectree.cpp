#include "file_manager_parse_dectree.h"

#include "../libelectronicfunction/dectree_function.h"

#include <algorithm>
#include <sstream>
#include <iostream>


void ProcessDecTreeLayer (std::ifstream& dectree_file, std::unique_ptr<DecTreeLayer>& dectree_layer)
{
    std::string condition = "none";
    int left_index = -1;
    double right_value = -99999;
    std::unique_ptr<DecTreeLayer> true_layer;
    double true_value = -99999;
    std::unique_ptr<DecTreeLayer> false_layer;
    double false_value = -99999;

    bool end_input_parse = false;
    std::string line;
    while (std::getline(dectree_file, line))
    {
        std::transform(line.begin(), line.end(), line.begin(),
            [] (char c) { return std::tolower(c); });

        std::erase_if(line, [] (const char& c)
            {
                return (c == '\"' || c == ':' || c == ',');
            });
        
        std::istringstream sline(line);
        std::string key;
        std::string val;

        sline >> key;
        sline >> val;

        switch (StringToDecTreeInputTypeEnum(key))
        {
            case DecTreeInputType::Condition:
            {
                condition = val;

                break;
            }

            case DecTreeInputType::FalseDecision:
            {
                if (val == "{") { ProcessDecTreeLayer(dectree_file, false_layer); }
                else { false_value = std::stod(val); }

                break;
            }

            case DecTreeInputType::EndLayer:
            {
                end_input_parse = true;

                break;
            }

            case DecTreeInputType::LeftHandIndex:
            {
                left_index = std::stoi(val);

                break;
            }

            case DecTreeInputType::RightHandValue:
            {
                right_value = std::stod(val);

                break;
            }

            case DecTreeInputType::TrueDecision:
            {
                if (val == "{") { ProcessDecTreeLayer(dectree_file, true_layer); }
                else { true_value = std::stod(val); }

                break;
            }

            default:
                break;
        }

        if (end_input_parse) { break; }
    }

    dectree_layer = std::make_unique<DecTreeLayer>(right_value, false_value, true_value, false_layer,
                                                    true_layer, left_index, condition);
}

void ReadDecTreeFunction (std::ifstream& dectree_file, std::unique_ptr<BaseElectronicFunction>& electronic_function)
{
    std::unique_ptr<DecTreeLayer> tree_root;

    std::string line;
    std::getline(dectree_file, line);
    if (line != "{")
    {
        std::cout << " Something wrong " << std::endl;
    }

    ProcessDecTreeLayer(dectree_file, tree_root);
    electronic_function = std::make_unique<DecTreeFunction>(tree_root);
}

DecTreeInputType StringToDecTreeInputTypeEnum (std::string str)
{
    if (str == "cond") { return DecTreeInputType::Condition; }
    if (str == "false") { return DecTreeInputType::FalseDecision; }
    if (str == "}") { return DecTreeInputType::EndLayer; }
    if (str == "lh") { return DecTreeInputType::LeftHandIndex; }
    if (str == "rh") { return DecTreeInputType::RightHandValue; }
    if (str == "true") { return DecTreeInputType::TrueDecision; }

    return DecTreeInputType::Default;
}