#include "file_manager.h"

#include "error_manager.h"
#include "file_manager_parse_dectree.h"
#include "file_manager_parse_ffnn.h"
#include "../libelectronicfunction/constant_function.h"
#include "../libprocessfunction/fermi_golden_rule_function.h"
#include "../libprocessfunction/distance_rate_function.h"
#include "../libstate/state.h"
#include "../libvibrationfunction/marcus_function.h"

#ifdef USE_OpenMP
    #include <omp.h>
#endif

#include <algorithm>
#include <iomanip>
#include <fstream>
#include <cstring>


//* Constructors

FileManager::FileManager (int argc, char* argv[], ErrorManager* error_manager)
: error_manager_(error_manager)
{
    for (size_t i = 1; i < argc; i++)
    {
        if (std::strcmp(argv[i], "-i") == 0) { in_name_ = std::getenv("SDNWORKPATH") + std::string("/") + std::string(argv[++i]); }
        else if (std::strcmp(argv[i], "-mol") == 0) { mol_name_ = std::getenv("SDNWORKPATH") + std::string("/") + std::string(argv[++i]); }
        else if (std::strcmp(argv[i], "-o") == 0) { out_name_ = std::getenv("SDNWORKPATH") + std::string("/") + std::string(argv[++i]); }
        else if (std::strcmp(argv[i], "-sdn") == 0) { sdn_name_ = std::getenv("SDNWORKPATH") + std::string("/") + std::string(argv[++i]); }
    }
}

//* Public Methods

void FileManager::ExitSimulation ()
{
    std::ofstream (out_file);
    out_file.open(out_name_, std::ios::out | std::ios::app);
    if (!out_file) { error_manager_->SFileError("Output", out_name_); }

    out_file << "\n\n" << std::setw(15) << "";
    out_file << std::setfill('-') << std::setw(41) << "\n" << std::setfill(' ') << std::endl;;
    out_file << std::setw(52) << std::right << " Tranquility lies in human's heart " << "\n" << std::endl;
    out_file << std::setw(15) << "";
    out_file << std::setfill('-') << std::setw(41) << "\n" << std::setfill(' ');
    out_file << "\n\n";

    out_file.close();
}

void FileManager::Initialize ()
{
    std::ofstream out_file;
    out_file.open(out_name_, std::ios::out);
    if (!out_file || !this->IsFileExtension(out_name_, "out"))
    {
        out_name_ = std::getenv("SDNWORKPATH") + std::string("/") + std::string("sedyn_output.out");
        out_file.open(out_name_, std::ios::out);
        if (!out_file) { error_manager_->SOutputError(); }
    }

    out_file << "\n" << std::setw(15) << "";
    out_file << std::setfill('=') << std::setw(41) << "\n" << std::setfill(' ') << std::endl;
    out_file << std::setw(50) << std::right << " Solid State Exciton Dynamics " << "\n" << std::endl;
    out_file << std::setw(15) << "";
    out_file << std::setfill('=') << std::setw(41) << "\n" << std::setfill(' ');
    out_file << "\n\n";

    out_file.close();
    
    std::ifstream in_file;
    in_file.open(in_name_, std::ios::in);
    if (!in_file || !this->IsFileExtension(in_name_, "in"))
    {
        error_manager_->SFileError("Input", in_name_);
    }
    in_file.close();

    std::ifstream mol_file;
    mol_file.open(mol_name_, std::ios::in);
    if (!mol_file || !this->IsFileExtension(mol_name_, "pdb"))
    {
        error_manager_->SFileError("Structure", mol_name_);
    }
    mol_file.close();
}

void FileManager::ReadInput (Analysis& analysis, System& system)
{
    //* Display output Text
    std::ofstream out_file;
    out_file.open(out_name_, std::ios::app);
    if (!out_file) { error_manager_->SFileError("Output", out_name_); }

    std::string read_input_section = "Reading input file and checking its integrity";
    out_file << read_input_section << "\n";
    out_file << std::setfill('-') << std::setw(read_input_section.size()+1) << "\n";
    out_file.close();

    //* Read Input File
    std::ifstream in_file;
    in_file.open(in_name_, std::ios::in);
    if (!in_file) { error_manager_->SFileError("Input", in_name_); }

    //* Function local variables
    std::vector<std::string> state_labels_list;
    std::vector<std::unique_ptr<State>> state_list;

    //* Read all lines from file
    bool is_run = false;
    bool is_analysis = false;
    std::string line;
    while (std::getline(in_file, line))
    {
        if (this->ProcessLine(line)) { continue; }

        std::transform(line.begin(), line.end(), line.begin(),
            [] (char c) { return std::tolower(c); });

        std::istringstream sline(line);
        std::string section;
        sline >> section;

        switch (this->StringToSectionsEnum(section))
        {
            //* Algorithm section is removed until another algorithm is included
            /* case Sections::Algorithm:
            {
                std::string section_label;
                sline >> section_label;
                if (section_label.empty()) { error_manager_->SSectionValueError(section); }

                break;
            } */

            case Sections::Analysis:
            {
                std::map<Analysis::AnalysisTypes, std::vector<std::string>> analysis_keywords;
                std::map<Analysis::AnalysisParameters, int> anal_param_int;

                this->ProcessAnalysisSection(in_file, analysis_keywords, anal_param_int);
                analysis = Analysis(analysis_keywords, error_manager_);
                analysis.set_anal_param_int(anal_param_int);
                
                is_analysis = true;

                break;
            }

            case Sections::Run:
            {
                int n_step = 10000;
                int n_traj = 1;
                bool read_sdn = false;
                double temperature = 298;
                arma::uvec boundaries = arma::uvec(3, arma::fill::zeros);
                arma::dvec cut_off {10.0, 10.0, 10.0};

                this->ProcessRunSection(in_file, n_step, n_traj, read_sdn, temperature, boundaries, cut_off);
                system = System(boundaries, cut_off, temperature, n_step, n_traj, read_sdn, error_manager_, this);

                is_run = true;

                break;
            }

            case Sections::State:
            {
                std::string label;
                int initial_num;
                int size;
                std::unique_ptr<BaseElectronicFunction> electronic_function = nullptr;
                std::unique_ptr<Hamiltonian> hamiltonian = nullptr;
                std::vector<std::unique_ptr<BaseProcessFunction>> process_list;
                std::vector<std::unique_ptr<TransitionFunction>> transition_list;

                this->ProcessStateSection(in_file, initial_num, size, label, electronic_function,
                                            hamiltonian, process_list, transition_list);

                std::unique_ptr<State> state = std::make_unique<State>(label, process_list, electronic_function, transition_list,
                                                                        hamiltonian, initial_num, size, error_manager_);

                state_labels_list.push_back(state->GetLabel());
                state_list.push_back(std::move(state));

                break;
            }

            case Sections::Default:
            {
                error_manager_->SSectionKeyError(section);

                break;
            }

            default:
            {
                error_manager_->SSectionKeyError("");

                break;
            }
        }
    }

    in_file.close();

    //* Check consistency

    if (!is_run) { error_manager_->SInputError("Run Section ... NONE"); }
    if (!is_analysis) { error_manager_->SInputError("Analysis Section ... NONE"); }
    if (state_list.size() == 0) { error_manager_->SInputError("State Section ... NONE"); }
    else
    {
        for (std::string label : state_labels_list) { this->WriteString("State Section ... " + label + " ... OK"); }
    }

    //* Add Ground State
    std::unique_ptr<State> ground_state;
    std::vector<std::unique_ptr<State>>::iterator it;
    it = std::find_if(state_list.begin(), state_list.end(),
                        [] (const std::unique_ptr<State>& st)
                        {
                            return st->GetLabel() == "gs";
                        });

    if (it != state_list.end())
    {
        this->WriteString("You have manually declared Ground State -- Ignoring default Ground State");
        ground_state = std::move(state_list[it - state_list.begin()]);
        state_list.erase(it);
    } else
    {
        this->GenerateGroundState(ground_state);
    }

    system.SaveReferenceStates(state_list, ground_state);
    analysis.CheckStatesToAnalyse(state_labels_list);
}

void FileManager::ReadNeighbours (arma::umat& neighbours)
{
    std::ifstream sdn_file;
    sdn_file.open(sdn_name_, std::ios::in);
    if (!sdn_file) { error_manager_->SFileError("SDN", sdn_name_); }
    sdn_file.close();

    bool is_loaded = neighbours.load(sdn_name_, arma::arma_ascii);
    if (!is_loaded)
    {
        error_manager_->SFileError("SDN", sdn_name_);
    }

    this->WriteString("Neighbours have been read from SDN File");
}

void FileManager::ReadStructure (System& system)
{
    std::ifstream mol_file;
    mol_file.open(mol_name_, std::ios::in);
    if (!mol_file) { error_manager_->SFileError("Structure", mol_name_); }

    //* Function local variables
    std::vector<std::unique_ptr<Molecule>> molecule_list;
    std::vector<double> v_coord;
    std::vector<std::string> elements;
    std::string type;
    arma::dvec cell_size = arma::dvec(3, arma::fill::zeros);

    //* Read first line of crystal data from file
    std::string line;
    std::getline(mol_file, line);
    if (line.substr(0, 6) != "CRYST1") { error_manager_->SFileError("Structure", mol_name_); }
    cell_size(0) = std::stod(line.substr(8, 17));
    cell_size(1) = std::stod(line.substr(17, 26));
    cell_size(2) = std::stod(line.substr(26, 35));

    //* Read all lines from file
    int current_molecule = 1;
    while (std::getline(mol_file, line))
    {
        std::transform(line.begin(), line.end(), line.begin(),
            [] (char c) { return std::toupper(c); });

        if (line == "END")
        {
            this->AddMoleculeToList(molecule_list, v_coord, elements);
            
            break;
        }

        type = line.substr(0, 3);
        if (type == "TER")
        {
            this->AddMoleculeToList(molecule_list, v_coord, elements);
            current_molecule = 1;

            continue;
        } else if (std::stoi(line.substr(22, 4)) == (current_molecule+1))
        {
            this->AddMoleculeToList(molecule_list, v_coord, elements);
            current_molecule += 1;
        }

        v_coord.push_back(std::stod(line.substr(30, 8)));
        v_coord.push_back(std::stod(line.substr(38, 8)));
        v_coord.push_back(std::stod(line.substr(46, 8)));
        
        std::string a_symbol = line.substr(12, 4);
        std::erase_if(a_symbol, [] (const char& c)
                        {
                            return (c == ' ' || std::isdigit(c));
                        });
        
        elements.push_back(a_symbol);
    }

    mol_file.close();

    //* Check molecule file
    if (molecule_list.size() == 0) { error_manager_->SInputError("Molecule File ... EMPTY"); }
    else
    {
        this->WriteString("Molecule File ... OK");
        this->WriteString("Number of molecules in the system ... " + std::to_string(molecule_list.size()));
    }

    system.SaveMolecules(molecule_list);
    system.SetCellSize(cell_size);
}

void FileManager::WriteNeighbours (const arma::umat& neighbours)
{
    bool is_saved = neighbours.save(sdn_name_, arma::arma_ascii);
    if (!is_saved)
    {
        error_manager_->SFileError("SDN", sdn_name_);
    }

    this->WriteString("Neighbours have been writen from SDN File");
}

void FileManager::WriteSimulationResults (std::string str, double elapsed_time)
{
    std::string out_str = "";
    out_str += str;

    std::stringstream elapsed_stream;
    elapsed_stream << std::fixed << std::setprecision(2) << elapsed_time;
    out_str += "\nElapsed CPU time ... " + elapsed_stream.str() + " s\n";

    this->WriteString(out_str);
}

void FileManager::WriteString (std::string str)
{
    std::ofstream out_file;
    out_file.open(out_name_, std::ios::app);
    if (!out_file) { error_manager_->SFileError("Output", out_name_); }

    out_file << str << std::endl;

    out_file.close();
}

void FileManager::WriteTrajectory (int current_trajectory, std::string trajectory_str, double elapsed_time)
{
    std::string out_str = "";
    out_str += std::string(15+std::to_string(current_trajectory).size(), '-') + "\n";
    out_str += "  Trajectory " + std::to_string(current_trajectory) + "\n";
    out_str += std::string(15+std::to_string(current_trajectory).size(), '-') + "\n\n";
    out_str += trajectory_str;

    std::stringstream elapsed_stream;
    elapsed_stream << std::fixed << std::setprecision(2) << elapsed_time;
    out_str += "Elapsed CPU time in trajectory ... " + elapsed_stream.str() + " s\n";

    this->WriteString(out_str);
}

void FileManager::WriteTrajectorySection (System& system)
{
    std::string out_str = "";
    out_str += "\n";
    out_str += "Number of trajectories ... " + std::to_string(system.GetNTraj()) + "\n";
    out_str += "Number of steps per trajectory ... " + std::to_string(system.GetNStep()) + "\n";
    out_str += "Parallelization algorihtm ... ";

#if USE_OpenMP
    out_str += "OpenMP ... " + std::to_string(omp_get_max_threads()) + "\n";
#else
    out_str += "NONE";
#endif

    out_str += "\n";
    this->WriteString(out_str);
}

void FileManager::WriteTrajectorySectionEnd ()
{
    std::string out_str = "";
    out_str += "\n" + std::string(23, '=') + "\n";
    out_str += "-- End of Simulation --\n";
    out_str += std::string(23, '=') + "\n";

    this->WriteString(out_str);
}

//* Private Methods

void FileManager::AddMoleculeToList (std::vector<std::unique_ptr<Molecule>>& molecule_list, std::vector<double>& v_coord,
                                        std::vector<std::string>& elements) const
{
    arma::dmat coordinates = arma::conv_to<arma::dmat>::from(v_coord);
    coordinates.reshape(3, v_coord.size()/3);

    std::unique_ptr<Molecule> molecule = std::make_unique<Molecule>(coordinates, elements);
    molecule_list.push_back(std::move(molecule));
    
    v_coord.clear();
    elements.clear();
}

void FileManager::GenerateGroundState (std::unique_ptr<State>& ground_state) const
{
    std::string label = "gs";
    int initial_num = 0;
    int size = 1;
    std::unique_ptr<BaseElectronicFunction> electronic_function = std::make_unique<ConstantFunction>(0.0);
    std::unique_ptr<Hamiltonian> hamiltonian = nullptr;
    std::vector<std::unique_ptr<BaseProcessFunction>> process_list;
    std::vector<std::unique_ptr<TransitionFunction>> transition_list;

    ground_state = std::make_unique<State>(label, process_list, electronic_function, transition_list,
                                            hamiltonian, initial_num, size, error_manager_);
}

std::vector<std::string> FileManager::GetStringList (std::string line, char split) const
{
    std::vector<std::string> string_vector;
    std::istringstream sline(line);
    std::string word;
    while (std::getline(sline, word, split))
    {
        if (word.empty()) { error_manager_->SKeyValError("", "", line); }

        string_vector.push_back(word);
    }

    return string_vector;
}

bool FileManager::IsFileExtension (std::string file_name, std::string target_extension) const
{
    std::string extension;
    std::istringstream sfname(file_name);
    std::getline(sfname, extension, '/');
    std::getline(sfname, extension, '.');
    std::getline(sfname, extension, '.');

    if (extension.empty() || extension != target_extension) { return false; }
    else { return true; }
}

void FileManager::ProcessAnalysisSection (std::ifstream& in_file, std::map<Analysis::AnalysisTypes, std::vector<std::string>>& analysis_keys,
                                            std::map<Analysis::AnalysisParameters, int>& anal_param_int) const
{
    std::string line;
    while (std::getline(in_file, line))
    {
        if (this->ProcessLine(line)) { continue; }

        std::transform(line.begin(), line.end(), line.begin(),
            [] (char c) { return std::tolower(c); });

        std::istringstream sline(line);
        std::string key;
        sline >> key;

        std::string val;
        sline >> val;
        if (key == "$$") { break; }
        if (key[0] != '$' && val.empty()) { error_manager_->SKeyValError("analysis", key, val); }

        switch (this->StringToAnalysisKeysEnum(key))
        {
            case AnalysisKeys::DiffCoeff:
            {
                std::vector<std::string> states = this->GetStringList(val, ',');
                analysis_keys.insert({Analysis::AnalysisTypes::DiffusionCoefficient, states});

                break;
            }

            case AnalysisKeys::DiffCoeffSubTraj:
            {
                anal_param_int.insert({Analysis::AnalysisParameters::SubTrajectoryLength, std::stoi(val)});

                break;
            }

            case AnalysisKeys::Lifetime:
            {
                std::vector<std::string> states = this->GetStringList(val, ',');
                analysis_keys.insert({Analysis::AnalysisTypes::Lifetime, states});

                break;
            }

            case AnalysisKeys::SaveTrajectory:
            {
                std::vector<std::string> states = this->GetStringList(val, ',');
                analysis_keys.insert({Analysis::AnalysisTypes::SaveTrajectory, states});

                break;
            }

            case AnalysisKeys::TotalCount:
            {
                std::vector<std::string> states = this->GetStringList(val, ',');
                analysis_keys.insert({Analysis::AnalysisTypes::TotalCount, states});

                break;
            }

            case AnalysisKeys::Default:
            {
                error_manager_->SKeyValError("analysis", key, val);

                break;
            }

            default:
            {
                error_manager_->SKeyValError("", "", "");

                break;
            }
        }
    }

    //* Default parameters
    if (anal_param_int.find(Analysis::AnalysisParameters::SubTrajectoryLength) == anal_param_int.end())
    {
        anal_param_int.insert({Analysis::AnalysisParameters::SubTrajectoryLength, 1000});
    }
}

void FileManager::ProcessElectronicFunction (std::unique_ptr<BaseElectronicFunction>& electronic_function,
                                                const std::string type, const std::string argument) const
{
    switch (this->StringToElectronicFunctionTypeEnum(type))
    {
        case ElectronicFunctionType::Constant:
        {
            double energy = std::stod(argument);
            if (energy < 1e-8) { error_manager_->SKeyValError("state", type, argument); }
            electronic_function = std::make_unique<ConstantFunction>(energy);

            break;
        }

        case ElectronicFunctionType::DecisionTree:
        {
            std::ifstream dectree_file;
            dectree_file.open(argument+".json", std::ios::in);
            if (!dectree_file || this->IsFileExtension(argument, "json"))
            {
                error_manager_->SFileError("DECTREE", argument+".json");
            }

            ReadDecTreeFunction(dectree_file, electronic_function);

            dectree_file.close();

            break;
        }

        case ElectronicFunctionType::FeedForwardNeuralNetwork:
        {
            std::ifstream ffnn_file;
            ffnn_file.open(argument+".h5", std::ios::in);
            if (!ffnn_file || this->IsFileExtension(argument, "h5"))
            {
                error_manager_->SFileError("FFNN", argument+".h5");
            }
            ffnn_file.close();

            ffnn_file.open(argument+".json", std::ios::in);
            if (!ffnn_file || this->IsFileExtension(argument, "json"))
            {
                error_manager_->SFileError("FFNN", argument+".json");
            }

            ReadFFNNFunction(ffnn_file, argument+".h5", electronic_function);

            ffnn_file.close();

            break;
        }

        case ElectronicFunctionType::Default:
        {
            error_manager_->SKeyValError("state", type, argument);

            break;
        }

        default:
        {
            error_manager_->SKeyValError("state", type, argument);

            break;
        }
    }
}

void FileManager::ProcessHamiltonian (std::ifstream& in_file, std::unique_ptr<Hamiltonian>& hamiltonian) const
{
    //* Hamiltonian variables
    std::string character_function;
    std::unique_ptr<BaseElectronicFunction> exchange_coupling = nullptr;

    std::string line;
    while (std::getline(in_file, line))
    {
        if (this->ProcessLine(line)) { continue; }

        std::transform(line.begin(), line.end(), line.begin(),
            [] (char c) { return std::tolower(c); });

        std::istringstream sline(line);
        std::string key;
        sline >> key;

        std::string val;
        sline >> val;
        if (key == "$$") { break; }
        if (key[0] != '$' && val.empty()) { error_manager_->SKeyValError("state hamiltonian", key, val); }
        
        switch (this->StringToHamiltonianTypeEnum(key))
        {
            case HamiltonianType::CharacterFunction:
            {
                character_function = val;

                break;
            }

            case HamiltonianType::ExchangeCouplingFunction:
            {
                std::string option;
                sline >> option;
                if (option.empty()) { error_manager_->SKeyValError("state hamiltonian", key, val); }

                this->ProcessElectronicFunction(exchange_coupling, val, option);

                break;
            }

            case HamiltonianType::Default:
            {
                error_manager_->SKeyValError("state hamiltonian", key, val);

                break;
            }

            default:
            {
                error_manager_->SKeyValError("", "", "");

                break;
            }
        }
    }

    hamiltonian = std::make_unique<Hamiltonian>(exchange_coupling, character_function);
}

bool FileManager::ProcessLine (const std::string line) const
{
    std::istringstream sline(line);
    std::string line_start;
    sline >> line_start;

    return (line_start.empty() || line_start[0] == '!') ? true : false;
}

void FileManager::ProcessProcessFunction (std::ifstream& in_file, std::unique_ptr<BaseProcessFunction>& process_function) const
{
    //* General Process Variables
    std::string type;
    std::string interaction_state;
    std::vector<std::string> target_state;
    double process_time = -1.0;
    int hamiltonian_coefficient = -1;

    //* Fermi Golden Rule Variables
    std::unique_ptr<BaseElectronicFunction> electronic_function;
    std::unique_ptr<MarcusFunction> vibration_function;

    //* Distance Rate Variables
    double distance = -1.0;
    double rate_constant = -1.0;

    std::string line;
    while (std::getline(in_file, line))
    {
        if (ProcessLine(line)) { continue; }

        std::transform(line.begin(), line.end(), line.begin(),
            [] (char c) { return std::tolower(c); });

        std::istringstream sline(line);
        std::string key;
        sline >> key;

        std::string val;
        sline >> val;
        if (key == "$$") { break; }
        if (key[0] != '$' && val.empty()) { error_manager_->SKeyValError("state process", key, val); }
        
        switch (this->StringToProcessFunctionTypeEnum(key))
        {
            case ProcessFunctionType::Distance:
            {
                distance = std::stod(val);

                break;
            }

            case ProcessFunctionType::ElectronicFunction:
            {
                std::string option;
                sline >> option;
                if (option.empty()) { error_manager_->SKeyValError("state process", key, val); }

                this->ProcessElectronicFunction(electronic_function, val, option);

                break;
            }

            case ProcessFunctionType::HamiltonianCoefficient:
            {
                hamiltonian_coefficient = std::stoi(val);
                if (hamiltonian_coefficient < 0 || hamiltonian_coefficient > 2)
                {
                    error_manager_->SKeyValError("state process", key, val);
                }

                break;
            }

            case ProcessFunctionType::InteractionState:
            {
                interaction_state = val;

                break;
            }

            case ProcessFunctionType::TargetStates:
            {
                std::istringstream sval(val);
                std::string i_state;
                while (std::getline(sval, i_state, ','))
                {
                    if (i_state.empty()) { error_manager_->SKeyValError("state process", key, val); }

                    target_state.push_back(i_state);
                }

                break;
            }

            case ProcessFunctionType::ProcessTime:
            {
                process_time = std::stod(val);

                break;
            }

            case ProcessFunctionType::RateConstant:
            {
                rate_constant = std::stod(val);

                break;
            }

            case ProcessFunctionType::Type:
            {
                type = val;

                break;
            }

            case ProcessFunctionType::Vibration:
            {
                if (val == "marcus") { vibration_function = std::make_unique<MarcusFunction>(); }
                else
                {
                    error_manager_->SKeyValError("state process", key, val);
                }

                break;
            }

            case ProcessFunctionType::Default:
            {
                error_manager_->SKeyValError("state process", key, val);

                break;
            }

            default:
            {
                error_manager_->SKeyValError("", "", "");

                break;
            }
        }
    }

    if (type == "fermigoldenrule")
    {
        process_function = std::make_unique<FermiGoldenRuleFunction>(interaction_state, target_state, error_manager_, process_time,
                                                                        hamiltonian_coefficient, electronic_function,
                                                                        vibration_function);
    } else if (type == "distancerate")
    {
        if (distance <= 0) { error_manager_->SKeyValError("state_process", "distance", "negative"); }
        else if (rate_constant <= 0) { error_manager_->SKeyValError("state_process", "rate_constant", "negative"); }
        else
        {
            process_function = std::make_unique<DistanceRateFunction>(interaction_state, target_state, error_manager_, process_time,
                                                                        hamiltonian_coefficient, distance, rate_constant);
        }
    } else
    {
        error_manager_->SKeyValError("state process", "type", type);
    }
}

void FileManager::ProcessRunSection (std::ifstream& in_file, int& n_step, int& n_traj, bool& read_sdn,
                                     double& temperature, arma::uvec& boundaries, arma::dvec& cut_off) const
{
    std::string line;
    while (std::getline(in_file, line))
    {
        if (this->ProcessLine(line)) { continue; }

        std::transform(line.begin(), line.end(), line.begin(),
            [] (char c) { return std::tolower(c); });

        std::istringstream sline(line);
        std::string key;
        sline >> key;

        std::string val;
        sline >> val;
        if (key == "$$") { break; }
        if (key[0] != '$' && val.empty()) { error_manager_->SKeyValError("run", key, val); }

        switch (this->StringToRunKeysEnum(key))
        {
            case RunKeys::Boundaries:
            {
                if (val == "pdb") { boundaries.fill(1); }
                else
                {
                    if (std::count(val.begin(), val.end(), ',') != 2)
                    {
                        error_manager_->SKeyValError("run", key, val);
                    }

                    std::istringstream sval(val);
                    std::string bound;
                    for (size_t i = 0; i < 3; i++)
                    {
                        std::getline(sval, bound, ',');
                        if (bound.empty()) { error_manager_->SKeyValError("run", key, val); }
                        
                        uint u_val = std::stoul(bound);
                        if (u_val > 1) { error_manager_->SKeyValError("run", key, val); }

                        boundaries(i) = u_val;
                    }
                }

                break;
            }

            case RunKeys::CutOff:
            {
                if (std::count(val.begin(), val.end(), ',') != 2)
                {
                    error_manager_->SKeyValError("run", key, val);
                }

                std::istringstream sval(val);
                std::string coff;
                for (size_t i = 0; i < 3; i++)
                {
                    std::getline(sval, coff, ',');
                    if (coff.empty()) { error_manager_->SKeyValError("run", key, val); }

                    double d_val = std::stod(coff);
                    if (d_val < 0) { error_manager_->SKeyValError("run", key, val); }

                    cut_off(i) = d_val;
                }
                
                break;
            }

            case RunKeys::MaxSteps:
            {
                n_step = std::stoi(val);
                if (n_step < 1) { error_manager_->SKeyValError("run", key, val); }
                
                break;
            }

            case RunKeys::ReadSDN:
            {
                if (val != "true" && val != "false") { error_manager_->SKeyValError("run", key, val); }
                std::istringstream ss(val);
                ss >> std::boolalpha >> read_sdn;

                break;
            }

            case RunKeys::Temperature:
            {
                temperature = std::stod(val);
                if (temperature < 1e-8) { error_manager_->SKeyValError("run", key, val); }

                break;
            }

            case RunKeys::Trajectories:
            {
                n_traj = stoi(val);
                if (n_traj < 1) { error_manager_->SKeyValError("run", key, val); }

                break;
            }

            case RunKeys::Default:
            {
                error_manager_->SKeyValError("run", key, val);

                break;
            }

            default:
            {
                error_manager_->SKeyValError("", "", "");

                break;
            }
        }
    }
}

void FileManager::ProcessStateSection(std::ifstream& in_file, int& initial_num, int& size, std::string& label,
                                        std::unique_ptr<BaseElectronicFunction>& electronic_function,
                                        std::unique_ptr<Hamiltonian>& hamiltonian,
                                        std::vector<std::unique_ptr<BaseProcessFunction>>& process_list,
                                        std::vector<std::unique_ptr<TransitionFunction>>& transition_list) const
{
    std::string line;
    while (std::getline(in_file, line))
    {
        if (this->ProcessLine(line)) { continue; }

        std::transform(line.begin(), line.end(), line.begin(),
            [] (char c) { return std::tolower(c); });

        std::istringstream sline(line);
        std::string key;
        sline >> key;

        std::string val;
        sline >> val;
        if (key == "$$") { break; }
        if (key[0] != '$' && val.empty()) { error_manager_->SKeyValError("state", key, val); }

        switch (this->StringToStateKeysEnum(key))
        {
            case StateKeys::ElectronicFunction:
            {
                std::string option;
                sline >> option;
                if (option.empty()) { error_manager_->SKeyValError("state", key, val); }

                this->ProcessElectronicFunction(electronic_function, val, option);

                break;
            }

            case StateKeys::Hamiltonian:
            {
                if (hamiltonian != nullptr) { error_manager_->SKeyValError("state", key, val); }
                this->ProcessHamiltonian(in_file, hamiltonian);

                break;
            }

            case StateKeys::InitialNum:
            {
                initial_num = std::stoi(val);
                if (initial_num < 0) { error_manager_->SKeyValError("state", key, val); }

                break;
            }

            case StateKeys::Label:
            {
                label = val;

                break;
            }

            case StateKeys::ProcessFunction:
            {
                std::unique_ptr<BaseProcessFunction> process_function;
                this->ProcessProcessFunction(in_file, process_function);

                process_list.push_back(std::move(process_function));

                break;
            }

            case StateKeys::Size:
            {
                size = std::stoi(val);
                if (size < 1) { error_manager_->SKeyValError("state", key, val); }

                break;
            }

            case StateKeys::TransitionFunction:
            {
                std::unique_ptr<TransitionFunction> transition_function;
                this->ProcessTransitionFunction(in_file, transition_function);

                transition_list.push_back(std::move(transition_function));

                break;
            }

            case StateKeys::Default:
            {
                error_manager_->SKeyValError("state", key, val);

                break;
            }

            default:
            {
                error_manager_->SKeyValError("", "", "");

                break;
            }
        }
    }
}

void FileManager::ProcessTransitionFunction (std::ifstream& in_file, std::unique_ptr<TransitionFunction>& transition_function) const
{
    //* General Process Variables
    std::string interaction_state;
    std::unique_ptr<BaseElectronicFunction> electronic_function;

    std::string line;
    while (std::getline(in_file, line))
    {
        if (this->ProcessLine(line)) { continue; }

        std::transform(line.begin(), line.end(), line.begin(),
            [] (char c) { return std::tolower(c); });

        std::istringstream sline(line);
        std::string key;
        sline >> key;

        std::string val;
        sline >> val;
        if (key == "$$") { break; }
        if (key[0] != '$' && val.empty()) { error_manager_->SKeyValError("state transition", key, val); }

        switch (this->StringToTransitionFunctionTypeEnum(key))
        {
            case TransitionFunctionType::ElectronicFunction:
            {
                std::string option;
                sline >> option;
                if (option.empty()) { error_manager_->SKeyValError("state transition", key, val); }

                this->ProcessElectronicFunction(electronic_function, val, option);

                break;
            }

            case TransitionFunctionType::InteractionState:
            {
                interaction_state = val;

                break;
            }

            case TransitionFunctionType::Default:
            {
                error_manager_->SKeyValError("state transition", key, val);

                break;
            }

            default:
            {
                error_manager_->SKeyValError("", "", "");

                break;
            }
        }
    }

    transition_function = std::make_unique<TransitionFunction>(interaction_state, electronic_function);
}

AnalysisKeys FileManager::StringToAnalysisKeysEnum (const std::string str) const
{
    if (str == "diffusion_coeff") { return AnalysisKeys::DiffCoeff; }
    if (str == "diffusion_coeff_subtraj") { return AnalysisKeys::DiffCoeffSubTraj; }
    if (str == "lifetime") { return AnalysisKeys::Lifetime; }
    if (str == "save_trajectory") { return AnalysisKeys::SaveTrajectory; }
    if (str == "total_count") { return AnalysisKeys::TotalCount; }
    
    return AnalysisKeys::Default;
}

ElectronicFunctionType FileManager::StringToElectronicFunctionTypeEnum (const std::string str) const
{
    if (str == "const") { return ElectronicFunctionType::Constant; }
    if (str == "dectree") { return ElectronicFunctionType::DecisionTree; }
    if (str == "ffnn") { return ElectronicFunctionType::FeedForwardNeuralNetwork; }
    
    return ElectronicFunctionType::Default;
}

HamiltonianType FileManager::StringToHamiltonianTypeEnum (const std::string str) const
{
    if (str == "character") { return HamiltonianType::CharacterFunction; }
    if (str == "exchange_coupling") { return HamiltonianType::ExchangeCouplingFunction; }

    return HamiltonianType::Default;
}

ProcessFunctionType FileManager::StringToProcessFunctionTypeEnum (const std::string str) const
{
    if (str == "distance") { return ProcessFunctionType::Distance; }
    if (str == "elec_func") { return ProcessFunctionType::ElectronicFunction; }
    if (str == "hamiltonian_coef") { return ProcessFunctionType::HamiltonianCoefficient; }
    if (str == "interaction_state") { return ProcessFunctionType::InteractionState; }
    if (str == "process_time") { return ProcessFunctionType::ProcessTime; }
    if (str == "rate_constant") { return ProcessFunctionType::RateConstant; }
    if (str == "target_states") { return ProcessFunctionType::TargetStates; }
    if (str == "type") { return ProcessFunctionType::Type; }
    if (str == "vibrations") { return ProcessFunctionType::Vibration; }

    return ProcessFunctionType::Default;
}

RunKeys FileManager::StringToRunKeysEnum (const std::string str) const
{
    if (str == "boundaries") { return RunKeys::Boundaries; }
    if (str == "cutoff") { return RunKeys::CutOff; }
    if (str == "max_step") { return RunKeys::MaxSteps; }
    if (str == "read_sdn") { return RunKeys::ReadSDN; }
    if (str == "temperature" || str == "temp") { return RunKeys::Temperature; }
    if (str == "trajectories" || str == "traj") { return RunKeys::Trajectories; }

    return RunKeys::Default;
}

StateKeys FileManager::StringToStateKeysEnum (const std::string str) const
{
    if (str == "elec_func") { return StateKeys::ElectronicFunction; }
    if (str == "$hamiltonian") { return StateKeys::Hamiltonian; }
    if (str == "initial_num") { return StateKeys::InitialNum; }
    if (str == "label") { return StateKeys::Label; }
    if (str == "$process") { return StateKeys::ProcessFunction; }
    if (str == "size") { return StateKeys::Size; }
    if (str == "$transition") { return StateKeys::TransitionFunction; }

    return StateKeys::Default;
}

Sections FileManager::StringToSectionsEnum (const std::string str) const
{
    //if (str == "$algorithm") { return Sections::Algorithm; }
    if (str == "$analysis") { return Sections::Analysis; }
    if (str == "$run") { return Sections::Run; }
    if (str == "$state") { return Sections::State; }

    return Sections::Default;
}

TransitionFunctionType FileManager::StringToTransitionFunctionTypeEnum (const std::string str) const
{
    if (str == "elec_func") { return TransitionFunctionType::ElectronicFunction; }
    if (str == "interaction_state") { return TransitionFunctionType::InteractionState; }

    return TransitionFunctionType::Default;
}