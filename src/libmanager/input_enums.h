#pragma once


enum class AnalysisKeys
{
    Default,
    DiffCoeff,
    DiffCoeffSubTraj,
    Lifetime,
    SaveTrajectory,
    TotalCount
};

enum class DecTreeInputType
{
    Condition,
    Default,
    EndLayer,
    FalseDecision,
    LeftHandIndex,
    RightHandValue,
    TrueDecision
};

enum class ElectronicFunctionType
{
    Default,
    Constant,
    DecisionTree,
    FeedForwardNeuralNetwork
};

enum class FFNNInputType
{
    Default,
    Activation,
    BatchInputShape,
    EndLayer,
    Name,
    Units,
    UseBias
};

enum class HamiltonianType
{
    Default,
    CharacterFunction,
    ExchangeCouplingFunction
};

enum class ProcessFunctionType
{
    Default,
    Distance,
    ElectronicFunction,
    HamiltonianCoefficient,
    InteractionState,
    ProcessTime,
    RateConstant,
    TargetStates,
    Type,
    Vibration
};

enum class RunKeys
{
    Default,
    Boundaries,
    CutOff,
    MaxSteps,
    ReadSDN,
    Temperature,
    Trajectories
};

enum class Sections
{
    Default,
    //Algorithm,
    Analysis,
    Run,
    State
};

enum class StateKeys
{
    Default,
    ElectronicFunction,
    Hamiltonian,
    InitialNum,
    Label,
    ProcessFunction,
    Size,
    TransitionFunction
};

enum class TransitionFunctionType
{
    Default,
    ElectronicFunction,
    InteractionState
};