#include "file_manager_parse_ffnn.h"

#include <algorithm>


void ProcessLayerDense (std::ifstream& ffnn_file, std::string ffnn_name, std::vector<std::unique_ptr<FFNNLayerDense>>& layer_dense_list)
{
    std::string activation;
    int n_input = -1;
    int n_output = -1;
    std::string l_name;
    bool use_bias = false;

    bool end_input_parse = false;
    std::string line;
    while (std::getline(ffnn_file, line))
    {
        std::transform(line.begin(), line.end(), line.begin(),
            [] (char c) { return std::tolower(c); });

        std::erase_if(line, [] (const char& c)
            {
                return (c == '\"' || c == ':' || c == ',');
            });

        std::istringstream sline(line);
        std::string key;
        std::string val;

        sline >> key;
        sline >> val;
        switch (StringToFFNNInputTypeEnum(key))
        {
            case FFNNInputType::Activation:
            {
                activation = val;
                
                break;
            }

            case FFNNInputType::BatchInputShape:
            {
                std::getline(ffnn_file, line);
                std::getline(ffnn_file, line);
                n_input = std::stoi(line);

                break;
            }

            case FFNNInputType::Name:
            {
                l_name = val;

                break;
            }

            case FFNNInputType::Units:
            {
                n_output = std::stoi(val);
                
                break;
            }

            case FFNNInputType::UseBias:
            {
                if (val == "true") { use_bias = true; }
                else if (val == "false") { use_bias = false; }

                break;
            }

            case FFNNInputType::EndLayer:
            {
                end_input_parse = true;

                break;
            }

            default:
                break;
        }

        if (end_input_parse) { break; }
    }

    if (n_input < 0) { n_input = layer_dense_list.back()->GetNOutput(); }

    arma::dmat layer_weights = arma::dmat(n_output, n_input, arma::fill::zeros);
    layer_weights.load(arma::hdf5_name(ffnn_name, l_name+"_weights"));
    layer_weights.resize(n_output, n_input);
    layer_weights = layer_weights.t();

    arma::dmat layer_biases_aux = arma::dmat(1, n_output, arma::fill::zeros);
    if (use_bias) { layer_biases_aux.load(arma::hdf5_name(ffnn_name, l_name+"_biases")); }
    arma::dvec layer_biases = layer_biases_aux.row(0).t();

    std::unique_ptr<FFNNLayerDense> new_layer = std::make_unique<FFNNLayerDense>(layer_weights, layer_biases, activation);
    layer_dense_list.push_back(std::move(new_layer));
}

void ReadFFNNFunction (std::ifstream& ffnn_file, std::string ffnn_name, std::unique_ptr<BaseElectronicFunction>& electronic_function)
{
    std::vector<std::unique_ptr<FFNNLayerDense>> hidden_layers;

    std::string line;
    while (std::getline(ffnn_file, line))
    {
        std::transform(line.begin(), line.end(), line.begin(),
            [] (char c) { return std::tolower(c); });

        std::erase_if(line, [] (const char& c)
            {
                return (c == '\"' || c == ':' || c == ',');
            });
        
        std::istringstream sline(line);
        std::string key;
        std::string val;

        sline >> key;
        sline >> val;

        if (key == "class_name" && val == "dense") { ProcessLayerDense(ffnn_file, ffnn_name, hidden_layers); }
    }

    electronic_function = std::make_unique<FFNNFunction>(hidden_layers);
}

FFNNInputType StringToFFNNInputTypeEnum (std::string str)
{
    if (str == "activation") { return FFNNInputType::Activation; }
    if (str == "batch_input_shape") { return FFNNInputType::BatchInputShape; }
    if (str == "name") { return FFNNInputType::Name; }
    if (str == "units") { return FFNNInputType::Units; }
    if (str == "use_bias") { return FFNNInputType::UseBias; }
    if (str == "}") { return FFNNInputType::EndLayer; }

    return FFNNInputType::Default;
}