#include "error_manager.h"

#include "file_manager.h"

#include <iostream>

//* Public Methods

void ErrorManager::SAlgorithmError (std::string str)
{
    std::string error = "System has crashed due to an error in Algorithm section:\n";
    error += "--------------------------------------------------------\n\n";
    
    if (str.empty()) { error += "Non declared error encountered"; }
    else { error += str; }

    file_manager_->WriteString(error);
    this->SCrash();
}

void ErrorManager::SAnalysisError (std::string str)
{
    std::string error = "System has crashed due to an error in Analysis section:\n";
    error += "-------------------------------------------------------\n\n";

    if (str.empty()) { error += "Non declared error encountered"; }
    else { error += str; }

    file_manager_->WriteString(error);
    this->SCrash();
}

void ErrorManager::SetFileManager (FileManager* file_manager)
{
    file_manager_ = file_manager;
}

void ErrorManager::SFileError (std::string f_type, std::string f_name)
{
    std::string error;
    if (f_name.empty()) { error = f_type + " file was not specified"; }
    else { error = "File " + f_name + " could not be openend"; }
    
    file_manager_->WriteString(error);
    this->SCrash();
}

void ErrorManager::SInputError (std::string str)
{
    std::string error;
    if (str.empty()) { error = "Some sections may be missing in the input file"; }
    else { error = str; }

    file_manager_->WriteString(error);
    this->SCrash();
}

void ErrorManager::SKeyValError (std::string section, std::string key, std::string val)
{
    std::string error;
    if (key.empty()) { error = "Something wrong happened with the " + section + " Keys"; }
    else { error = "Unrecognised " + section + " Key - Value pair\tKey: " + key + " Val: " + val; }

    file_manager_->WriteString(error);
    this->SCrash();
}

void ErrorManager::SOutputError ()
{
    std::cout << "It was not possible to generate output file" << std::endl;
    exit(1);
}

void ErrorManager::SSectionKeyError (std::string key)
{
    std::string error;
    if (key.empty()) { error = "Something wrong happened with the Section Keys"; }
    else { error = "Unrecognised Section Key: " + key; }

    file_manager_->WriteString(error);
    this->SCrash();
}

void ErrorManager::SSectionValueError (std::string val)
{
    std::string error;
    if (val.empty()) { error = "Something wrong happened with the Section Values"; }
    else { error = "Unrecognised Section Value in Section: " + val; }

    file_manager_->WriteString(error);
    this->SCrash();
}

void ErrorManager::SStateError (std::string str)
{
    std::string error = "System has crashed due to an error in State section:\n";
    error += "----------------------------------------------------\n\n";

    if (str.empty()) { error += "Non declared error encountered"; }
    else { error += str; }

    file_manager_->WriteString(error);
    this->SCrash();
}

void ErrorManager::SSystemError (std::string str)
{
    std::string error = "System has crashed due to an error:\n";
    error += "-----------------------------------\n\n";

    if (str.empty()) { error += "Non declared error encountered"; }
    else { error += str; }

    file_manager_->WriteString(error);
    this->SCrash();
}

//* Private Methods

void ErrorManager::SCrash ()
{
    file_manager_->ExitSimulation();
    exit(1);
}