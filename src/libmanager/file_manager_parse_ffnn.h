/**
 * @file file_manager_parse_ffnn.h
 * @author Aitor Diaz-Andres (ad.andres@outlook.com)
 * @brief Parse Feed Forward Neural Network information from input file.
 * @date 2024-05-21
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#pragma once

#include "../libelectronicfunction/ffnn_function.h"
#include "../libelectronicfunction/ffnn_layer_dense.h"
#include "input_enums.h"

#include <fstream>
#include <memory>
#include <string>


/**
 * @brief Reads the FFNN Layer section.
 * 
 * The FFNN layer involves the input and output nodes, the weights and biases for
 * the LayerDense. For each layer read in the input file, one FFNNLayerDense is construct
 * and stored in the vector.
 * 
 * @param ffnn_file FFNN input file.
 * @param ffnn_name FFNN input file name.
 * @param layer_dense_list Empty vector of FFNNLayerDense object references.
 */
void ProcessLayerDense (std::ifstream& ffnn_file, std::string ffnn_name,
                        std::vector<std::unique_ptr<FFNNLayerDense>>& layer_dense_list);

/**
 * @brief Reads the FFNN input file.
 * 
 * This block of code reads the input FFNN file (.json) and the associated weights and biases
 * input files (.h5). This function call ProcessLayerDense to generate sequentially all the
 * FFNNLayerDense objects and constructs a new FFNNLFunction object.
 * 
 * @param ffnn_file FFNN input file.
 * @param ffnn_name FFNN input file name.
 * @param electronic_function Empty BaseElectronicFunction object reference.
 */
void ReadFFNNFunction (std::ifstream& ffnn_file, std::string ffnn_name,
                        std::unique_ptr<BaseElectronicFunction>& electronic_function);

/**
 * @brief Interprets the FFNN input key and returns a FFNNInputType.
 * 
 * @param str Key string to be interpreted.
 * @return FFNNInputType 
 */
FFNNInputType StringToFFNNInputTypeEnum (std::string str);