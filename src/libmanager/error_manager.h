/**
 * @file error_manager.h
 * @author Aitor Diaz-Andres (ad.andres@outlook.com)
 * @brief ErrorManager class declaration.
 * @date 2024-05-20
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#pragma once

#include <string>


class FileManager;

/**
 * @brief Error Manager to manage any error from the code.
 * 
 * This class will be called every time there is an error in the execution.
 * 
 * @warning Inherit from Singleton in the future.
 */
class ErrorManager
{
private:
    FileManager* file_manager_;                                                 // 8

public:
    /**
     * @brief Construct a new ErrorManager object
     * 
     */
    ErrorManager () = default;

    /**
     * @brief Destroy the ErrorManager object
     * 
     */
    ~ErrorManager () = default;

    /**
     * @brief Launches an error in the algorithm section.
     * 
     * @param str String with the algorithm section.
     */
    void SAlgorithmError (std::string str);

    /**
     * @brief Launches an error in the analysis section.
     * 
     * @param str String with the analysis section.
     */
    void SAnalysisError (std::string str);

    /**
     * @brief Set the FileManager object.
     * 
     * @param file_manager FileManager object reference.
     */
    void SetFileManager (FileManager* file_manager);

    /**
     * @brief Launches an error with trying to read a file.
     * 
     * @param f_type Type of the file.
     * @param f_name Name of the file.
     */
    void SFileError (std::string f_type, std::string f_name);

    /**
     * @brief Launches an error in the input file.
     * 
     * @param str String with the error.
     */
    void SInputError (std::string str);

    /**
     * @brief Launches an error in a section of the input file associated with
     * a key and value.
     * 
     * @param section String with the input section.
     * @param key String with the key.
     * @param val String with the value.
     */
    void SKeyValError (std::string section, std::string key, std::string val);

    /**
     * @brief Launches an error when no output file can be generated.
     * 
     */
    void SOutputError ();

    /**
     * @brief Launches an error in any section of the input file.
     * 
     * @param key String with the error key.
     */
    void SSectionKeyError (std::string key);

    /**
     * @brief Launches an error in any section of the input file.
     * 
     * @param val String with the error value. 
     */
    void SSectionValueError (std::string val);

    /**
     * @brief Launches an error in any State of the input file.
     * 
     * @param str String with the error.
     */
    void SStateError (std::string str);

    /**
     * @brief Launches an error in the System of the input file.
     * 
     * @param str String with the error.
     */
    void SSystemError (std::string str);

private:
    /**
     * @brief Launch the final step of the error by closing files and exiting the software.
     * 
     */
    void SCrash ();
};