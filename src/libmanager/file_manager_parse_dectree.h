/**
 * @file file_manager_parse_dectree.h
 * @author Aitor Diaz-Andres (ad.andres@outlook.com)
 * @brief Parse Decision Tree information from input file.
 * @date 2024-05-21
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#pragma once

#include "../libelectronicfunction/base_electronic_function.h"
#include "../libelectronicfunction/dectree_layer.h"
#include "input_enums.h"

#include <fstream>
#include <memory>
#include <string>


/**
 * @brief Reads the Decision Tree Layer section.
 * 
 * The Decision Tree layer involves the left and right hand sides of the conditional
 * evaluation with the corresponding condition. Besides, this function reads the true or
 * false decisions. These decision can be values or another DecTreeLayer objects.
 * 
 * @param dectree_file Decision Tree input file.
 * @param dectree_layer Empty DecTreeLayer object reference.
 */
void ProcessDecTreeLayer (std::ifstream& dectree_file, std::unique_ptr<DecTreeLayer>& dectree_layer);

/**
 * @brief Reads the Decision Tree input file.
 * 
 * This block of code reads the input Decision Tree file (.json) and asks to read the 
 * "tree root". This tree root is the first DecTreeLayer of the DecTreeFunction. From
 * this layer generates the DecTreeFunction object.
 * 
 * @param dectree_file Decision Tree input file.
 * @param electronic_function Empty BaseElectronicFunction object reference.
 */
void ReadDecTreeFunction (std::ifstream& dectree_file, std::unique_ptr<BaseElectronicFunction>& electronic_function);

/**
 * @brief Interprets the Decision Tree input key and returns a DecTreeInputType.
 * 
 * @param str Key string to be interpreted.
 * @return DecTreeInputType.
 */
DecTreeInputType StringToDecTreeInputTypeEnum (std::string str);