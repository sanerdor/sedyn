#pragma once

/**
 * @mainpage Solid State Exciton Dynamics
 * 
 * Some kind of small description or definition of the software.
 * 
 * @authors Aitor Diaz-Andres 2022--present
 * 
 * @section sedyn_contents Contents:
 * - @subpage sedyn_introduction
 * - @subpage sedyn_installation
 * - @subpage sedyn_inputs
 * - @subpage sedyn_examples
 * 
 */

/**
 * @page sedyn_introduction Introduction
 * 
 * @tableofcontents
 * 
 * @section sed_intro_manual General Information
 * 
 * This manual contains all the information regarding SEDYN, a Solid state Exciton Dynamics simulation software. The
 * manual explores some of the theoretical background behind SEDYN and all the user parameters to tune the simulations
 * as desired. It is assumed the user has some basic knowledge of Unix/Linux, a basic understanding of quantum chemistry
 * and a text editor.
 * 
 * @subsection sed_intro_summaries Chapter Summaries (Q-Chem)
 * 
 * @link sedyn_introduction **Sec. 1:** @endlink General SEDYN information, contributors and contact information.\n 
 * @link sedyn_installation **Sec. 2:** @endlink Installation in Unix/Linux machines, SEDYN customization and general
 * execution protocol.\n 
 * @link sedyn_inputs **Sec. 3:** @endlink Manual of input and output files. All the keywords are described in this
 * section.\n 
 * @link sedyn_examples **Sec. 4:** @endlink Collection of examples to run SEDYN simulations.
 * 
 * @section sed_intro_contributors Contributors
 * 
 * Hello! My name is Aitor Diaz-Andres. I am the only contributor to the code. I leave my contact information to answer
 * any questions regarding the code and the input/output format.
 * - **E-mail:** ad.andres@outlook.com
 * - **Gitlab:** gitlab.com/sanerdor
 * 
 * @section sed_intro_features SEDYN Features
 * @subsection sed_intro_overview Overview of SEDYN Features
 * 
 * SEDYN offers a blend of accurate quantum chemistry methods and fast and scalable simulation algorithms. Quantum
 * chemistry methods have shown promising results in describing the electronic structure of molecules and aggregates as
 * well as describing the interaction between molecules. This capability is sometimes constrained by the computational
 * cost of many theoretical methods, hence, for large molecular systems with large amounts of electrons many of the
 * quantum chemistry calculations are forces to make approximations.
 * 
 * On the other hand, exciton and charge simulations in molecular structures are of great use in studying the properties
 * of molecular systems, such as charge mobility, singlet fission/fusion and light emission. However, most if not all of
 * the software approximates the molecules as points in space. The simulations are performed from point to point in
 * space and the probability of the transitions are governed by parametrized functions. Although this approximation
 * allows very fast and reliable simulations for organic crystals, it becomes hard to parametrize and simulate amorphous
 * systems.
 * 
 * SEDYN removes the approximation of points in space and stores all the atomic coordinates in memory. Similar to
 * quantum chemistry calculations, SEDYN uses these atomic coordinates to predict the electronic couplings of
 * interacting molecules and excited state energies. Besides, instead of applying parametrized functions, SEDYN predicts
 * these quantities through machine learning algorithms previously trained, such as Decision Trees and Feed Forward
 * Neural Networks as well as other non machine learning algorithms. This approach to the prediction of the transition
 * probability provides more complex simulations and crystal and amorphous molecular solids.
 * 
 * @subsection sed_intro_feat22 Changes in SEDYN 2.2
 * 
 * - Redesign of the SDN file.\n 
 *  - HDF5 dependence (in this file) removed. Now the neighbours are saved as a vector of pairs in ASCII.\n 
 *  - Neighbours file now is more lightweight.\n 
 * - Redesign of analysis keywords.\n 
 *  - General state analysis removed. Now, for each analysis type, the user can declare which states are to be
 * analysed.\n 
 *  - Analysis class has been modified to accept any new analysis feature by adding an enumerator instead of individual 
 * boolean keywords.\n
 *  - New @link sedyn_input_form_analysis_diff_subtraj diffusion_coeff_subtraj @endlink keyword to split long
 * trajectories to compute diffusion coefficient.\n
 *  - New @link sedyn_input_form_analysis_savetraj save_trajectory @endlink keyword to save the trajectories of declared
 * states.\n 
 *  - New @link sedyn_input_form_analysis_totalcount total_count @endlink keyword to record the number of time a state
 * is registered.\n
 * 
 * @subsection sed_intro_feat21 Changes in SEDYN 2.1
 * 
 * - Redesign of the code to remove Global Objects.\n 
 *  - Remove Analysis global object.\n 
 *  - Remove ErrorManager global object.\n 
 *  - Remove FileManager global object.\n 
 *  - Remove System global object.\n 
 * - Documentation of the code.\n 
 *  - Now the code is documented in gitlab pages. In the main page of gitlab can be found the link.\n 
 * 
 * @subsection sed_intro_feat20 Changes in SEDYN 2.0
 * 
 * - Implementation of libmanager to remove libinput and liboutput.\n 
 *  - New libmanager manages input/output features within one class.\n 
 *  - New error manager to handle the errors throughout the code and crash the software.\n 
 *  - New input file interpretation code with enumerators.\n 
 *  - Grant input file manager the ability to read external files such as Decision Tree data.\n 
 * - Remove Dynamic Monte Carlo algorithm.\n 
 * - Rewrite electronic data classes.\n 
 *  - Remove classical Forster's equation.\n 
 * - Implementation of Coulomb Matrix withing molecule class.\n
 *  - Simplify the dependency of the molecule class.\n 
 * - Redesing the processes for easier development.\n 
 *  - Remove Lifetime decay function for later reintroduction.\n 
 * - Redesing of SEDYN main and system class.\n 
 *  - Grant system the ability to run trajectories.\n 
 * 
 * @subsection sed_intro_feat10 Changes in SEDYN 1.0
 * 
 * - Standard input and output libraries.\n 
 *  - Input file interpreter in libinput.\n 
 *  - External data files for machine learning algorithms included in libinput.\n 
 *  - General output file created and filled in liboutput.\n 
 * - Two algorithms to simulate with similar properties.\n 
 *  - Kinetic Monte Carlo algorithm.\n 
 *  - Dynamic Monte Carlo algorithm.\n 
 * - Single machine learning model to predict electronic data.\n 
 *  - Feed Forward Neural Network (FFNN) model.\n 
 * - Representation of molecules.\n 
 *  - Storage of atomic coordinates and symbols.\n 
 *  - Calculation of nearest neighbours according to a custom cutoff.\n 
 *  - Reading of nearest neighbours from external files. This file is an output of SEDYN from a previous simulation.\n 
 * - Desing of processes for one or two molecules.\n 
 *  - Fermi's Golden Rule for the calculation of transition probability for two molecules.\n 
 *      - The calculation of Frank Condon Weighted Density of states is performed with Marcus' theory.\n 
 *  - Distance custom rate returns a custom rate according to the distance between two molecules.\n 
 *  - Lifetime decay function returns a lifetime-dependent rate.
 *      - The probability is computed from a random number and the age/lifetime proportions.\n 
 * - New class to gather the information regarding a state.\n 
 *  - State class stores reference to processes, molecules and electronic functions.\n 
 * - General algorithm to manage the simulation.\n 
 *  - System class stores all the states, molecules and selection algorithm in the simulations.\n 
 *  - For every step in a trajectory the System updates the internal state-molecule relationship.\n 
 *  - The information on the processes is stored for later analysis.
 * 
 * @section sed_intro_citing Citing SEDYN
 * 
 * There is no publication to cite SEDYN so I would appreciate if the user could write the link to the gitlab url and/or
 * write some acknowlegments.
 */

/**
 * @page sedyn_installation Installation, Customization and Execution
 * 
 * @tableofcontents
 * 
 * @section sed_instal_installing Installing SEDYN
 * @subsection sed_instal_downloading Downloading SEDYN
 * 
 * SEDYN software can be downloaded from <a href="https://gitlab.com/sanerdor/sedyn/-/releases">Sedyn releases</a>. The
 * latests version of the software is 2.0.0 which can be found in the link. The user can download the compress package,
 * uncompress it and follow with the installation steps. Another option to download the code it to clone the public
 * <a href="https://gitlab.com/sanerdor/sedyn.git">repository</a> and checkout the lastest version branch.
 * 
 * In any of the above options, the user will download a directory with all the compilation and source files.
 * 
 * @subsection sed_instal_requirements Installation Requirements (Ubuntu)
 * 
 * The following packages are required:
 * - CMake      (sudo apt install cmake)
 * - OpenMP     (sudo apt install libomp-dev)
 * - OpenBLAS   (sudo apt install libopenblas-dev)
 * - LAPACK     (sudo apt install liblapack-dev)
 * - HDF5       (sudo apt install libhdf5-dev)
 * 
 * SEDYN requires Armadillo to run to code. It is recommended to install armadillo with all the optional packages.
 * <a href="https://arma.sourceforge.net/download.html">Armadillo Downloads</a>.
 * 
 * @subsection sed_instal_compile Compiling the code (Ubuntu)
 * 
 * The general compilation can be done in two steps, configuring the software and compiling.
 * @code {.sh}
 * ./configure gcc release
 * cd build
 * make
 * @endcode
 * 
 * @subsubsection sed_instal_compile_omp OpenMP parallelization
 * 
 * OpenMP parallelization can be used for speed up but must be declared when compiling the code. In the configuration
 * file there is an option to activate the OpenMP parallel execution.
 * @code {.sh}
 * ./configure gcc release omp
 * @endcode
 * 
 * @section sed_instal_running Running SEDYN
 * 
 * @subsection sed_instal_usage General Usage
 * 
 * Once the installation is finished there are two steps to run successfully a simulation with SEDYN. First, environment
 * variables must be declared and SEDYN's execution file's path must be included in the PATH. The code below is an
 * of how to declare this environment variables in Linux systems.
 * @code {.sh}
 * export PATH=$PATH:/path/to/sedyn/bin
 * export SDNWORKPATH=/path/to/working/directory
 * export OMP_NUM_THREADS=N
 * @endcode
 * In general SDNWORKPATH should be set to the current working directory where the input and external files are located.
 * Of course, SEDYN accepts relative paths for code execution but declaring SDNWORKPATH to the working directory is
 * recomended.
 * 
 * On the other hand, the execution of the software requires at least two input files. These files are the input and
 * molecular system path files which are compulsory for the simulation to be computed. Once the environment variables
 * are declared, the software can be run by typing
 * @code {.sh}
 * sedyn -i infile -mol molfile
 * @endcode
 * This simple code execution will generate one output file and sedyn checkpoint file in the SDNWORKPATH diretory, 
 * sedyn_output.out and sedyn_sdn.h5 respectively. However, there it is possible to explicitly request specific file
 * names by typing
 * @code {.sh}
 * sedyn -i infile -mol molfile -o outfile -sdn sdnfile
 * @endcode
 * **It is very important to understand that sdnfile must by a hdf5 file with .h5 extension.**
 */

/**
 * @page sedyn_inputs SEDYN Inputs
 * 
 * @tableofcontents
 * 
 * @section sed_input_form General Form
 * 
 * SEDYN is designed to use a text format input file of a series of keywords (and corresponding values). This
 * architecture allows the complete customization of the input file despite having a steep learning curve. To put is
 * simple, all the input sections are declared with *$sectionname* and finished with *$$*. The complexity of SEDYN's
 * input structure resides in the nested sections, this is, one section may be inside another one.
 * 
 * The input file has in total six sections that may be used to customize a simulation. However, three of them (run,
 * analysis and state) are compulsory; the other three sections (process, transition and hamiltonian) will not crash the
 * input reader but will critically affect the simulation.
 * 
 * Section Name | Description
 * --|--
 * @link sedyn_input_form_run *$run* @endlink | Information regarding the simulation steps, boundaries, temperature...
 * @link sedyn_input_form_analysis *$analysis* @endlink | Declaration of analysis target states and features
 * @link sedyn_input_form_state *$state* @endlink | State definition section from properties to processes and
 * hamiltonian
 * @link sedyn_input_form_process *$process* @endlink | Declaration of monomolecular or bimolecular processes
 * @link sedyn_input_form_transition *$transition* @endlink | Simple transition delta energy declaration section
 * @link sedyn_input_form_hamiltonian *$hamiltonian* @endlink | Triplet-Triplet interaction Hamiltonian declaration 
 * section
 * 
 * @subsection sedyn_input_form_run $run section
 * 
 * *$run* section is compulsory for the any simulation in SEDYN. This section rules the calculation and the most basic
 * features, hence, the basic *$run* specifications are required in order to compute any simulation. And error will be
 * 
 * @warning Error message will be thrown if the section is not found.
 * 
 * @subsubsection sedyn_input_form_run_boundaries BOUNDARIES
 * - Specifies the type of boundaries for the solid.\n 
 * - Type : Vector of integer.
 * - Default : (0, 0, 0).\n 
 * 
 * The boundaries is a vector of three integer variables (x,y,z) and each of them will determine if Periodic Boundary
 * Conditions (PBC) are applied in each of these directions. The values of the vector either 0 or 1 as these values will
 * be converted to boolean variables, hence, any integer value higher or lower will result in an error. The PBC allow
 * the system to virtully extend the trajectory of each exciton while keeping all the excitons confined. The vector
 * allows setting PBC along none, some or all of the directions.
 * 
 * @subsubsection sedyn_input_form_run_cutoff CUTOFF
 * - Sets the elipsoid cutoff radius to compute the nearest neighbours of each molecule.\n 
 * - Type : Vector of doubles.
 * - Default : (10, 10, 10) AA.\n 
 * 
 * The cutoff radius is a vector of three double variables (x,y,z) and each of them will determine the size of the
 * elipsoid. This parameters is ment to fix the first neighbours, hence, the computation time of each step by only 
 * computing the desired molecular dimers. The cutoff radius will only be used for those simulations with READ_SDN =
 * TRUE.
 * 
 * @subsubsection sedyn_input_form_run_maxstep MAX_STEP
 * - Specifies the maximun of steps to simulate.\n 
 * - Type : Integer.\n 
 * - Default : 10k.
 * 
 * It is recomended to set MAX_STEP to at least 10k steps. However, for high density of excitons the user should declare
 * more steps.
 * 
 * @subsubsection sedyn_input_form_run_temp TEMPERATURE
 * - Specifies the temperature of the simulation.\n 
 * - Type : Double.\n 
 * - Default : 298 K.
 * 
 * The temperature of the simulation will be used for internal functions such as Marcus' Theory to calculate properties.
 * To avoid any computational error due to zero division, the minimum temperature should be 1e-8 K. However, SEDYN will
 * not launch any error if the user declared temperature is below that.
 * 
 * @subsubsection sedyn_input_form_run_traj TRAJECTORIES
 * - Specifies the number of simulations to perform.\n 
 * - Type : Integer.\n 
 * - Default : 1.
 * 
 * In each trajectory the system will restart by deleting all existing excitons and injecting new ones. For the moment,
 * there is no clear advantage of increasing the number of excitons, number of steps or number of trajectory. In the
 * future, when MPI parallel arquitechture is implemented, it is expected a speed up and efficiency on the simulations.
 * 
 * @subsection sedyn_input_form_analysis $analysis section
 * 
 * The *$analysis* section will determine which excitons are going to be analysed and what information will be printed
 * in the output file. For the moment, the analysis is only related to the exciton transport, hence, those excitons that
 * will not have any transport processes should not be included in this section.
 * 
 * @subsubsection sedyn_input_form_analysis_diff DIFFUSION_COEFF
 * - Defines the states to analyse the diffusion coefficient during simulation.\n 
 * - Type : Vector of string.\n
 * 
 * @subsubsection sedyn_input_form_analysis_diff_subtraj DIFFUSION_COEFF_SUBTRAJ
 * - Defines the length of the subtrajectories for the calculation of the diffusion coefficient.\n 
 * - Type : Integer.\n 
 * - Default : 1000.
 * 
 * This parameters must be equal or higher than the number of steps of the simulation. However, for a good statistical
 * approximation, it is recomended that the number of subtrajectories is at least 10 and the minimum length is 1000.
 * The calculation of the diffusion coefficient relies on long simulations and plenty of subtrajectories.
 * 
 * @subsubsection sedyn_input_form_analysis_lifetime LIFETIME
 * - Defines the states to analyse the lifetime during simulation.\n 
 * - Type : Vector of string.\n
 * 
 * @subsubsection sedyn_input_form_analysis_savetraj SAVE_TRAJECTORY
 * - Defines the states to save their trajectory.\n 
 * - Type : Vector of string.\n 
 * 
 * The subtrajectories will be saved in ASCII text files. Each line of the text file will contain the time and x, y, z
 * vector of the position of the excitons in the simulation. SEDYN (armadillo) works in column-major ordering. To be 
 * more compatible with external tools (numpy), the trajectories are saved in row-major order.
 * 
 * @subsubsection sedyn_input_form_analysis_totalcount TOTAL_COUNT
 * - Defines the states to count all the times a state is registered.
 * - Type : Vector of string.\n 
 * 
 * @subsection sedyn_input_form_state $state section
 * 
 * The *$state* section will declare all the information to generate and work with states/excitons/charges. SEDYN will
 * require one *$state* section for each type of state in the system although they may not be included at the beginning
 * of the simulation.
 * 
 * @warning If one state in any part of the input file is requested and the corresponding *$state* section is not found,
 * SEDYN will throw an error.
 * 
 * @subsubsection sedyn_input_form_state_elecfunc ELEC_FUNC
 * - Defines how the electronic data will be obtained.
 * - Type : String variable
 * 
 * This parameters specifies the method and value to obtain the energy of the state. Here, SEDYN requires the ELEC_FUNC
 * key and two values separated by an space. The first value will be the electronic function method and the second value
 * will declare input data for the electronic function method. Below we link the electronic function methods.
 * - @ref ConstantFunction "ConstantFunction"
 * - @ref DecTreeFunction "DecTreeFunction"
 * - @ref FFNNFunction "FFNNFunction"
 * 
 * @subsubsection sedyn_input_form_hamiltonian $hamiltonian section
 * 
 * The *$hamiltonian* section allows the declaration of a Triplet-Triplet interaction spin Hamiltonian for a given
 * state. This Hamiltonian will define a Hamiltonian state according to a character probability and calculate a state
 * energy for that character. For the moment, the simplest Triplet-Triplet interaction is included, however, SEDYN is 
 * under development to include other interactions such as the anisotropy or external magnetic field.
 * 
 * @par CHARACTER
 * - Defines how the character of the state will be chosen.\n 
 * - Type : string variable.\n 
 * - Default : Character probability.
 * 
 * The character of the Hamiltonian determines the final state and therefore the energy of the state. There are two
 * options:
 * - Character probability : The user determines a probability for each character in a vector (p1, p2, p3)
 * - Boltzmann probability : The probability is determined by a Boltzmann distribution of state energies.
 * 
 * @par EXCHANGE_COUPLING
 * See @link sedyn_input_form_state_elecfunc ELEC_FUNC @endlink.
 * 
 * @subsubsection sedyn_input_form_state_initnum INITIAL_NUM
 * - Defines the number of states injected at the beginning of the simulation.\n 
 * - Type : Integer.\n 
 * - Default : 0.
 * 
 * This parameters is only available for state with SIZE = 1, hence, for larges states this parameters must be set to 0.
 * 
 * @subsubsection sedyn_input_form_state_label LABEL
 * - Defines and links a state with a string.\n 
 * - Type : String.\n 
 * 
 * There must be one label per state and never repeat the labels. SEDYN internally makes a map between reference state
 * objects and the label string. If the state label is repeated in the input file, the map will be overwritten.
 * 
 * @subsubsection sedyn_input_form_state_process $process section
 * 
 * The *$process* section will be explained in each of the class definitions. The keywords required for all of them
 * are listed and explained below but the rest of them and specific for each process type will be declared in the
 * respective class definition (links below).
 * 
 * @par HAMILTONIAN_COEF
 * - Define the Hamiltonian coefficient to be requested from *$hamiltonian* section.\n 
 * - Type : Integer.\n 
 * - Default : -1.
 * 
 * It is only allowed 0 or 1 for the moment. The default value of -1 will automatically ignore the coefficients of the
 * Hamiltonian and will be always be set to 1.
 * 
 * @par INTERACTION_STATE
 * - Define the interacting state of this process.\n 
 * - Type : String.\n 
 * 
 * The target interaction state must be present in the input file, otherwise SEDYN will throw an error.
 * 
 * @par PROCESS_TIME
 * - Define a specific process time which will overwrite the output from the algorithm.\n 
 * - Type : Double.\n 
 * - Default : -1.0.
 * 
 * The default value of negative one will ensure the usage of the algorithm to compute the process time.
 * 
 * @par TARGET_STATE
 * - Define the output states.\n 
 * - Type : Vector of strings.\n 
 * 
 * The target states will update the system to match the output vector of labels. The current state could move or be 
 * deleted from the system while new state could be created. This target state will be responsible of defining the
 * reaction of the process.
 * 
 * @par TYPE
 * - Declares the process function to be used.\n 
 * - Type : String.\n 
 * 
 * There are listed below the options available with the links of how to use and implement in the input file each of 
 * them.
 * - @ref DistanceRateFunction "DistanceRateFunction".
 * - @ref FermiGoldenRuleFunction "FermiGoldenRuleFunction".
 * 
 * @subsubsection sedyn_input_form_state_size SIZE
 * - Defines the size of the state.
 * - Type : Integer.\n 
 * 
 * Only those states with SIZE = 1 can be injected at the beginning of the simulation and can have transport processes.
 * 
 * @subsubsection sedyn_input_form_state_transition $transition section
 * 
 * The *$transition* section maps a state-state interaction with a energy function method. For some processes, the 
 * interaction energy between state can be relevant for the success of the process.
 * 
 * @par ELEC_FUNC
 * See @link sedyn_input_form_state_elecfunc ELEC_FUNC @endlink.
 * 
 * @par INTERACTION_STATE
 * - Determines the target interaction state.\n 
 * - Type : String.\n 
 * 
 * The target interaction state must be present in the input file, otherwise SEDYN will throw an error. On the other
 * hand, for each interaction between states, there must be declared in both interacting states the corresponding 
 * *$transition* section with the INTERACTION_STATE set to the interacting state.
 * 
 * @section sed_input_output SEDYN Output File (Q-Chem)
 */

/**
 * @page sedyn_examples SEDYN examples
 * 
 * @tableofcontents
 * 
 * @section sedyn_input_example_simple Simple example
 * 
 * This simple example shows how to set up the minimal keywords to run a simulation. For that the singlet exciton
 * transport in rubrene crystal solid is configured and run.
 * 
 * @code {.sh}
 * !Lines that begin with "!" symbol are ignored by the input manager.
 * 
 * ! ******
 * ! SYSTEM
 * ! ******
 * 
 * $run
 *     max_step 10000
 *     trajectories 1
 *     cutoff 20.09175,8.9625,10.65825
 *     boundaries 1,1,1
 *     temperature 298
 *     read_sdn false
 * $$
 * 
 * ! ********
 * ! ANALYSIS
 *  ********
 * 
 * 
 * $analysis
 *     diffusion_coeff s1
 *     lifetime s1
 * $$
 * 
 * ! ********
 * ! STATE S1
 * ! ********
 * 
 * $state
 *     label s1
 *     initial_num 1
 *     size 1
 *     elec_func const 2.23
 * 
 *     $process
 *         type fermigoldenrule
 *         interaction_state gs
 *         target_states gs,s1
 *         elec_func dectree seet
 *         vibrations marcus
 *     $$
 * 
 *     $transition
 *         interaction_state gs
 *         elec_func const 0.1
 *     $$
 * $$
 * @endcode
 * 
 * @section sedyn_input_example_complete Complete example
 * 
 * This example is a more complex simulation with some state transformation processes and reactions. Following the 
 * previous example, the example simulates triplet exciton transport, singlet exciton transport as well as triplet to
 * singlet transformation through TTA process.
 * 
 * @code {.sh}
 * !Lines that begin with "!" symbol are ignored by the input manager.
 * 
 * ! ******
 * ! SYSTEM
 * ! ******
 * 
 * $run
 *     max_step 10000
 *     trajectories 1
 *     cutoff 20.09175,8.9625,10.65825
 *     boundaries 1,1,1
 *     temperature 298
 *     read_sdn false
 * $$
 * 
 * ! ********
 * ! ANALYSIS
 *  ********
 * 
 * 
 * $analysis
 *     diffusion_coeff t1,s1
 *     diffusion_coeff_subtraj 1000
 *     lifetime s1
 * $$
 * 
 * ! ********
 * ! STATE T1
 * ! ********
 * 
 * $state
 *     label t1
 *     initial_num 8
 *     size 1
 *     elec_func const 1.14
 * 
 *     $process
 *         type fermigoldenrule
 *         interaction_state gs
 *         target_states gs,t1
 *         elec_func dectree teet
 *         vibrations marcus
 *     $$
 * 
 *     $process
 *         type distancerate
 *         interaction_state t1
 *         target_states tt
 *         distance 7.5
 *         rate_constant 10000
 *         process_time 0
 *     $$
 * 
 *     $transition
 *         interaction_state gs
 *         elec_func const 0.1
 *     $$
 * 
 *     $transition
 *         interaction_state t1
 *         elec_func const 0.1
 *     $$
 * 
 *     $transition
 *         interaction_state tt
 *         elec_func const 0.1
 *     $$
 * $$
 * 
 * ! ********
 * ! STATE TT
 * ! ********
 * 
 * $state
 *     label tt
 *     initial_num 0
 *     size 2
 *     elec_func const 2.28
 * 
 *     $hamiltonian
 *         exchange_coupling dectree exc
 *         character func boltzmann
 *     $$
 * 
 *     $process
 *         type fermigoldenrule
 *         hamiltonian_coef 0
 *         target_states s1,gs
 *         elec_func dectree tta
 *         vibrations marcus
 *     $$
 * 
 *     $transition
 *         interaction_state t1
 *         elec_func const 0.1
 *     $$
 * 
 *     $transition
 *         interaction_state s1
 *         elec_func const 0.1
 *     $$
 * 
 *     $transition
 *         interaction_state gs
 *         elec_func const 0.1
 *     $$
 * $$
 * 
 * ! ********
 * ! STATE S1
 * ! ********
 * 
 * $state
 *     label s1
 *     initial_num 1
 *     size 1
 *     elec_func const 2.23
 * 
 *     $process
 *         type fermigoldenrule
 *         interaction_state gs
 *         target_states gs,s1
 *         elec_func dectree seet
 *         vibrations marcus
 *     $$
 * 
 *     $transition
 *         interaction_state gs
 *         elec_func const 0.1
 *     $$
 * $$
 * @endcode
 */