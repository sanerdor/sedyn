/**
 * @file kinetic_monte_carlo.h
 * @author Aitor Diaz-Andres (ad.andres@outlook.com)
 * @brief Kinetic Monte Carlo algorithm declaration file.
 * @date 2024-05-20
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#pragma once

#include "../libmanager/error_manager.h"
#include "../libstate/rate_state_process.h"

#include <armadillo>
#include <memory>
#include <vector>


struct RateStateProcess;

/**
 * @brief Kinetic Monte Carlo algorithm class declaration.
 * 
 */
class KineticMonteCarlo
{
private:
    ErrorManager* error_manager_;

public:
    /**
     * @brief Construct a new KineticMonteCarlo object.
     * 
     */
    KineticMonteCarlo () = default;
    
    /**
     * @brief Construct a new KineticMonteCarlo object.
     * 
     * @param error_manager ErrorManager object reference.
     */
    KineticMonteCarlo (ErrorManager* error_manager);

    /**
     * @brief Destroy the Kinetic Monte Carlo object.
     * 
     */
    ~KineticMonteCarlo () = default;

    KineticMonteCarlo& operator= (const KineticMonteCarlo& other);

    /**
     * @brief Select one process from the input vector.
     * 
     * @param allowed_processes Vector of RateStateProcess objects.
     * @return RateStateProcess 
     */
    RateStateProcess EvaluateProcesses (std::vector<RateStateProcess>& allowed_processes) const;

private:
    /**
     * @brief Compute the process time cost for the chosen state.
     * 
     * @param chosen_state Weak pointer to the chosen state.
     * @param allowed_processes Vector of RateStateProcess objects.
     * @return double 
     */
    double GetProcessTime (const std::weak_ptr<State>& chosen_state, const std::vector<RateStateProcess>& allowed_processes) const;

    /**
     * @brief Randomly select one process rate evaluating the cummulative sum of rates.
     * 
     * @param rate_list Armadillo vector of process rates.
     * @return int 
     */
    int SelectProcess (const arma::dvec& rate_list) const;
};