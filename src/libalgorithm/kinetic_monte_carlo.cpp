#include "kinetic_monte_carlo.h"

#include "../global_objects.h"
#include "../libmanager/error_manager.h"

#include <memory>
#include <omp.h>


KineticMonteCarlo::KineticMonteCarlo (ErrorManager* error_manager)
: error_manager_(error_manager)
{}

KineticMonteCarlo& KineticMonteCarlo::operator= (const KineticMonteCarlo& other)
{
    if (this == &other) { return *this; }

    return *this;
}

RateStateProcess KineticMonteCarlo::EvaluateProcesses (std::vector<RateStateProcess>& allowed_processes) const
{
    int size = allowed_processes.size();
    arma::dvec rate_list = arma::dvec(size, arma::fill::zeros);
    for (size_t i = 0; i < size; i++) { rate_list(i) = allowed_processes[i].GetRate(); }

    int selected_process = this->SelectProcess(rate_list);
    RateStateProcess chosen = allowed_processes[selected_process];
    
    if (chosen.GetTime() < 0)
    {
        double proc_time = this->GetProcessTime(chosen.GetState(), allowed_processes);
        chosen.SetTime(proc_time);
    }

    auto it = std::find(allowed_processes.begin(), allowed_processes.end(), chosen);
    allowed_processes.erase(it);

    return chosen;
}

double KineticMonteCarlo::GetProcessTime (const std::weak_ptr<State>& chosen_state, const std::vector<RateStateProcess>& allowed_processes) const
{
    std::vector<double> chosen_state_rates;
    std::for_each(allowed_processes.begin(), allowed_processes.end(),
        [&] (const RateStateProcess& rsp)
        {
            if (chosen_state.lock() == rsp.GetState().lock()) { chosen_state_rates.push_back(rsp.GetRate()); }
        });

    arma::dvec state_rates = arma::conv_to<arma::dvec>::from(chosen_state_rates);
    std::uniform_real_distribution<double> r_val(0, 1);
    double r = 1.0 - r_val(GlobalObjects::randomizer);

    return -std::log(r)/arma::sum(state_rates);
}

int KineticMonteCarlo::SelectProcess (const arma::dvec& rate_list) const
{
    std::uniform_real_distribution<double> r_val(0, 1);
    double r = arma::sum(rate_list) * r_val(GlobalObjects::randomizer);
    arma::dvec cum_sum = arma::cumsum(rate_list);

    for (size_t i = 0; i < cum_sum.n_elem; i++)
    {
        if (r < cum_sum(i)) { return i; }
    }

    std::string error = "No process was selected in the Kinetic Monte Carlo step";
    error_manager_->SAlgorithmError(error);

    return 0;
}