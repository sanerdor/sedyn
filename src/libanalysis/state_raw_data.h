/**
 * @file state_raw_data.h
 * @author Aitor Diaz-Andres (ad.andres@outlook.com)
 * @brief StateRawData declaration file.
 * @date 2024-05-20
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#pragma once

#include <armadillo>


/**
 * @brief StateRawData class is mostly a container of information.
 * 
 * In this object, the Analysis algorithm stored the current state, position, age...
 * of a state that has performed a process. This information is then evaluated to
 * retrieve the desired properties such as diffusion coefficients and lifetimes.
 * 
 */
class StateRawData
{
private:
    arma::dmat center_of_mass_record_;                                         // 192
    arma::dvec current_periodic_transformation_;                               // 192
    arma::dvec delta_time_record_;                                             // 192
    double diffusion_coefficient_;                                             // 8
    double lifetime_;                                                          // 8
    bool is_analysed_;                                                         // 4
    int max_step_;                                                             // 4
    int step_;                                                                 // 4
    
public:
    /**
     * @brief Construct a new StateRawData object.
     * 
     */
    StateRawData () = default;

    /**
     * @brief Construct a new StateRawData object.
     * 
     * @param max_step Maximum number of steps in the trajectory.
     */
    StateRawData (int max_step);

    /**
     * @brief Copy constructor
     * 
     * @param other StateRawData constant reference
     */
    StateRawData (const StateRawData& other);

    /**
     * @brief Destroy the State Raw Data object
     * 
     */
    ~StateRawData () = default;

    /**
     * @brief Stores the current information of a state.
     * 
     * The information provided as input is the position in transformed space, the transformation
     * in Periodic Boundary Condition and the delta time for the process computed by the KineticMonteCarlo
     * algorithm.
     * 
     * @param center_of_mass Position of the center of mass of the molecule.
     * @param transformation Transformation in Periodic Boundary Conditions.
     * @param delta_time Delta time for the process.
     */
    void AddRecord (arma::dvec center_of_mass, arma::dvec transformation, double delta_time);

    /**
     * @brief Computes, stores and returns the diffusion coefficient.
     * 
     * @param subtraj_length Length of subtrajectories.
     * @return double 
     */
    double AnalyseDiffusionCoefficient (int subtraj_length);

    /**
     * @brief Computes, stores and returns the lifetime.
     * 
     * @return double 
     */
    double AnalyseLifetime ();

    /**
     * @brief Get the Center Of Mass Record variable
     * 
     * @return arma::dmat 
     */
    arma::dmat GetCenterOfMassRecord () const { return center_of_mass_record_; }

    /**
     * @brief Get the Delta Time Record variable
     * 
     * @return arma::dvec 
     */
    arma::dvec GetDeltaTimeRecord () const { return delta_time_record_; }

    /**
     * @brief Get the diffusion coefficient.
     * 
     * @return double 
     */
    double GetDiffusionCoefficient () const { return diffusion_coefficient_; }

    /**
     * @brief Get if the state is already analysed.
     * 
     * @return true 
     * @return false
     */
    bool GetIsAnalysed () const { return is_analysed_; }

    /**
     * @brief Get the lifetime.
     * 
     * @return double 
     */
    double GetLifetime () const { return lifetime_; }
};