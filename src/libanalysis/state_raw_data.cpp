#include "state_raw_data.h"

#include <algorithm>


StateRawData::StateRawData (int max_step)
: center_of_mass_record_(arma::dmat(3, max_step, arma::fill::zeros)), current_periodic_transformation_(arma::dvec(3, arma::fill::zeros)),
    delta_time_record_(arma::dvec(max_step, arma::fill::zeros)), diffusion_coefficient_(0.0), lifetime_(0.0), 
    is_analysed_(false), max_step_(max_step), step_(0)
{}

StateRawData::StateRawData (const StateRawData& other)
: center_of_mass_record_(other.center_of_mass_record_), current_periodic_transformation_(other.current_periodic_transformation_),
    delta_time_record_(other.delta_time_record_), diffusion_coefficient_(other.diffusion_coefficient_), lifetime_(other.lifetime_),
    is_analysed_(other.is_analysed_), max_step_(other.max_step_), step_(other.step_)
{}

void StateRawData::AddRecord (arma::dvec center_of_mass, arma::dvec transformation, double delta_time)
{
    if (!transformation.is_zero()) { current_periodic_transformation_ += transformation; }
    center_of_mass_record_.col(step_) = center_of_mass - current_periodic_transformation_;
    delta_time_record_(step_) = delta_time;
    step_++;
}

double StateRawData::AnalyseDiffusionCoefficient (int subtraj_length)
{
    is_analysed_ = true;

    arma::uvec last_nonzero = arma::find(delta_time_record_ != 0.0, 1, "last");
    if (last_nonzero.n_elem == 0) { return 0.0; }

    if (last_nonzero(0) < subtraj_length-1) { return 0.0; }
    int n_subtraj = last_nonzero(0) / subtraj_length + 1;

    arma::dmat com_record(subtraj_length, n_subtraj, arma::fill::zeros);
    arma::dmat time_record(delta_time_record_.memptr(), subtraj_length, n_subtraj);
    for (size_t i = 0; i < n_subtraj; i++)
    {
        const arma::dvec& com_reference = center_of_mass_record_.col(i*subtraj_length);
        for (size_t j = 0; j < subtraj_length; j++)
        {
            const arma::dvec& com_target = center_of_mass_record_.col(i*subtraj_length+j);
            com_record(j,i) = std::pow(arma::norm(com_target - com_reference), 2);

            time_record(j,i) = time_record(j+i*subtraj_length) - time_record(i*subtraj_length);
        }
    }
    com_record = com_record.t();
    time_record = time_record.t();

    arma::dvec com_mean(subtraj_length, arma::fill::zeros);
    arma::dvec time_mean(subtraj_length, arma::fill::zeros);
    if (n_subtraj == 1)
    {
        com_mean = com_record.as_col();
        time_mean = time_record.as_col();
    } else
    {
        for (size_t i = 0; i < subtraj_length; i++)
        {
            com_mean(i) = arma::mean(com_record.col(i));
            time_mean(i) = arma::mean(time_record.col(i));
        }
    }
    time_mean = arma::cumsum(time_mean);

    arma::dvec slope = arma::solve(time_mean, com_mean);
    diffusion_coefficient_ = slope(0)/6;

    return diffusion_coefficient_;
}

double StateRawData::AnalyseLifetime ()
{
    is_analysed_ = true;

    lifetime_ = arma::sum(delta_time_record_);
    
    return lifetime_;
}