#include "analysis.h"

#include "../libmanager/error_manager.h"
#include "../libmanager/file_manager.h"
#include "../libsystem/system.h"

#include <algorithm>
#include <iomanip>
#include <map>
#include <sstream>


Analysis::Analysis (std::map<AnalysisTypes, std::vector<std::string>> state_analysis, ErrorManager* error_manager)
: state_analysis_(state_analysis), error_manager_(error_manager)
{
    for (auto& it : state_analysis_)
    {
        for (std::string state_label : it.second)
        {
            if (!this->IsStringInVector(state_list_, state_label))
            {
                state_list_.push_back(state_label);
            }
        }
    }
    
    for (std::string state_label : state_list_) { state_count_.insert({state_label, 0}); }
}

Analysis::Analysis (const Analysis& other)
: state_analysis_(other.state_analysis_), state_list_(other.state_list_), state_count_(other.state_count_),
    error_manager_(other.error_manager_)
{}

Analysis& Analysis::operator= (const Analysis& other)
{
    if (this == &other) { return *this; }

    error_manager_ = other.error_manager_;
    state_analysis_ = other.state_analysis_;
    state_list_ = other.state_list_;
    state_count_ = other.state_count_;
    
    for (auto& it : other.state_record_)
    {
        std::unique_ptr<StateRawData> st_raw_data = std::make_unique<StateRawData>(*it.second);
        state_record_.insert({it.first, std::move(st_raw_data)});
    }

    return *this;
}

std::string Analysis::AnalyseSimulation () const
{
    std::string out_str = "";

    for (std::string state_label : state_list_)
    {
        out_str += "Results ... " + state_label + "\n";
        out_str += std::string(12+state_label.size(), '-') + "\n";

        //* Diffusion Coefficient
        if (this->IsTypeAndStateInAnalysis(AnalysisTypes::DiffusionCoefficient, state_label))
        {
            double diffusion_coefficient = 0.0;
            int n_state = 0;
            for (auto& it : state_record_)
            {
                if (it.first->GetLabel() == state_label)
                {
                    diffusion_coefficient += it.second->GetDiffusionCoefficient();
                    n_state++;
                }
            }

            std::stringstream diffusion_coefficient_str;
            if (n_state != 0) { diffusion_coefficient_str << std::setprecision(6) << (diffusion_coefficient/n_state*1e-7); }
            out_str += "Diffusion Coefficient ... " + diffusion_coefficient_str.str() + " cm^2/s\n";
        }

        //* Lifetime
        if (this->IsTypeAndStateInAnalysis(AnalysisTypes::Lifetime, state_label))
        {
            double lifetime = 0.0;
            int n_state = 0;
            for (auto& it : state_record_)
            {
                if (it.first->GetLabel() == state_label)
                {
                    lifetime += it.second->GetLifetime();
                    n_state++;
                }
            }

            std::stringstream lifetime_str;
            if (n_state != 0) { lifetime_str << std::setprecision(6) << (lifetime/n_state); }
            out_str += "Lifetime ... " + lifetime_str.str() + " ns\n";
        }

        

        out_str += "\n";
    }

    return out_str;
}

std::string Analysis::AnalyseTrajectory (const System& system)
{
    std::string out_str = "";

    for (std::string state_label : state_list_)
    {
        out_str += "Results ... " + state_label + "\n";
        out_str += std::string(12+state_label.size(), '-') + "\n";

        int end_state = system.GetCurrentStateListCount(state_label);
        out_str += "Number at simulation end ... " + std::to_string(end_state) + "\n";

        //* Total Count
        if (this->IsTypeAndStateInAnalysis(AnalysisTypes::TotalCount, state_label))
        {
            int state_count = std::count_if(state_record_.begin(), state_record_.end(),
            [&] (const auto& it)
            {
                return (it.first->GetLabel() == state_label && !it.second->GetIsAnalysed());
            });

            out_str += "Number of registered ... " + std::to_string(state_count) + "\n";
        }

        //* Diffusion Coefficient
        if (this->IsTypeAndStateInAnalysis(AnalysisTypes::DiffusionCoefficient, state_label))
        {
            double diffusion_coefficient = 0.0;
            int subtraj_length = anal_param_int_.at(AnalysisParameters::SubTrajectoryLength);
            int n_state = 0;
            for (auto& it : state_record_)
            {
                if (!it.second->GetIsAnalysed() && it.first->GetLabel() == state_label)
                {
                    diffusion_coefficient += it.second->AnalyseDiffusionCoefficient(subtraj_length);
                    n_state++;
                }
            }

            std::stringstream diffusion_coefficient_str;
            if (n_state != 0) { diffusion_coefficient_str << std::setprecision(6) << (diffusion_coefficient/n_state*1e-7); }
            out_str += "Diffusion Coefficient ... " + diffusion_coefficient_str.str() + " cm^2/s\n";
        }

        //* Lifetime
        if (this->IsTypeAndStateInAnalysis(AnalysisTypes::Lifetime, state_label))
        {
            double lifetime = 0.0;
            int n_state = 0;
            for (auto& it : state_record_)
            {
                if (!it.second->GetIsAnalysed() && it.first->GetLabel() == state_label)
                {
                    lifetime += it.second->AnalyseLifetime();
                    n_state++;
                }
            }

            std::stringstream lifetime_str;
            if (n_state != 0) { lifetime_str << std::setprecision(6) << (lifetime/n_state); }
            out_str += "Lifetime ... " + lifetime_str.str() + " ns\n";
        }

        //* Save Trajectory
        if (this->IsTypeAndStateInAnalysis(AnalysisTypes::SaveTrajectory, state_label))
        {
            out_str += "\n";
            for (auto& it : state_record_)
            {
                if (it.first->GetLabel() == state_label)
                {
                    int curr_state_num = state_count_.at(state_label);
                    std::string path = std::getenv("SDNWORKPATH") + std::string("/traj_") + state_label + std::string("_");
                    path += std::to_string(curr_state_num) + ".txt";

                    const arma::dvec& time_record = it.second->GetDeltaTimeRecord();
                    const arma::dmat& com_record = it.second->GetCenterOfMassRecord();

                    arma::uvec last_nonzero = arma::find(time_record != 0.0, 1, "last");
                    if (last_nonzero.n_elem == 0) { continue; }
                    size_t size = last_nonzero(0)+1;

                    arma::dmat simulation(4, size, arma::fill::zeros);
                    simulation.row(0) = arma::cumsum(time_record.rows(0, size-1)).t();
                    simulation.rows(1, 3) = com_record.cols(0, size-1);

                    //* Conver it to row mayor
                    simulation = simulation.t();
                    simulation.save(path, arma::arma_ascii);
                    state_count_.at(state_label) = curr_state_num+1;

                    out_str += "Trajectory of state " + state_label + " " + std::to_string(curr_state_num) + " saved.\n";
                }
            }
        }

        out_str += "\n";
    }

    return out_str;
}

void Analysis::CheckStatesToAnalyse (std::vector<std::string> state_labels_list) const
{
    for (const std::string st_to_anal : state_list_)
    {
        std::vector<std::string>::iterator it = std::find(state_labels_list.begin(), state_labels_list.end(), st_to_anal);
        if (it == state_labels_list.end())
        {
            std::string error = st_to_anal + " error was declared to analyse but it was not found in the Input file";
            error_manager_->SAnalysisError(error);
        }
    }
}

void Analysis::RecordStateData (const RateStateProcess& rate_state_process, const int n_step)
{
    std::shared_ptr<State> state = rate_state_process.GetState().lock();
    
    //* State dependent analysis
    if (state_record_.find(state) == state_record_.end())
    {
        state_record_[state] = std::make_unique<StateRawData>(n_step); 
    }

    //* Molecule dependent analysis
    std::shared_ptr<Molecule> molecule = state->GetMolecules()[0].lock();
    state_record_[state]->AddRecord(molecule->GetCenterOfMass(), rate_state_process.GetPeriodicTranformation(),
                                    rate_state_process.GetTime());
    
    if (state.use_count() == 1)
    {
        // When state is no more used, do something with it
    }
}

bool Analysis::IsStringInVector (const std::vector<std::string>& vector, const std::string string) const
{
    return std::find(vector.begin(), vector.end(), string) != vector.end();
}

bool Analysis::IsTypeAndStateInAnalysis (const AnalysisTypes type, const std::string state) const
{
    if (state_analysis_.find(type) == state_analysis_.end()) { return false; }
    else
    {
        return this->IsStringInVector(state_analysis_.at(type), state);
    }
}