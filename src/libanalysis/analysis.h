/**
 * @file analysis.h
 * @author Aitor Diaz-Andres (ad.andres@outlook.com)
 * @brief Analysis class declaration file.
 * @date 2024-05-20
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#pragma once

#include "state_raw_data.h"
#include "../libstate/rate_state_process.h"
#include "../libmanager/error_manager.h"

#include <map>
#include <memory>
#include <string>
#include <vector>

#define ANALYSIS_MAP(type)\
private:\
    std::map<Analysis::AnalysisParameters, type> anal_param_##type##_;\
public:\
    void set_anal_param_##type (std::map<Analysis::AnalysisParameters, type> parameters) { anal_param_##type##_ = parameters; }


/**
 * @brief Class to perform analysis of trajectories and simulations.
 * 
 * This class contains the methods to analyse any (defined) property of trajectories
 * and simulations. It requires that the target states to analyse evolve in time.
 * This means that those target states that, for example to compute the diffusion coefficient,
 * do not move will throw an error.
 */
class Analysis
{
public:
    enum class AnalysisTypes
    {
        DiffusionCoefficient,
        Lifetime,
        SaveTrajectory,
        TotalCount
    };

    enum class AnalysisParameters
    {
        SubTrajectoryLength
    };

private:
    std::map<std::shared_ptr<State>, std::unique_ptr<StateRawData>> state_record_; // 48
    std::map<AnalysisTypes, std::vector<std::string>> state_analysis_;          // 48

    ANALYSIS_MAP(int);

    std::map<std::string, int> state_count_;                                    // 48
    std::vector<std::string> state_list_;                                       // 24
    ErrorManager* error_manager_;                                               // 8

public:
    /**
     * @brief Construct a new Analysis object.
     * 
     */
    Analysis () = default;

    /**
     * @brief Construct a new Analysis object.
     * 
     * @param state_analysis All the requested analysis keys paired with the states to be analysed.
     * @param error_manager ErrorManager object reference.
     */
    Analysis (std::map<AnalysisTypes, std::vector<std::string>> state_analysis, ErrorManager* error_manager);

    /**
     * @brief Copy constructor
     * 
     * @param other Analysis constant reference
     */
    Analysis (const Analysis& other);

    /**
     * @brief Destroy the Analysis object
     * 
     */
    ~Analysis () = default;

    Analysis& operator= (const Analysis& other);

    /**
     * @brief Performs the analysis of the simulation once all the trayectories have been analysed. 
     * 
     * The analysis of the simulation relies that the trajectories have been analysed previously and
     * for each state one StateRawData is stored. The algorithm will calculate the mean value for the target property
     * and the result will be returned as a string value.
     * 
     * @return std::string 
     */
    std::string AnalyseSimulation () const;

    /**
     * @brief Performs the analysis of the trajectory.
     * 
     * The analysis of the trajectory is performed for each state and StateRawData recorded douring the trajectory.
     * For each target state to analyse, the mean value of the property is computed.
     * 
     * @param system System object reference.
     * @return std::string 
     * 
     * @warning This code is very similar (almost equal) to Analysis::AnalyseSimulation(). We are under development to
     * remove one of them.
     */
    std::string AnalyseTrajectory (const System& system);

    /**
     * @brief Checks the Input state list labels and compares with the states labels to be analysed.
     * 
     * @param state_labels_list Vector of state label strings.
     */
    void CheckStatesToAnalyse (std::vector<std::string> state_labels_list) const;

    /**
     * @brief Get the states to analyse.
     * 
     * @return std::vector<std::string> 
     */
    std::vector<std::string> GetStatesToAnalyse () const { return state_list_; }

    /**
     * @brief Saved the current information of a state and the process it has suffered.
     * 
     * The information of the state is stored in a StateRawData object. If there is no previour record for 
     * a given state, a new StateRawData object if allocated.
     * 
     * @param rate_state_process RateStateProcess object constant reference.
     * @param n_step Max step for simulation.
     */
    void RecordStateData (const RateStateProcess& rate_state_process, const int n_step);

private:
    /**
     * @brief Return true if string is in vector
     * 
     * @param vector 
     * @param string 
     * @return true 
     * @return false 
     */
    inline bool IsStringInVector (const std::vector<std::string>& vector, const std::string string) const;

    inline bool IsTypeAndStateInAnalysis (const AnalysisTypes type, const std::string state) const;
};