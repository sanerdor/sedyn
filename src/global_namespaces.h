#pragma once


//* Universal Constant Parameters
namespace ncon
{
    /* pi -- Pi */
    inline static constexpr double pi = 3.1415926535897932385;

    /* hbar -- Planck's constant divided by 2pi (eV * ns) */
    inline static constexpr double hbar = 6.58210893126952e-7;

    /* angtoel -- Atomic to angs el transformation (angs * e) */
    inline static constexpr double angtoel = 0.52917721;

    /* vacPerm -- Vaccum permitivity (e^2 / (eV * Angs)) */
    inline static constexpr double vacPerm = 0.005524906526621038;

    /* kb -- Boltzmann's constant (eV / K) */
    inline static constexpr double kb = 8.617333262e-5;

    /* htoev -- Hartree to electronVoltion transformation (eV) */
    inline static constexpr double htoev = 27.2114;
};