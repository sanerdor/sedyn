#pragma once

#include "../libstate/state.h"

#include <armadillo>
#include <memory>
#include <vector>


class State;

class Molecule
{
    static constexpr double vdw_radius = 1.0;
public:
    //* Constructor
    Molecule () = default;
    Molecule (arma::dmat&, std::vector<std::string>&);
    Molecule (const Molecule&);
    Molecule (const std::vector<Molecule>&);

    //* Destructor
    ~Molecule () = default;

    //* Operator overloading
    bool operator== (const Molecule&) const;
    bool operator!= (const Molecule&) const;

    //* Getters
    arma::dvec GetCenterOfMass () const;
    arma::dmat GetCoordinates () const;
    std::vector<std::string> GetElements () const;
    const std::vector<std::weak_ptr<Molecule>>& GetNeighbours () const;
    int GetSize () const;
    const std::weak_ptr<State>& GetState () const;

    //* Setters
    void SetState (const std::shared_ptr<State>&);
    
    //* Public Methods
    void AddNeighbour (const std::shared_ptr<Molecule>&);
    arma::dmat GetCoulombMatrix () const;
    void Transform (const arma::dvec);

private:
    //* Variables
    arma::dvec center_of_mass_;                                                // 192
    arma::dmat coordinates_;                                                   // 192
    std::vector<std::string> elements_;                                        // 24
    std::vector<std::weak_ptr<Molecule>> neighbours_;                          // 24
    std::weak_ptr<State> state_;                                               // 16

    //* Private Methods
    void ComputeCenterOfMass ();
};