#include "molecule.h"

#include "../global_functions.h"
#include "../global_objects.h"

#include <memory>


//* Constructor

Molecule::Molecule (arma::dmat& coordinates, std::vector<std::string>& elements)
: center_of_mass_(arma::dvec(3, arma::fill::zeros)), coordinates_(std::move(coordinates)), elements_(std::move(elements))
{
    this->ComputeCenterOfMass();
}

Molecule::Molecule (const Molecule& other)
: center_of_mass_(other.center_of_mass_), coordinates_(other.coordinates_), elements_(other.elements_),
    neighbours_(other.neighbours_), state_(other.state_)
{}

Molecule::Molecule (const std::vector<Molecule>& other)
: center_of_mass_(arma::dvec(3, arma::fill::zeros))
{
    coordinates_ = other[0].coordinates_;
    elements_ = other[0].elements_;

    for (size_t i = 1; i < other.size(); i++)
    {
        coordinates_ = arma::join_rows(coordinates_, other[i].coordinates_);
        elements_.insert(elements_.end(), other[i].elements_.begin(), other[i].elements_.end());
    }

    this->GetCenterOfMass();
}

//* Operator overloading

bool Molecule::operator== (const Molecule& other) const
{   
    if (arma::all(arma::vectorise(arma::abs(center_of_mass_ - other.center_of_mass_)) > 1e-8) ||
        arma::all(arma::vectorise(arma::abs(coordinates_ - other.coordinates_)) > 1e-8) ||
        elements_ != other.elements_ ||
        !VectorsIncluded<Molecule>(neighbours_, other.neighbours_) ||
        state_.lock() != other.state_.lock())
    {
        return false;
    }

    return true;
}

bool Molecule::operator!= (const Molecule& other) const
{
    return !(*this == other);
}

//* Getters

arma::dvec Molecule::GetCenterOfMass () const { return center_of_mass_; }
arma::dmat Molecule::GetCoordinates () const { return coordinates_; }
std::vector<std::string> Molecule::GetElements () const { return elements_; }
const std::vector<std::weak_ptr<Molecule>>& Molecule::GetNeighbours () const { return neighbours_; }
int Molecule::GetSize () const { return elements_.size(); }
const std::weak_ptr<State>& Molecule::GetState () const { return state_; }

//* Setters

void Molecule::SetState (const std::shared_ptr<State>& state)
{
    state_ = std::weak_ptr<State>(state);
}

//* Public Methods

void Molecule::AddNeighbour (const std::shared_ptr<Molecule>& molecule)
{
    neighbours_.push_back(molecule);
}

arma::dmat Molecule::GetCoulombMatrix () const
{
    arma::dmat coord = coordinates_;
    coord.each_col() -= this->GetCenterOfMass();

    int size = coord.n_cols;
    arma::dmat cm = arma::dmat(size, size, arma::fill::ones);

    for (size_t i = 0; i < coord.n_cols; i++)
    {
        //* Off diagonal elements
        for (size_t j = i+1; j < coord.n_cols; j++)
        {
            double m1 = GlobalObjects::AtomMasses[elements_[i]];
            double m2 = GlobalObjects::AtomMasses[elements_[j]];

            cm(i, j) = m1 * m2 / arma::norm(coord.col(j) - coord.col(i));
            cm(j, i) = cm(i, j);
        }

        //* Diagonal elements
        cm(i, i) = 0.5 * std::pow(GlobalObjects::AtomMasses[elements_[i]], 2.4);  // 0.25 = 0.5 * 0.5 to make it half
    }

    return cm;
}

void Molecule::Transform (const arma::dvec transform)
{
    coordinates_.each_col() -= transform;
}

//* Private Methods

void Molecule::ComputeCenterOfMass ()
{
    double sum_of_mass = 0;
    arma::dvec coord_mass = {0.0, 0.0, 0.0};

    for (size_t i = 0; i < coordinates_.n_cols; i++)
    {
        double mass = GlobalObjects::AtomMasses[elements_[i]];
        sum_of_mass += mass;
        coord_mass += mass * coordinates_.col(i);
    }

    center_of_mass_ = coord_mass / sum_of_mass;
}