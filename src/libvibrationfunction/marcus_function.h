#pragma once

#include <armadillo>


class MarcusFunction
{
public:
    //* Constructor
    MarcusFunction () = default;

    //* Destructor
    ~MarcusFunction () = default;

    //* Public Methods
    arma::dvec GetVibrationSpectrum (const arma::dvec&, const double, const double, const double) const;
    
private:
};