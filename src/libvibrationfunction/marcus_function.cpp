#include "marcus_function.h"

#include "../global_namespaces.h"
#include "../libsystem/system.h"

#include <math.h>

//* Public Methods

arma::dvec MarcusFunction::GetVibrationSpectrum (const arma::dvec& X, const double reorg_energy, const double delta_energy, const double temperature) const
{
    double sign = (delta_energy > 0) - (delta_energy < 0);
    double res_pre = 1.0 / std::sqrt(4.0 * ncon::pi * ncon::kb * temperature * reorg_energy);

    arma::dmat res_post = arma::exp(-arma::pow((delta_energy - sign * X + reorg_energy), 2) /
                                    (4.0 * ncon::kb * temperature * reorg_energy));
    
    return res_pre * res_post;
}