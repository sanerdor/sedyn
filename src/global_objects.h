#pragma once

#include <map>
#include <memory>
#include <random>
#include <string>


class Analysis;
class ErrorManager;
class FileManager;
class System;

class GlobalObjects
{
public:
    static std::map<std::string, double> AtomMasses;
    static std::mt19937 randomizer;
};