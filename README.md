# SEDYN

Solid State Exciton Dynamics.

## Required packages (Ubuntu)

The following packages are required for the correct compilation of SEDYN.

- CMake
- OpenMP
- OpenBLAS
- LAPACK
- Armadillo
- HDF5

```
sudo apt update
sudo apt install -y cmake libomp-dev
```

For the installation of Armadillo, OpenBLAS and LAPACK linear algebra packages are required.
There are many ways to install these packages. Here is a simple example.

```
sudo apt install -y libopenblas-dev liblapack-dev
```

The following packages are optional but recommended by Armadillo.

```
sudo apt install -y libarpack2-dev libsuperlu-dev
```

The installation of Armadillo must be made by visitting their web side and downloading their package: 
https://arma.sourceforge.net/download.html