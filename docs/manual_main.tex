%==============================================================================
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%==============================================================================

\documentclass[10pt]{report}

\usepackage{chemformula}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{blindtext}
\usepackage{microtype}
\usepackage[english]{babel}
\usepackage{enumitem}
    \setlist[itemize]{noitemsep}
    \setlist[description]{font=\normalfont,labelindent=2em,leftmargin=!}

\makeatletter
\renewcommand*\env@matrix[1][\arraystretch]{
  \edef\arraystretch{1.5}
  \hskip -\arraycolsep
  \let\@ifnextchar\new@ifnextchar
  \array{*\c@MaxMatrixCols c}}
\makeatother

\usepackage{nccmath}                                                            
\usepackage{upgreek}                                                            
\usepackage[a4paper,left=2.5cm, right=2cm,top=2.5cm,bottom=2.5cm]{geometry}
\usepackage[hypcap=false]{caption}                                              
\usepackage{subcaption}                                                         
\usepackage{wrapfig}                                                            
\usepackage{dashrule}                                                           
\usepackage{bigstrut}                                                           
\usepackage{multirow}                                                           
\usepackage{graphicx}    
    \graphicspath{{figures/}}
\usepackage{amsmath}
\usepackage{xparse}
\usepackage{physics}
\usepackage{multicol}                                                           
\usepackage{hyperref}                                                           
\usepackage{indentfirst}
\usepackage{natbib}
\usepackage{booktabs}
\usepackage{setspace}
\usepackage{chemfig}
\usepackage{tocloft}

\usepackage{listings}
\lstset{
    basicstyle=\ttfamily\small,
    aboveskip={1.0\baselineskip},
    belowskip={1.0\baselineskip},
    columns=fixed,
    extendedchars=true,
    breaklines=true,
    tabsize=4,
    prebreak=\raisebox{0ex}[0ex][0ex]{\ensuremath{\hookleftarrow}},
    frame=lines,
    showtabs=false,
    showspaces=false,
    showstringspaces=false,
    keywordstyle=\color[rgb]{0.627,0.126,0.941},
    commentstyle=\color[rgb]{0.133,0.545,0.133},
    stringstyle=\color[rgb]{1,0,0},
    numbers=left,
    numberstyle=\small,
    stepnumber=1,
    numbersep=10pt,
    captionpos=t,
    escapeinside={\%*}{*)}
}

\newcommand{\mbf}[1]{\mathbf{#1}}
\newcommand{\cf}[1]{\chemfig[atom sep=2em]{#1}}
\newcommand{\tline}{\noindent\rule{\textwidth}{0.4pt}}
\newcommand{\expar}[1]{\bigskip - - - - - #1 - - - - -}
\newcommand{\neweqn}{\end{equation*}\begin{equation*}}

\newenvironment{eqnleft}{\begin{fleqn}\begin{equation*}}{\end{equation*}\end{fleqn}}

\renewcommand{\labelenumi}{\alph{enumi}.}
\setlist{nosep}

\setlength{\parskip}{1ex}
\setlength{\parindent}{4ex}
\setlength{\headheight}{15pt}
\setlength{\marginparwidth}{2cm}


\begin{document}

%------------------------------------------------------------------------------
%   TITLE SECTION
%------------------------------------------------------------------------------

\begin{titlepage}
	\centering
	{\scshape\LARGE Donostia International Physics Center\par}
	\vspace{1cm}
	{\scshape\Large User's Manual\par}
	\vspace{6.5cm}
	{\huge\bfseries SEDYN\par}	
	{\scshape\Large Solid State Exciton Dynamics\par}
	\vspace{1.5cm}
	{\Large\itshape Aitor Diaz Andres\par}
	\vfill
	{\large \today\par}
\end{titlepage}

%==============================================================================
%   TABLE OF CONTENTS
%==============================================================================

\tableofcontents

%==============================================================================
%	Sedyn Keywords
%==============================================================================

\chapter{Sedyn Keywords}%
\label{cha:sedyn_keywords}

%------------------------------------------------------------------------------
%	\$run
%------------------------------------------------------------------------------

\section{\$run}%
\label{sec:run}

The \$run section is compulsory for any calculation in sedyn with all the keywords specified below.
This section rules the calculation and the most basic features, hence, the basic run specifications are required in order to compute an exciton simulation.
If no \$run section is found, the software will exit with an error.

\begin{lstlisting}
$run
max_step		N
trajectories	N
cutoff			x,y,z
dimension		x,y,z
spawn_density	N
$$run
\end{lstlisting}

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{MAX\_STEP}
        \begin{description}
            \item[Specifies the maximun number of steps to simulate.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[INTEGER]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[$n$] $n>0$
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[It is recomended to be at least 10000 steps for each initial exciton.]
        \end{description}
\end{itemize}

\bigskip

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{TRAJECTORIES}
        \begin{description}
            \item[Specifies the number of simulations to perform.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[INTEGER]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[$n$] $n>0$
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[NONE]
        \end{description}
\end{itemize}

\newpage

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{CUTOFF}
        \begin{description}
            \item[Sets the elipsoid cutoff radius to compute the nearest neighbours of each molecule.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[REAL]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[$x$] $x>0.0$
            \item[$y$] $y>0.0$
            \item[$z$] $z>0.0$
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[It is recomended to analyse the solid structure first and use numbers to reduce to a minimun]
            \item[the number of "relevant" neighbours, thus, reduce the computational effort. In order to obtain]
            \item[a spherical cutoff three parameters ($x$, $y$ and $z$) must be set to the same number.]
        \end{description}
\end{itemize}

\bigskip

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{DIMENSION}
        \begin{description}
            \item[Sets the size of the system, i.e., the replicas of the structure in the structure file.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[INTEGER]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[$x$] $x>0$
            \item[$y$] $y>0$
            \item[$z$] $z>0$
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[For crystal systems inputed as ".cif" file it is recomended to set large parameters (>50) so as]
            \item[to generate a large enough gigacell.]
        \end{description}
\end{itemize}

\bigskip

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{SPAWN\_DENSITY}
        \begin{description}
            \item[Sets the initial concentration of excitons in the center of the gigacell.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[REAL]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[$n$] $n>0$
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[NONE]
        \end{description}
\end{itemize}



%------------------------------------------------------------------------------
%	\$readfiles
%------------------------------------------------------------------------------

\section{\$readfiles}%
\label{sec:readfiles}

The \$readfiles section is compulsory for any calculation in sedyn.
This section saves the names of the files to be read with all the structure and electronic data.
Additionally, a keyword (read\_sdn) is provided to read a .sdn file with all the structural data from previous calculations.
This keywords is ment to save time as it avoid the recalculation of all the molecular neightbours.
If no \$readfiles section is found, the software will exit with an error.

\begin{lstlisting}
$readfiles
read_sdn		bool
system_file		name
mol_data_file	name
pair_data_file	name
$$readfiles
\end{lstlisting}

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{READ\_SDN}
        \begin{description}
            \item[Allows the read of "system\_mln.sdn" file.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[BOOL]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[FALSE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[true] Read "system\_mln.sdn" file.
            \item[false] Do no read and create new "system\_mln.sdn" file.
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[For consecutive simulations of different excitons of properties with the same system is highly]
            \item[recomended to create one "system\_mln.sdn" file and set to true this parameter.]
        \end{description}
\end{itemize}

\bigskip

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{SYSTEM\_FILE}
        \begin{description}
            \item[Specifies the name of the file with structural data.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[STRING]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[name] STRING
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[Only ".cif" and ".pbd" structure extensions are supported.]
        \end{description}
\end{itemize}

\bigskip

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{MOL\_DATA\_FILE}
        \begin{description}
            \item[Specifies the name of the file with molecular electronic structure data.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[STRING]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[name] STRING
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[NONE]
        \end{description}
\end{itemize}

\bigskip

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{PAIR\_DATA\_FILE}
        \begin{description}
            \item[Specifies the name of the file with molecular pairs electronic structure data.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[STRING]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[name] STRING
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[NONE]
        \end{description}
\end{itemize}

\newpage

%------------------------------------------------------------------------------
%	\$algorithm label
%------------------------------------------------------------------------------

\section{\$algorithm label}%
\label{sec:algorithm}

The \$algorithm section is compulsory for any calculation in sedyn.
This section specifies the name of the algorithm to use through the simulation.
If no \$readfiles section is found, the software will exit with an error.

The "label" parameter specifies the type of algorithm to be used.
Currently the software allow the use of Kinetic Monte Carlo algorithm ("kineticmontecarlo" or "kinmc") and the Dynamic Monte Carlo algorithm ("dynamicmontecarlo" or "dynmc").

\begin{lstlisting}
$algorithm label
$$algorithm
\end{lstlisting}

% Explain in subsections the different algorithms with specific recomendations for different systems.

%------------------------------------------------------------------------------
%	State declaration (\$state label)
%------------------------------------------------------------------------------

\section{State declaration (\$state label)}%
\label{sec:state}

The state section allows the user to introduce unique states to the system, hence, it requires that each state is asociated to an unique "label".
These states may be included from the beginning of the simulation or declared as product of proicesses.
At least one state section is required in order to begin the simulation.

\begin{lstlisting}
$state label
initial_num		n
multiplicity	n
size			n
.
.
.
$$state
\end{lstlisting}

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{INITIAL\_NUM}
        \begin{description}
            \item[Specifies the number of replicas of this state at the begining of the simulation.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[INTEGER]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[$n$] $n\ge0$
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[NONE]
        \end{description}
\end{itemize}

\bigskip

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{MULTIPLICITY}
        \begin{description}
            \item[Specifies the multiplicity of the state.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[INTEGER]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[$n$] $n>0$
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[NONE]
        \end{description}
\end{itemize}

\newpage

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{SIZE}
        \begin{description}
            \item[Specifies the size of the state, this is, how many molecules are required for this state.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[INTEGER]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[$n$] $n>0$
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[Only those states of size = 1 can have "initial\_num $\ge$ 0".]
        \end{description}
\end{itemize}

\bigskip

%------------------------------------------------------------------------------
%	Process declaration (\$process plabel)
%------------------------------------------------------------------------------

\subsection{Process declaration (\$process plabel)}
\label{sub:process}

Inside \$state section, there should be declared all the processes that state can perform in any situation and all the outcomes that are possible.
There are some processes that are compulsory by default.
However, processes such us trasport, decay and recombination must be declared inside this subsection.

\begin{lstlisting}
$state label
.
.
.
$process plabel
int_state label(1),...,label(N)
target_states label(1),...,label(N)
proc_time n
hamiltonian_coeff n
$$process
.
.
.
$$state
\end{lstlisting}

The "plabel" parameter specifies the type of process declared.
There are many common keywords between processes and in this manual all processes are going to be explained with all their keywords.

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{INT\_STATE (optional)}
        \begin{description}
            \item[Specifies the labels of other states that interact with this state.]
            \item[If more than one state is declared, all states must interact at the same time.]
            \item[If not interaction states are declared, the process may be considered as decay to target state.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[STRING]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[label(n)] STRING
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[]
        \end{description}
\end{itemize}

\newpage

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{TARGET\_STATES}
        \begin{description}
            \item[Specifies the labels of the states product of this process.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[STRING]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[label(n)] STRING
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[]
        \end{description}
\end{itemize}

\bigskip

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{PROC\_TIME (optional)}
        \begin{description}
            \item[Sets a custom process time no matter the process rate resultant from the calculation of this process.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[FLOAT]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[-1]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[$n$] $n\ge0$
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[If no process time is declared, the system will compute this value with the method of the algorithm.]
        \end{description}
\end{itemize}

\bigskip

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{HAMILTONIAN\_COEFF (optional)}
        \begin{description}
            \item[Sets the coefficient associated to the hamiltonian to compute a hamiltonian modified process rate.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[INTEGER]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[-1]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[$n$] $0 < n \le 9$
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[If no hamiltonian coefficient is declared, it will not be possible to implement the hamiltonian]
            \item[for this state and process.]
        \end{description}
\end{itemize}

\newpage

%------------------------------------------------------------------------------
%	\$process fermigoldenrule
%------------------------------------------------------------------------------

\subsubsection{\$process fermigoldenrule}
\label{ssub:fermigoldenrule}

The Fermi's Golden Rule equation requires two parameters that must be specified by the user.
This parameters are the electronic couplings between states $i$ and $f$ and the vibrational model that will be used to compute the $\rho(E_{f})$ term.

\begin{equation}
\Gamma_{if} = \dfrac{2\pi}{\hbar} \abs{\mel{f}{\hat{H}}{i}}^{2} \rho(E_{f})
\end{equation}

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{ELEC\_COUP\_FUNC}
        \begin{description}
            \item[Specifies the function to compute the electronic coupling.]
            \item[It is possible to use an external data base (see \$readfiles section) to obtain raw values.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[STRING]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[function]
            \item[external] coupling	(see \$readfiles section)
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[For non cristaline systems and mixed systems such as interphases it is usually recomended]
            \item[to compute these couplings with an external quantum chemical software and import them with]
            \item[the "external coupling" key.]
        \end{description}
\end{itemize}

\bigskip

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{VIBRATIONS}
        \begin{description}
            \item[Specifies the function to compute the density of states.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[STRING]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[marcusmodel]
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[NONE]
        \end{description}
\end{itemize}



%------------------------------------------------------------------------------
%	\$process distancecustomrate
%------------------------------------------------------------------------------

\subsubsection{\$process distancecustomrate}
\label{ssub:distancecustomrate}

This process specifies a rate constant to evaluate any possible interaction inside the specified intermolecular distance.
There will not be taken into account intermolecular or intramolecular interactions in order to compute a rate constant, this is, the intermolecular distance will determine if the process may be performed and the custom rate will be used.

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{DISTANCE}
        \begin{description}
            \item[Specifies the maximun intermolecular distance to allow this process.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[FLOAT]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[$n$] $n > 0$
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[NONE]
        \end{description}
\end{itemize}

\newpage

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{RATE}
        \begin{description}
            \item[Specifies the rate constant to evaluate the process by the algorithm if]
            \item[intermolecular distance is less than "distance".]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[FLOAT]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[$n$] $n \ge 0$
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[NONE]
        \end{description}
\end{itemize}



%------------------------------------------------------------------------------
%	\$process lifetimedecay
%------------------------------------------------------------------------------

\subsubsection{\$process lifetimedecay}
\label{ssub:lifetimedecay}

This process evaluate the current lifetime of a state and the custom lifetime specified in this process.
The rate constant should be small when the state is "young" and increase exponentially according to its custom lifetime.
If the lifetime of the exciton is larger than the "normal" simulation time, there should not be specified this process as it only will increase the computational cost without any simulation improvement.

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{EXP\_FUNC}
        \begin{description}
            \item[Specifies the function to evaluate the exponential lifetime.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[INTEGER]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[$n = 0$] $1/\exp[-age/lifetime]$
            \item[$n = 1$] $\exp[-age/lifetime]$
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[It is recomended that for Kinetic Monte Carlo algorithm "exp\_func" is set to 0]
            \item[and for Dynamic Monte Carlo algorithm to 1.]
        \end{description}
\end{itemize}

\bigskip

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{LIFETIME}
        \begin{description}
            \item[Specifies the expected lifetime of an state.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[FLOAT]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[$n$] $n > 0$
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[For lifetimes larger than simulation time, this process should not be declared.]
        \end{description}
\end{itemize}

\newpage

%------------------------------------------------------------------------------
%	Trasition declaration (\$transition tlabel)
%------------------------------------------------------------------------------

\subsection{Transition declaration (\$transition tlabel)}
\label{sub:transition}

Inside \$state section, there should be declared all the transitions that state can perform.

\begin{lstlisting}
$state label
.
.
.
$transition tlabel
reorg_energy n
$$transition
.
.
.
$$state
\end{lstlisting}

\begin{itemize} \small
    \setlength\itemsep{0.5em}
    \item[] \textbf{REORG\_ENERGY}
        \begin{description}
            \item[Specifies the reorganization energy from this state to "tlabel" state.]
        \end{description}
    \item[] TYPE:
        \begin{description}
            \item[FLOAT]
        \end{description}
    \item[] DEFAULT:
        \begin{description}
            \item[NONE]
        \end{description}
    \item[] OPTIONS:
        \begin{description}[labelwidth=1.5cm]
            \item[$n$] $n > 0$
        \end{description}
    \item[] RECOMENDATION:
        \begin{description}
            \item[NONE]
        \end{description}
\end{itemize}

\newpage

%------------------------------------------------------------------------------
%	Examples
%------------------------------------------------------------------------------

\section{Examples}
\label{sec:examples}

Common examples of exciton transport and decay are declared.

\paragraph{Singlet exciton transport:} External data is used for the Fermi's Golden Rule process

\begin{lstlisting}
!Lines that begin with "!" symbol are ignored by the Input File Manager

$run
	max_step			10000
	trajectories		2
	cutoff				10,15,5
	dimension			5,5,5
	spawn_density		1.0
$$run

$readfiles
	read_sdn			false
	system_file			rubrene.cif
	mol_data_file		mol_data.csv
	pair_data_file		pair_data.csv
$$readfiles

$algorithm kineticmontecarlo
$$algorithm

$state s1
	initial_num			2
	multiplicity		1
	size				1

	$process fermigoldenrule
		int_state		gs
		target_states	s1
		ele_coup_func	external forster
		vibrations		marcusmodel
	$$process

	$process lifetimedecay
		target_states	gs
		lifetime		10
		exp_func		0
	$$process

	$trasition gs
		reorg_energy	0.1
	$$transition

$$state
\end{lstlisting}

\newpage

\paragraph{TTA process:} Singlet exciton transport, Triplet exciton transport, Triplet-triplet exciton interaction, TT formation and TT recombination into S1 processes are evaluated in the following input file.



\begin{lstlisting}
! It has not been completely tested if the results are correct

$run
	max_step			10000
	trajectories		1
	cutoff				20.09174,8.9625,10.65825
	dimension			8,32,16
	spawn_density		1.0
$$run

$readfiles
	read_sdn			true
	system_file			rubrene.cif
	mol_data_file		mol_data.csv
	pair_data_file		pair_data.csv
$$readfiles

$algorithm kinmc
$$algorithm

! ========
! STATE T1
! ========

$state t1
	initial_num			10
	multiplicity		3
	size				1

	$process fermigoldenrule
		int_state		gs
		target_states	t1
		ele_coup_func	external dexter
		vibrations		marcusmodel
	$$process

	$process distancecustomrate
		int_state		t1
		target_states	tt
		distance		20.09175
		rate			1e+6
		proc_time		0
	$$process

	$trasition gs
		reorg_energy	0.1
	$$transition

$$state

! ========
! STATE S1
! ========

$state s1
	initial_num			0
	multiplicity		1
	size				1

	$process fermigoldenrule
		int_state		gs
		target_states	s1
		ele_coup_func	external forster
		vibrations		marcusmodel
	$$process

	$trasition gs
		reorg_energy	0.1
	$$transition
	
	$transition tt
		reorg_energy	0.1
	$$transition

$$state

! ========
! STATE TT
! ========

$state tt
	initial_num			0
	multiplicity		1
	size				2
	
	$hamiltonian
	$$hamiltonian

	$process fermigoldenrule
		target_states	gs,s1
		ele_coup_func	external tta
		vibrations		marcusmodel
	$$process
	
	$transition s1
		reorg_energy	0.1
	$$transition

$$state
\end{lstlisting}

\end{document}
