#include "../../src/libvibrationfunction/marcus_function.h"

#include "../../src/global_namespaces.h"
#include "../../src/global_objects.h"
#include "../../src/libmanager/error_manager.h"
#include "../../src/libmanager/file_manager.h"
#include "../../src/libsystem/system.h"

#include <armadillo>
#include <memory>

//* Global Objects Declaration ================================================

std::unique_ptr<ErrorManager> GlobalObjects::ErrorMan = nullptr;
std::unique_ptr<FileManager> GlobalObjects::FileMan = nullptr;
std::unique_ptr<System> GlobalObjects::SSystem = nullptr;

std::map<std::string, double> GlobalObjects::AtomMasses = 
{
    {"H", 1.00784},
    {"C", 12.011}
};

#if DO_TEST
std::mt19937 GlobalObjects::randomizer(0);
#else
std::random_device rd;
std::mt19937 GlobalObjects::randomizer(rd());
#endif

//* ===========================================================================

int main()
{
    GlobalObjects::SSystem = std::make_unique<System>();
    MarcusFunction marc_func;

    //* Prepare variables
    double delta_energy = -0.15;
    double range = std::abs(-delta_energy) + ncon::kb * 1e4;
    arma::dvec X = arma::linspace<arma::dvec> (-range, range, int(1e4));
    double reorg_energy = 0.1;

    //* Result variables
    arma::dvec vib_spec_p;
    arma::dvec vib_spec_n;

    //* 1e-8 K
    /* GlobalObjects::SSystem->SetTemperature(1e-8);
    vib_spec_p = marc_func.GetVibrationSpectrum(X, reorg_energy, delta_energy);
    vib_spec_n = marc_func.GetVibrationSpectrum(X, reorg_energy, -delta_energy);
    
    if (std::abs(arma::sum(vib_spec_p)) > 1e-8) { return 1; }
    if (std::abs(arma::sum(vib_spec_n)) > 1e-8) { return 1; }

    //* 298 K
    GlobalObjects::SSystem->SetTemperature(298);
    vib_spec_p = marc_func.GetVibrationSpectrum(X, reorg_energy, delta_energy);
    vib_spec_n = marc_func.GetVibrationSpectrum(X, reorg_energy, -delta_energy);
    
    if (std::abs(arma::sum(vib_spec_p)) < 1e-8) { return 1; }
    arma::dvec integral_p = arma::trapz(vib_spec_p);
    if (std::abs(arma::sum(vib_spec_p)/integral_p(0, 0) - 1.0) > 1e-8 ) { return 1; }
    if (std::abs(arma::mean(vib_spec_p) - 0.4941519667) > 1e-8) { return 1; }

    if (std::abs(arma::sum(vib_spec_n)) < 1e-8) { return 1; }
    arma::dvec integral_n = arma::trapz(vib_spec_n);
    if (std::abs(arma::sum(vib_spec_n)/integral_n(0, 0) - 1.0) > 1e-8 ) { return 1; }
    if (std::abs(arma::mean(vib_spec_n) - 0.4941519667) > 1e-8) { return 1; }

    //* Test overlap
    arma::dvec overlap = arma::trapz(vib_spec_p % vib_spec_n);
    if (std::abs(overlap(0, 0) - 2775.52814785) > 1e-8) { return 1; } */

    return 0;
}