#include "../../src/libelectronicfunction/constant_function.h"

#include "../../src/global_objects.h"
#include "../../src/libmanager/error_manager.h"
#include "../../src/libmanager/file_manager.h"
#include "../../src/libsystem/system.h"

#include <armadillo>
#include <memory>
#include <vector>

//* Global Objects Declaration ================================================

std::unique_ptr<ErrorManager> GlobalObjects::ErrorMan = nullptr;
std::unique_ptr<FileManager> GlobalObjects::FileMan = nullptr;
std::unique_ptr<System> GlobalObjects::SSystem = nullptr;

std::map<std::string, double> GlobalObjects::AtomMasses = 
{
    {"H", 1.00784},
    {"C", 12.011}
};

#if DO_TEST
std::mt19937 GlobalObjects::randomizer(0);
#else
std::random_device rd;
std::mt19937 GlobalObjects::randomizer(rd());
#endif

//* ===========================================================================

int main()
{
    GlobalObjects::SSystem = std::make_unique<System>();
    
    //* Prepare variables
    std::uniform_real_distribution<double> distribution(0, 100);
    double return_energy;
    
    for (size_t i = 0; i < 100; i++)
    {
        double test_energy = distribution(GlobalObjects::randomizer);
        ConstantFunction cf(test_energy);

        return_energy = cf.GetEnergy(std::vector<Molecule>{});
        if (std::abs(return_energy - test_energy) > 1e-8) { return 1; }

        return_energy = cf.GetEnergy(std::vector<std::weak_ptr<Molecule>>{});
        if (std::abs(return_energy - test_energy) > 1e-8) { return 1; }
    }
    
    return 0;
}