#include "../../src/libalgorithm/kinetic_monte_carlo.h"

#include "../../src/global_objects.h"
#include "../../src/libmanager/error_manager.h"
#include "../../src/libmanager/file_manager.h"
#include "../../src/libsystem/system.h"
#include "../../src/libstate/rate_state_process.h"

#include <armadillo>
#include <memory>
#include <vector>

//* Global Objects Declaration ================================================

std::map<std::string, double> GlobalObjects::AtomMasses = 
{
    {"H", 1.00784},
    {"C", 12.011}
};

#if DO_TEST
std::mt19937 GlobalObjects::randomizer(0);
#else
std::random_device rd;
std::mt19937 GlobalObjects::randomizer(rd());
#endif

//* ===========================================================================

int main()
{
    KineticMonteCarlo kmc;

    //* Prepare variables
    std::shared_ptr<State> state = std::make_shared<State>();
    std::vector<RateStateProcess> rate_list
    {
        RateStateProcess(arma::dvec(1, arma::fill::zeros), std::vector<std::string>{},
                            std::shared_ptr<Molecule>(nullptr), std::shared_ptr<BaseProcessFunction>(nullptr),
                            state, 1.0, 1.0),
        RateStateProcess(arma::dvec(1, arma::fill::zeros), std::vector<std::string>{},
                            std::shared_ptr<Molecule>(nullptr), std::shared_ptr<BaseProcessFunction>(nullptr),
                            state, 2.0, 2.0),
        RateStateProcess(arma::dvec(1, arma::fill::zeros), std::vector<std::string>{},
                            std::shared_ptr<Molecule>(nullptr), std::shared_ptr<BaseProcessFunction>(nullptr),
                            state, 3.0, 3.0),
        RateStateProcess(arma::dvec(1, arma::fill::zeros), std::vector<std::string>{},
                            std::shared_ptr<Molecule>(nullptr), std::shared_ptr<BaseProcessFunction>(nullptr),
                            state, 0.5, 0.5)
    };

    //* Result variables
    int rate_list_size = rate_list.size();
    RateStateProcess chosen;
    chosen = kmc.EvaluateProcesses(rate_list);

    //* Check if list size decreased by 1
    if (rate_list.size() != rate_list_size-1) { return 1; }

    //* Check if chosen rate state process is not in list
    for (RateStateProcess& rsp : rate_list)
    {
        if (rsp == chosen) { return 1; }
    }

    //* Check if chosen state rate is always the same
    if (chosen.GetRate() != 3.0) { return 1; }

    return 0;
}