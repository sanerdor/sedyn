#include "../../src/libalgorithm/kinetic_monte_carlo.h"

#include "../../src/global_objects.h"
#include "../../src/libmanager/error_manager.h"
#include "../../src/libmanager/file_manager.h"
#include "../../src/libsystem/system.h"

#include <armadillo>
#include <memory>
#include <vector>

#include <iomanip>

//* Global Objects Declaration ================================================

std::map<std::string, double> GlobalObjects::AtomMasses = 
{
    {"H", 1.00784},
    {"C", 12.011}
};

#if DO_TEST
std::mt19937 GlobalObjects::randomizer(0);
#else
std::random_device rd;
std::mt19937 GlobalObjects::randomizer(rd());
#endif

//* ===========================================================================

int main()
{
    size_t size = 0;
    double lifetime = 0;
    double true_lifetime = 0;

    //* Simple lifetime
    size = 100;
    StateRawData srd_1(size);
    for (size_t i = 0; i < 100; i++)
    {
        srd_1.AddRecord(arma::dvec(3, arma::fill::zeros), arma::dvec(3, arma::fill::zeros), i);
    }

    true_lifetime = 4950.0;
    lifetime = srd_1.AnalyseLifetime();
    if (std::abs(lifetime - true_lifetime) > 1e-8) { return 1; }

    //* Random large lifetime
    size = 1000;
    StateRawData srd_2(size);
    std::uniform_real_distribution<double> distribution(0, 1);
    for (size_t i = 0; i < 500; i++)
    {
        double aux = distribution(GlobalObjects::randomizer);
        srd_2.AddRecord(arma::dvec(3, arma::fill::zeros), arma::dvec(3, arma::fill::zeros), aux);
    }

    true_lifetime = 254.779010953451;
    lifetime = srd_2.AnalyseLifetime();
    if (std::abs(lifetime - true_lifetime) > 1e-8) { return 1; }

    //* Mixed and random large lifetime
    size = 1000;
    StateRawData srd_3(size);
    std::uniform_real_distribution<double> mixed_distribution(0, 1);
    for (size_t i = 0; i < 100; i++)
    {
        srd_3.AddRecord(arma::dvec(3, arma::fill::zeros), arma::dvec(3, arma::fill::zeros), i);
    }

    true_lifetime = 4950.0;
    lifetime = srd_3.AnalyseLifetime();
    if (std::abs(lifetime - true_lifetime) > 1e-8) { return 1; }

    for (size_t i = 100; i < 500; i++)
    {
        double aux = mixed_distribution(GlobalObjects::randomizer);
        srd_3.AddRecord(arma::dvec(3, arma::fill::zeros), arma::dvec(3, arma::fill::zeros), aux);
    }

    true_lifetime = 5150.815668167365;
    lifetime = srd_3.AnalyseLifetime();
    if (std::abs(lifetime - true_lifetime) > 1e-8) { return 1; }

    return 0;
}