#include "../../src/libalgorithm/kinetic_monte_carlo.h"

#include "../../src/global_objects.h"
#include "../../src/libmanager/error_manager.h"
#include "../../src/libmanager/file_manager.h"
#include "../../src/libsystem/system.h"

#include <armadillo>
#include <memory>
#include <vector>

#include <iomanip>

//* Global Objects Declaration ================================================

std::map<std::string, double> GlobalObjects::AtomMasses = 
{
    {"H", 1.00784},
    {"C", 12.011}
};

#if DO_TEST
std::mt19937 GlobalObjects::randomizer(0);
#else
std::random_device rd;
std::mt19937 GlobalObjects::randomizer(rd());
#endif

//* ===========================================================================

int main()
{
    size_t size = 0;
    double diffusion_coefficient = 0;
    double true_diffusion_coefficient = 0; 
    int n_subtraj = 100;

    //* Simple regression
    size = 100;
    StateRawData srd_1(size);
    for (size_t i = 0; i < 100; i++)
    {
        arma::dvec com = {(double)i, (double)i, (double)i};
        srd_1.AddRecord(com, arma::dvec(3, arma::fill::zeros), 1);
    }

    true_diffusion_coefficient = 37.31155778894473;
    diffusion_coefficient = srd_1.AnalyseDiffusionCoefficient(n_subtraj);
    if (std::abs(diffusion_coefficient - true_diffusion_coefficient) > 1e-8) { return 1; }

    //* Polynomial
    size = 100;
    StateRawData srd_2(size);
    for (size_t i = 0; i < 100; i++)
    {
        double aux = std::pow(i/50, 2);
        arma::dvec com = {aux, aux, aux};
        srd_2.AddRecord(com, arma::dvec(3, arma::fill::zeros), 1);
    }

    true_diffusion_coefficient = 0.005672300898431552;
    diffusion_coefficient = srd_2.AnalyseDiffusionCoefficient(n_subtraj);
    if (std::abs(diffusion_coefficient - true_diffusion_coefficient) > 1e-8) { return 1; }

    //* Larger size simple
    size = 1000;
    StateRawData srd_3(size);
    for (size_t i = 0; i < 100; i++)
    {
        arma::dvec com = {(double)i, (double)i, (double)i};
        srd_3.AddRecord(com, arma::dvec(3, arma::fill::zeros), 1);
    }

    true_diffusion_coefficient = 37.31155778894473;
    diffusion_coefficient = srd_3.AnalyseDiffusionCoefficient(n_subtraj);
    if (std::abs(diffusion_coefficient - true_diffusion_coefficient) > 1e-8) { return 1; }

    //* Larger size exponential
    size = 1000;
    StateRawData srd_4(size);
    for (size_t i = 0; i < 50; i++)
    {
        double aux = std::exp(i/25);
        arma::dvec com = {aux, aux, aux};
        srd_4.AddRecord(com, arma::dvec(3, arma::fill::zeros), 1);
    }

    true_diffusion_coefficient = 0;
    diffusion_coefficient = srd_4.AnalyseDiffusionCoefficient(n_subtraj);
    if (std::abs(diffusion_coefficient - true_diffusion_coefficient) > 1e-8) { return 1; }

    //* Larger size double function
    size = 1000;
    StateRawData srd_5(size);
    for (size_t i = 0; i < 100; i++)
    {
        double aux = std::pow(i/50, 2);
        arma::dvec com = {aux, aux, aux};
        srd_5.AddRecord(com, arma::dvec(3, arma::fill::zeros), 1);
    }

    true_diffusion_coefficient = 0.005672300898431552;
    diffusion_coefficient = srd_5.AnalyseDiffusionCoefficient(n_subtraj);
    if (std::abs(diffusion_coefficient - true_diffusion_coefficient) > 1e-8) { return 1; }

    for (size_t i = 100; i < 200; i++)
    {
        double aux = std::exp((i-100.0)/25.0);
        arma::dvec com = {aux, aux, aux};
        srd_5.AddRecord(com, arma::dvec(3, arma::fill::zeros), 1);
    }

    true_diffusion_coefficient = 2.224655790324324;
    diffusion_coefficient = srd_5.AnalyseDiffusionCoefficient(n_subtraj);
    if (std::abs(diffusion_coefficient - true_diffusion_coefficient) > 1e-8)  { return 1; }

    return 0;
}