#include "../../src/libmolecule/molecule.h"

#include "../../src/global_objects.h"
#include "../../src/libmanager/error_manager.h"
#include "../../src/libmanager/file_manager.h"
#include "../../src/libsystem/system.h"

#include <armadillo>
#include <memory>

//* Global Objects Declaration ================================================

std::unique_ptr<ErrorManager> GlobalObjects::ErrorMan = nullptr;
std::unique_ptr<FileManager> GlobalObjects::FileMan = nullptr;
std::unique_ptr<System> GlobalObjects::SSystem = nullptr;

std::map<std::string, double> GlobalObjects::AtomMasses = 
{
    {"H", 1.00784},
    {"C", 12.011},
    {"O", 15.999}
};

#if DO_TEST
std::mt19937 GlobalObjects::randomizer(0);
#else
std::random_device rd;
std::mt19937 GlobalObjects::randomizer(rd());
#endif

//* ===========================================================================

int main()
{
    GlobalObjects::SSystem = std::make_unique<System>();
    
    //* H20
    arma::dmat coord_1
    {
        {0.0000000, 0.0184041, 0.0000000},
        {0.0000000, -0.5383518, 0.7830364},
        {0.0000000, -0.5383518, -0.7830364}
    };
    coord_1 = coord_1.t();
    std::vector<std::string> elem_1 {"O", "H", "H"};
    arma::dvec com_1 {0.0, -0.04389186, 0.0};
    
    Molecule mol_1(coord_1, elem_1);
    arma::dvec res_1 = mol_1.GetCenterOfMass();
    if (arma::any(res_1 - com_1 > 1e-8)) { return 1; }
    
    return 0;
}