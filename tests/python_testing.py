import numpy as np
from scipy.stats import linregress

atom_to_mass = {"H": 1.00784,
                "C": 12.011,
                "O": 15.999}

def center_of_mass(coordinates, symbols):
    total_mass = 0
    weighted_coordinate = np.zeros(3)
    
    for enum, coord in enumerate(coordinates):
        total_mass += atom_to_mass[symbols[enum]]
        weighted_coordinate += atom_to_mass[symbols[enum]] * coord
        
    return weighted_coordinate / total_mass

def analyze_diffusion_coefficient(center_of_mass_record, delta_time_record):
    size = np.where(delta_time_record != 0)[0][-1]
    
    time_record = np.zeros(size)
    com_record = np.zeros(size)
    for ii in range(size):
        time_record[ii] = delta_time_record[ii] + time_record[np.max(ii-1,0)]
        com_record[ii] = np.linalg.norm(center_of_mass_record[ii, :] - center_of_mass_record[0, :])**2
    
    import matplotlib.pyplot as plt
    plt.plot(time_record, com_record)
    plt.show()
    
    diffusion_coefficient = linregress(time_record, com_record).slope
    print(diffusion_coefficient/6)
    
def analyze_lifetime(delta_time_record):
    lifetime = np.sum(delta_time_record)
    
    print(lifetime)


if __name__ == '__main__':
    """ coord_1 = np.array([[0.0000000, 0.0184041, 0.0000000],
                        [0.0000000, -0.5383518, 0.7830364],
                        [0.0000000, -0.5383518, -0.7830364]])
    elem_1 = ["O", "H" , "H"]
    com_1 = center_of_mass(coord_1, elem_1)
    
    print(com_1) """
    
    """ # Simple
    center_of_mass_record = np.zeros((100, 3))
    delta_time_record = np.zeros(100)
    
    for ii in range(100):
        center_of_mass_record[ii, :] = np.array([ii, ii, ii])
        delta_time_record[ii] = 1
        
    analyze_diffusion_coefficient(center_of_mass_record, delta_time_record)
    
    # Polynomial
    center_of_mass_record = np.zeros((100, 3))
    delta_time_record = np.zeros(100)
    
    for ii in range(100):
        aux = (ii/50)**2
        center_of_mass_record[ii, :] = np.array([aux, aux, aux])
        delta_time_record[ii] = 1
        
    analyze_diffusion_coefficient(center_of_mass_record, delta_time_record)
    
    # Larger size simple 
    center_of_mass_record = np.zeros((1000, 3))
    delta_time_record = np.zeros(1000)
    
    for ii in range(150):
        center_of_mass_record[ii, :] = np.array([ii, ii, ii])
        delta_time_record[ii] = 1
        
    analyze_diffusion_coefficient(center_of_mass_record, delta_time_record)
    
    # Larger size exponential
    center_of_mass_record = np.zeros((1000, 3))
    delta_time_record = np.zeros(1000)
    
    for ii in range(50):
        center_of_mass_record[ii, :] = np.array([np.exp(ii/50), np.exp(ii/50), np.exp(ii/50)])
        delta_time_record[ii] = 1
        
    analyze_diffusion_coefficient(center_of_mass_record, delta_time_record)
    
    center_of_mass_record = np.zeros((1000, 3))
    delta_time_record = np.zeros(1000)
    
    for ii in range(50):
        aux = (ii/50)**2
        center_of_mass_record[ii, :] = np.array([aux, aux, aux])
        delta_time_record[ii] = 1
        
    analyze_diffusion_coefficient(center_of_mass_record, delta_time_record)
    
    for ii in range(50, 100):
        aux = np.exp((ii-50)/25)
        center_of_mass_record """
    
    # Simple lifetime    
    delta_time_record = np.zeros(100)
    for ii in range(100):
        delta_time_record[ii] = ii
        
    analyze_lifetime(delta_time_record)
    
    
    delta_time_record = np.zeros(1000)